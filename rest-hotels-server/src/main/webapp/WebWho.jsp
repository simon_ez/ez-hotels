<%@page import="java.sql.*"%>
<%@page import="javax.sql.DataSource"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setContentType("text/html;charset=big5");

	PrintWriter printwriter = response.getWriter();
	InetAddress inetaddress = InetAddress.getLocalHost();

	printwriter.println("ServerInfo");
	printwriter.println("<br>IP: " + inetaddress.getHostAddress());
	printwriter.println("<br>Name: " + inetaddress.getHostName());

	Connection con = null;
	PreparedStatement psql = null;
	ResultSet rset = null;
	DataSource ds = null;
	String result = "";
	WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(application);
	try {
		//Oracle DataBase
		printwriter.println("<br>ORACLE(9i): ");
		ds = (DataSource) context.getBean("oracleDataSource");
		con = ds.getConnection();
		psql = con.prepareStatement("select 1 from dual");
		rset = psql.executeQuery();
		while (rset.next())
			result = rset.getString(1);
		if (result.length() > 0) {
			printwriter.println("資料庫聯結OK!");
		} else {
			printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
		}
	} catch (Exception e) {
		printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
	} finally {
		try {
			if (rset != null)
				rset.close();
			if (psql != null)
				psql.close();
			if (con != null)
				con.close();
		} catch (Exception e) {
			printwriter.println("connection close Error.");
		}
	}

	try {
		//Oracle11g DataBase
		printwriter.println("<br>ORACLE(11g): ");
		ds = (DataSource) context.getBean("dataSourceOra11g");
		con = ds.getConnection();
		psql = con.prepareStatement("select 1 from dual");
		rset = psql.executeQuery();
		while (rset.next())
			result = rset.getString(1);
		if (result.length() > 0) {
			printwriter.println("資料庫聯結OK!");
		} else {
			printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
		}
	} catch (Exception e) {
		printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
	} finally {
		try {
			if (rset != null)
				rset.close();
			if (psql != null)
				psql.close();
			if (con != null)
				con.close();
		} catch (Exception e) {
			printwriter.println("connection close Error.");
		}
	}

	try {
		//Edb DataBase
		printwriter.println("<br>EDB: ");
		ds = (DataSource) context.getBean("edbDataSource");
		con = ds.getConnection();
		psql = con.prepareStatement("select 1");
		rset = psql.executeQuery();
		while (rset.next())
			result = rset.getString(1);
		if (result.length() > 0) {
			printwriter.println("資料庫聯結OK!");
		} else {
			printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
		}
	} catch (Exception e) {
		printwriter.println("<font color='red'>資料庫聯結失敗!</font>");
	} finally {
		try {
			if (rset != null)
				rset.close();
			if (psql != null)
				psql.close();
			if (con != null)
				con.close();
		} catch (Exception e) {
			printwriter.println("connection close Error.");
		}
	}
%>
