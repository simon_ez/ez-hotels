/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.util
 * @FileName: HotelUtil.java
 * @author:   cano0530
 * @date:     2015/3/23, 下午 03:37:21
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */

package eztravel.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.image.ImageResizeUtil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import eztravel.rest.enums.hotel.HotelEquipment;
import eztravel.rest.pojo.hotel.PromoInfo;
import eztravel.rest.util.common.DateUtils;

public class HotelUtil {
  
  private static final Logger logger = LoggerFactory.getLogger(HotelUtil.class);
  
  /* 飯店候補最近天數 */
  private static final int HOTEL_WAIT_LIMIT_DAY = 2;
  
  /**
   * 將物件轉成json
   * @param object
   * @return String
   */
  public static String getObjectJsonString(Object object){
    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    String json = "NO JSON";
    try {
      json = ow.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      json = e.getMessage();
    }
    return json;
  }
  
  /**
   *  將字串物件 null 或 空白 轉成 空字串
   *  @param String s1
   *  @return 空字串或原字串
  **/
  public static String fp_isNull(String s1) {
    int count=0;
    String ss= "";
    if (s1 == null )
      return ss;
    else if (s1.equals(""))
      return ss;
    else if (s1.equals("null"))
      return ss;

    //判斷字串是否全部都是空白
    String s = s1.toString() ;
    for(int i=0; i<s.length();i++) {
      if (s.charAt(i)   == ' ' )
        count++;
    }
    //若全部為空白傳回一空字串
    if (count == s.length() )
       return ss;
    else
       return s.trim();
  }
  
  
  /**
   * Write file.
   * 
   * @param text
   *          the text
   * @param apiType
   *          the api type
   * @param extension
   *          the extension
   * @param vendor
   *          the vendor
   */
  public static void writeFile(String text, String apiType, String extension){
    try {
      Date date = new Date();
      String time = new SimpleDateFormat("HHmmss").format(date);
      String path =
          "../standalone/log/transaction/" + DateUtils.getToday("yyyyMMdd") + "/Ctrip" + File
              .separator
              + File.separator;
      String fileName = apiType + "_" + time + "_" + Thread.currentThread().getId() + "." + extension;
      File dir = new File(path);
      if(!dir.exists()){
        dir.mkdirs();
      }
      ByteArrayInputStream bais = new ByteArrayInputStream(text.getBytes("UTF-8"));
      FileOutputStream fos = new FileOutputStream(path + fileName); 
      byte[] buffer = new byte[1024]; 
      int idx = 0; 
      while ((idx = bais.read(buffer, 0, 128)) != -1) { 
        fos.write(buffer, 0, idx); 
      } 
      bais.close(); 
      fos.close();
    } catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    
  }
  
  /**
   * 取得Htl商編, 最少5位
   * 
   * @param String hotelId
   * @return String
   */
  public static String getFormattedHtlId(String hotelId) {
    String prefix = hotelId.substring(0, 4); // 取出前綴
    Integer code = Integer.parseInt(hotelId.substring(4, 13)); // 取出數字
    return prefix + String.format("%05d", code);  // 前綴 + 最少5位數字
  }
  
  
  
  // 將server equipment list 轉成 enum
  public static List<HotelEquipment> hotelequipmentexchange(List<String> equipment) {
    List<HotelEquipment> result = new ArrayList<HotelEquipment>();
    try {
      //設施依照前端篩選器排序
      List<String>sortresults = new ArrayList<String>();
      List<String>sort = new ArrayList<String>();
      sort.add("19");//wifi
      sort.add("17");
      sort.add("1");
      sort.add("13");
      sort.add("21");
      sort.add("3");
      sort.add("18");
      sort.add("5");
      sort.add("20");
      sort.add("6");
      //先將需排序的放置最前面
      for(String sorting: sort){
        for(String equip: equipment){
          if(StringUtils.equals(sorting, equip))
            sortresults.add(equip);
        }
      }
      
      //處理未排序的
      if(sortresults.size() != equipment.size()){
        for(String equip: equipment){
          if(!sort.contains(equip))
            sortresults.add(equip);
        }
      }
      equipment=sortresults;
      //轉成 enum
      for (int i = 0; i < equipment.size(); i++) {
        int index = Integer.parseInt(equipment.get(i));
        for (HotelEquipment b : HotelEquipment.values()) {
          if (index == b.getId()) {
            result.add(HotelEquipment.valueOf(b.name()));
          }
        }
      }
    } catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    return result;
  }
  
  /**
   * 將圖片網址轉成img.eztravel.com.tw
   */
  public static String convertPhotoUrl(String photoUrls) {
    String urlResult="";
    try{
        String  urlString = ImageResizeUtil.buildImage(photoUrls).toUrl();
        
        if (isPhotoExist(urlString)) {
          urlResult = urlString;
        }else{
          urlResult = photoUrls;
        }
        

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    return urlResult;
  }

  public static String processPhotosProxyUrl(String photoUrls) {
    String urlResult="";

      urlResult = ImageUtils.getThumborUrl(photoUrls, null, null);


    return urlResult;
  }
  
  public static String ctripAdjustPhotoURL(String photoUrls , String usageKey) {
    String urlResult="";

      urlResult = ImageUtils.getCtripAdjustPhotoURL(photoUrls, usageKey);


    return urlResult;
  }

  
//篩選促銷標籤
  public static List<PromoInfo> promoTag(List<PromoInfo> roomPromoInfos ,int priceAvg , int salePriceAvg) {
    
    List<PromoInfo> result = new ArrayList<PromoInfo>();
    List<String> tempStr = new ArrayList<String>();
    
    if (roomPromoInfos != null && roomPromoInfos.size()>0) {
      for(PromoInfo promo :roomPromoInfos ){
        if(promo.getPromoType() != null){
          if(!tempStr.contains(promo.getPromoType())){
            tempStr.add(promo.getPromoType());
            result.add(promo);
          }
        }
      }
    }
    
    if(result.size()==1 && result.get(0).getPromoType().equals("004")){//只有一個促銷且是app,若是售價小於原價則給003特價標簽
      if(priceAvg > salePriceAvg){
        PromoInfo pinfo = new PromoInfo();
        pinfo.setPromoType("003");
        result.add(pinfo);
      }
    }else if(result.size() ==0){//如果沒有促銷,若是售價大於原價則給003特價標簽
      if(priceAvg > salePriceAvg){
        PromoInfo pinfo = new PromoInfo();
        pinfo.setPromoType("003");
        result.add(pinfo);
      }else{
        result = null;
      }
    }
    return result;
  }
  /**
   * 去除http 或 https 的固定協定, 使其自由跟隨瀏覽頁面
   * 
   * @param String
   *          url
   */
  public static String freeSecureProtocol(String url) {
    if(url == null || url.trim().equals("")) return null;
    String secureProtocol = url.split(":")[1];
    if (secureProtocol != null && secureProtocol.length() > 0) {
      url = secureProtocol;
    }
    return url;
  }
  

  /**
   * 傳入網址檢查圖片是否存在
   * 
   * @param photoUrl
   * @return true: exist, false: not exist
   */
  public static boolean isPhotoExist(String photoUrl) {

    URL httpurl = null;
    try {
      httpurl = new URL(photoUrl);
      URLConnection urlConnection = httpurl.openConnection();
      urlConnection.getInputStream();
      return true;
    } catch (Exception e) {
      return false;
    }
  }
  
  
  /**
   * 依照入住日 判斷是否查詢候補
   * 2. 入住日超過今日「兩天」ex.7/1可候補7/3(含)以後的房間
   * 
   * @param String checkIn
   * @return 候補與否
   */
    public static boolean isQueryingWaiting(String checkIn) {
        // 飯店及民宿皆有候補
        // 查詢可訂：「入住日在 今天＋兩天（不含）內」
        if ( !canWaitAtDate(checkIn)) {
            return false;
        // 查詢候補
        } else {
            return true;
        }
    }
    
    /**
     * 判斷日期可否候補
     * 入住日期須在[今天]+[2]天之後
     * 
     * @param checkInDate
     * @return y/n
     */
    public static boolean canWaitAtDate(String checkIn) {
        return UtilDate.getDiffDays(UtilDate.getToday("yyyyMMdd") , checkIn) >= HOTEL_WAIT_LIMIT_DAY;
    }
}
