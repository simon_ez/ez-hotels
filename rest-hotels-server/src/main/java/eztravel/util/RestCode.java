package eztravel.util;

/**
 * Rest Code Enum
 */
public enum RestCode {

	/** The form empty. */
	PROCESS_OK(0, "執行完成")
	, SYSTEM_UNKNOWN_ERR(1, "未知錯誤，請通知網站管理者")
	, PARAM_MISSING(2, "參數未提供完整或遺漏")
	, BNSINESS_LOGIC_ERR(3, "系統已完成請求，但有流程問題請參閱錯誤訊息")
	, LOGIN_ERROR(4, "使用者驗證失敗"), DEFAULT(999, "");

	/** 錯誤代碼. */
	final Integer code;

	/** 異常訊息歸類. */
	final String msg;

	/**
	 * 異常訊息代碼.
	 * 
	 * @param code
	 *            異常代碼
	 * @param name
	 *            異常訊息歸類
	 */
	private RestCode(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	/**
	 * Gets the err code.
	 * 
	 * @return the err code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the err msg.
	 * 
	 * @return the err msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * From code.
	 * 
	 * @param code
	 *            the code
	 * @return the rest err code
	 */
	public static RestCode fromCode(Integer code) {
		for (RestCode restCode : values()) {
			if (restCode.code == code) {
				return restCode;
			}
		}
		throw new IllegalArgumentException("No matching constant for [" + code + "]");
	}
}
