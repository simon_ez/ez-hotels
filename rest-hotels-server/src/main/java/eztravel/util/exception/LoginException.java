package eztravel.util.exception;

/**
 * The Class BsLogicException.
 */
@SuppressWarnings("serial")
public class LoginException extends Exception{
  
  public LoginException() {
    super("停滯逾時，登入資訊已清除，請重新登入");
    // TODO Auto-generated constructor stub
  }

  public LoginException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public LoginException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public LoginException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }
}
