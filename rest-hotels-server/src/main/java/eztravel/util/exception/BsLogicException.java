package eztravel.util.exception;

/**
 * The Class BsLogicException.
 */
@SuppressWarnings("serial")
public class BsLogicException extends Exception{
  
  public BsLogicException() {
    super();
    // TODO Auto-generated constructor stub
  }

  public BsLogicException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public BsLogicException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public BsLogicException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }
}
