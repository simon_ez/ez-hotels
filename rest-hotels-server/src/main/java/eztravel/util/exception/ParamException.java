package eztravel.util.exception;
/**
 *	Param check Exception
 * @author simon
 */
@SuppressWarnings("serial")
public class ParamException extends Exception{
  public ParamException() {
    super();
    // TODO Auto-generated constructor stub
  }

  public ParamException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public ParamException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public ParamException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }
}
