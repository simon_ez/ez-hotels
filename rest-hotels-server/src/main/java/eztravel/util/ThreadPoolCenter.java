package eztravel.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by jimin on 7/16/15.
 */
public class ThreadPoolCenter {

  private static ExecutorService computingPool = Executors.newFixedThreadPool((int)(Runtime.getRuntime().availableProcessors()) + 1);
  private static ExecutorService ioPool = newCachedThreadPool();
  
  public static ExecutorService newCachedThreadPool() {  
    return new ThreadPoolExecutor((int)(Runtime.getRuntime().availableProcessors()), //corePoolSize（线程池基本大小）必须大于或等于0；
    															(int)(Runtime.getRuntime().availableProcessors() * 2), //maximumPoolSize（线程池最大大小）必须大于或等于1，必须大于或等于corePoolSize；
                                  60, //keepAliveTime（线程存活保持时间）必须大于或等于0；
                                  TimeUnit.SECONDS, //keepAliveTime（线程存活保持时间）單元；
                                  new SynchronousQueue<Runnable>(), //workQueue（任务队列）不能为空；
                                  new ThreadPoolExecutor.CallerRunsPolicy()); //调用者所在线程来运行该任务，此策略提供简单的反馈控制机制，能够减缓新任务的提交速度。
  }

  public static ExecutorService getComputingPool() {
    return computingPool;
  }

  public static ExecutorService getIoPool() {
  	//System.out.println("目前使用執行序數量: " + ((ThreadPoolExecutor)ioPool).getPoolSize());
    return ioPool;
  }

}
