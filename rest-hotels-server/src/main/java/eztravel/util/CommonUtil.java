package eztravel.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class CommonUtil {
  /**
	 * 隨機產生亂數Key (預設6碼).
	 *
	 * @return String
	 */
	public static String randomKey(){
		return randomKey(6);
	}
	
	/**
	 * 隨機產生亂數Key.
	 *
	 * @param length the length
	 * @return String
	 */
	public static String randomKey(int length) {
		StringBuilder sb = new StringBuilder();
		int i, z;
		for (i = 0; i < length; i++) {
		  z = (int) ((Math.random() * 7) % 3);
		  if (z == 1) { // 放數字
		  	sb.append((int) ((Math.random() * 10)));
			} else if (z == 2) { // 放大寫英文
			  sb.append((char) (((Math.random() * 26) + 65)));
			} else {// 放小寫英文
			    sb.append(((char) ((Math.random() * 26) + 97)));
			}
		}
		return sb.toString();
	}
	
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final Base64 base64 = new Base64();
	/**
	 * 加密
	 * @param text
	 * @return
	 */
	public static String encryptBASE64(String origin){
		try{
			if(StringUtils.isNotBlank(origin))
				origin = base64.encodeToString(origin.getBytes(DEFAULT_ENCODING));
		}catch(Exception e){
			//do something
		}
    return origin;
	}
	
	/**
	 * 解密
	 * @param text
	 * @return
	 */
	public static String decryptionBASE64(String origin){
		try{
			if(StringUtils.isNotBlank(origin))
				origin = new String(base64.decode(origin), DEFAULT_ENCODING);
		}catch(Exception e){
			//do something
		}
    return origin;
	}
	
	/**
	 * AES加解密元素
	 */
	private final static String AES_KEY = "eztravel";
	private final static String AES_IV = "eztravel";
	
	/**
	 * 加密AES
	 * @param origin
	 * @return
	 */
	public static String encryptAES(String origin){
		try{
			SecretKeySpec skeySpec = new SecretKeySpec(StringUtils.rightPad(AES_KEY, 16, "0").getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");// 算法/模式/補碼方式
			IvParameterSpec iv = new IvParameterSpec(StringUtils.rightPad(AES_IV, 16, "0").getBytes());// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(origin.getBytes());
			origin = base64.encodeToString(encrypted);
		}catch(Exception e){
			System.out.println(e.getMessage());
		//do something
		}
		return origin;
	}
	
	/**
	 * 解密AES
	 * @param origin
	 * @return
	 */
	public static String decryptionAES(String origin){
		try{
			SecretKeySpec skeySpec = new SecretKeySpec(StringUtils.rightPad(AES_KEY, 16, "0").getBytes("ASCII"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(StringUtils.rightPad(AES_IV, 16, "0").getBytes());
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			origin = new String(cipher.doFinal(base64.decode(origin)));
		}catch(Exception e){
		//do something
		}
		return origin;
	}

	/**
	 * 判斷是否全數字
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
