package eztravel.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import tw.com.eztravel.ezlogger.Logger;
import tw.com.eztravel.ezlogger.LoggerFactory;
import utils.image.ImageResizeUtil;
import utils.image.ImageUrlBuilder;

import com.squareup.pollexor.Thumbor;
import com.squareup.pollexor.ThumborUrlBuilder;
import com.squareup.pollexor.ThumborUrlBuilder.FitInStyle;


public class ImageUtils {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(ImageUtils.class);

  /**
   * Ctrip Image server 圖片格式參數:
   * 
   * R：固定宽高（压缩）
   * C：固定宽高（压缩或者放大）
   * W：高固定，宽（原图比例计算），宽固定，高（原图比例计算） （压缩）
   * Z：高固定，宽（原图比例计算），宽固定，高（原图比例计算） （压缩或者放大）
   * 
   */
  private static final String CTRIP_HOTEL_PHOTO_TYPE = "C";

  private static final String CTRIP_HOTEL_UNIVERSAL_WIDTH = "550";

  private static final String CTRIP_HOTEL_UNIVERSAL_HEIGHT = "412";

  //private static String NO_IMG_PATH = "oversea/assets/images/no_hotel_picture_small.png";

  private static boolean img_proxy_switch = true;

  public static boolean isImg_proxy_switch() {
    return img_proxy_switch;
  }

  public static void setImg_proxy_switch(boolean img_proxy_switch) {
    ImageUtils.img_proxy_switch = img_proxy_switch;
  }

  /**
   * 替換http 開頭的Url為 https
   * 
   * @param String
   *          url
   */
  public static String transferToHttps(String url) {
    if(url == null) return null;
    String secureProtocol = url.split(":")[0];
    if (secureProtocol.equals("http")) {
      // 只換第一個http
      url = url.replaceFirst("http", "https");
    }
    return url;
  }

  /**
   * 去除http 或 https 的固定協定, 使其自由跟隨瀏覽頁面
   * 
   * @param String
   *          url
   */
  public static String freeSecureProtocol(String url) {
    if(url == null) return null;
    String secureProtocol = url.split(":")[1];
    if (secureProtocol != null && secureProtocol.length() > 0) {
      url = secureProtocol;
    }
    return url;
  }

  /**
   * 去除http 或 https 的固定協定, 使其自由跟隨瀏覽頁面
   * 
   * @param String
   *          url
   */
  public static List<String> freeSecureProtocol(List<String> urls) {
    List<String> processedUrls = new ArrayList<String>();
    if(urls == null) return null;
    for (String url : urls) {
      processedUrls.add(freeSecureProtocol(url));
    }
    return processedUrls;
  }

  /**
   * 傳入網址檢查圖片是否存在
   * 
   * @param photoUrl
   * @return true: exist, false: not exist
   */
  public static boolean isPhotoExist(String photoUrl) {

    URL httpurl = null;
    try {
      httpurl = new URL(photoUrl);
      URLConnection urlConnection = httpurl.openConnection();
      urlConnection.getInputStream();
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * 組成真實Url
   * 
   * @param protocol
   *          , source = host + path , queryMap
   * @return realImgUrl
   */
  public static String assembleRealUrl(String protocol, String source, Map<String, String[]> queryMap) {
    String realImgUrl = "{protocol}://{host&path}{queryString}";
    // protocol
    String usingProtocol = "http";
    if (org.apache.commons.lang.StringUtils.isNotBlank(protocol) && protocol.equals("s")) {
      usingProtocol = "https";
    }

    // host + path = source

    // query
    String parameterQuery = "";
    if (queryMap.keySet().size() > 0) {
      int i = 0;
      for (String key : queryMap.keySet()) {
        if (i == 0)
          parameterQuery += "?";
        else
          parameterQuery += "&";
        parameterQuery += key + "=" + Arrays.toString(queryMap.get(key));
        i++;
      }
    }

    realImgUrl = realImgUrl.replace("{protocol}", usingProtocol).replace("{host&path}", source).replace("{queryString}", parameterQuery);
    return realImgUrl;
  }

  /**
   * get CtripPhote as CTRIP_HOTEL_PHOTO_SIZE</br>
   * 為原大小圖檔改為飯店圖大小
   * 
   * @param url
   * @param usageKey
   *          : "HotelUniversal" - 550 , 412
   * @return
   */
  public static String getCtripAdjustPhotoURL(String url, String usageKey) {
    // Default
    String width = "550";
    String height = "412";

    String oriPath = "images4.c-ctrip.com/target";
    String imgApPath = "dimg04.c-ctrip.com/images";

    // Check usage
    if (usageKey.equals("HotelUniversal")) {
      width = CTRIP_HOTEL_UNIVERSAL_WIDTH;
      height = CTRIP_HOTEL_UNIVERSAL_HEIGHT;
    }

    if (url != null) {
      // 調整為對方image server hostname
      if (url.contains(oriPath)) {
        url = url.replace(oriPath, imgApPath);
      }
      // 追加字尾詞段
      String additionalAdjust = "_" + CTRIP_HOTEL_PHOTO_TYPE + "_" + width + "_" + height;
      if (url.lastIndexOf(".") >= 0) {
        url = url.substring(0, url.lastIndexOf(".")) + additionalAdjust + url.substring(url.lastIndexOf("."));
      }
    }
    return url;
  }

  

  /** The resize image. */
  private static boolean resizeImage = true;

  /** The watermark image. */
  private static boolean watermarkImage = false;

  /**
   * Checks if is resize image.
   * 
   * @return true, if is resize image
   */
  public static boolean isResizeImage() {
    return resizeImage;
  }

  /**
   * Sets the resize image.
   * 
   * @param resizeImage
   *          the new resize image
   */
  public static void setResizeImage(boolean resizeImage) {
    ImageUtils.resizeImage = resizeImage;
  }

  /** image resize server endpoint *. */
  // private static String serverEndpoint = "http://img-01.eztravel.photo/";
  // private static String serverEndpoint = "http://10.10.2.190/";
  private static String serverEndpoint = "http://img.eztravel.photo/";

  /**
   * Gets the server endpoint.
   * 
   * @return the server endpoint
   */
  public static String getServerEndpoint() {
    return serverEndpoint;
  }

  /**
   * Sets the server endpoint.
   * 
   * @param serverEndpoint
   *          the new server endpoint
   */
  public static void setServerEndpoint(String serverEndpoint) {
    if (serverEndpoint == null || serverEndpoint.trim().length() == 0) {
      ImageUtils.serverEndpoint = "http://img.eztravel.photo/"; // default
    } else {
      ImageUtils.serverEndpoint = serverEndpoint;
    }
  }

  /** The watermark image url. */
  private static String watermarkImageUrl = "http://www.eztravel.com.tw/watermark/e-z-logo-2.png";

  /**
   * Gets the watermark image url.
   * 
   * @return the watermark image url
   */
  public static String getWatermarkImageUrl() {
    return watermarkImageUrl;
  }

  /**
   * Sets the watermark image url.
   * 
   * @param watermarkImageUrl
   *          the new watermark image url
   */
  public static void setWatermarkImageUrl(String watermarkImageUrl) {
    ImageUtils.watermarkImageUrl = watermarkImageUrl;
  }

  /** The image server public key. */
  public static String imageKey = "Fy3dXdajuP7IcnlNzIAiBc1n7z3aIVMvSx10JzPbYisHFOtNAhDZntyx";

  /**
   * Gets the image key.
   * 
   * @return the image key
   */
  public static String getImageKey() {
    return imageKey;
  }

  /**
   * Sets the image key.
   * 
   * @param imageKey
   *          the new image key
   */
  public static void setImageKey(String imageKey) {
    ImageUtils.imageKey = imageKey;
  }

  /**
   * Gets the thumbor url.
   * 
   * @param imgUrls
   *          the img urls
   * @param imageWidth
   *          the image width
   * @param imageHeight
   *          the image height
   * @return the thumbor url
   */
  public static String getThumborUrl(String[] imgUrls, Integer imageWidth, Integer imageHeight) {
    return getThumborUrl(imgUrls, 0, imageWidth, imageHeight);
  }

  /**
   * Gets the thumbor url.
   * 
   * @param imgUrls
   *          the img urls
   * @param imageIndex
   *          the image index
   * @param imageWidth
   *          the image width
   * @param imageHeight
   *          the image height
   * @return the thumbor url
   */
  public static String getThumborUrl(String[] imgUrls, int imageIndex, Integer imageWidth, Integer imageHeight) {
    if (imgUrls == null || imgUrls.length <= imageIndex) {
      return getThumborUrl("", null, null);
    }
    return getThumborUrl(imgUrls[imageIndex], imageWidth, imageHeight);

  }

  /**
   * Gets the thumbor url.
   * 
   * @param imgUrl
   *          the img url
   * @param imageWidth
   *          the image width
   * @param imageHeight
   *          the image height
   * @return the thumbor url
   */
  public static String getThumborUrl(String imgUrl, Integer imageWidth, Integer imageHeight) {

    if (!isImg_proxy_switch()) { // 若沒開啟代理服務 , 則直接返回
      return imgUrl;
    }

    if (imgUrl == null || imgUrl.trim().length() == 0) {
      return imgUrl;
    }

    if (imgUrl != null) { // trim imgUrl
      imgUrl = imgUrl.trim();
    }

    Thumbor thumbor = null;

    imgUrl = getDecodeString(imgUrl);
    imgUrl = getEncodeString(imgUrl);

    if (getImageKey() != null && getImageKey().trim().length() > 0) {
      // 利用key產生secure url
      thumbor = Thumbor.create(getServerEndpoint(), getImageKey());
    } else {
      thumbor = Thumbor.create(getServerEndpoint());
    }

    if ((imageWidth == null || imageWidth == 0) && (imageHeight == null || imageHeight == 0)) {
      imgUrl = thumbor.buildImage(imgUrl).toUrl();
    } else {
      imgUrl = thumbor.buildImage(imgUrl).resize(imageWidth, imageHeight).fitIn(FitInStyle.FULL).toUrl();
    }

    return imgUrl;

  }
  
  /**
   * Gets the thumbor url by ez-common
   * @param imageUrl
   * @return
   */
  public static String getThumborUrlByImageUrlBuilder(String imageUrl) {
	  ImageUrlBuilder imageUrlBuilder = ImageResizeUtil.buildImage(imageUrl);
	  return imageUrlBuilder.toUrl();
  }

  /**
   * Gets the encode string.
   * 
   * @param imgUrl
   *          the img url
   * @return the encode string
   */
  private static String getEncodeString(String imgUrl) {

    try {
      URL url = new URL(imgUrl);
      URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

      // return encode url
      return URLEncoder.encode(uri.toASCIIString(), "UTF-8");

    } catch (URISyntaxException e) {
      logger.error(e.getMessage(), e);
    } catch (MalformedURLException e) {
      logger.error(e.getMessage(), e);
    } catch (UnsupportedEncodingException e) {
      logger.error(e.getMessage(), e);
    }

    // 錯誤時回傳原url
    return imgUrl;
  }

  /**
   * Gets the Decode string.
   * 
   * @param imgUrl
   *          the img url
   * @return the Decode string
   */
  private static String getDecodeString(String imgUrl) {

    try {

      // return encode url
      return URLDecoder.decode(imgUrl, "UTF-8");

    } catch (UnsupportedEncodingException e) {
      logger.error(e.getMessage(), e);
    }

    // 錯誤時回傳原url
    return imgUrl;
  }

  /**
   * Watermark image.
   * 
   * @param srcImageUrl
   *          the src image url
   * @return the string
   * @throws UnsupportedEncodingException
   */
  public static String watermarkImage(String srcImageUrl) throws UnsupportedEncodingException {
    return watermarkImage(srcImageUrl, getWatermarkImageUrl(), -1, -1, 0, 0, 0);
  }

  /**
   * Watermark image.
   * 
   * @param srcImageUrl
   *          the src image url
   * @return the string
   * @throws UnsupportedEncodingException
   */
  public static String watermarkImage(String srcImageUrl, int width, int height) throws UnsupportedEncodingException {
    return watermarkImage(srcImageUrl, getWatermarkImageUrl(), -1, -1, 0, width, height);
  }

  /**
   * Image watermark.
   * 
   * @param srcImageUrl
   *          the src image url
   * @param watermarkImageUrl
   *          the watermark image url
   * @param x
   *          the x
   * @param y
   *          the y
   * @param t
   *          the t
   * @return the string
   * @throws UnsupportedEncodingException
   */
  public static String watermarkImage(String srcImageUrl, String watermarkImageUrl, int x, int y, int t, int width, int height) throws UnsupportedEncodingException {

    Thumbor thumbor = null;

    if (srcImageUrl == null) {
      return srcImageUrl;
    }

    // 判斷若url已包含server endpoint,就不處理
    if (srcImageUrl.contains(getServerEndpoint())) {
      return srcImageUrl;
    }

    String imgUrl = getEncodeString(URLDecoder.decode(srcImageUrl, "UTF-8"));

    if (getImageKey() != null && getImageKey().trim().length() > 0) {
      // 利用key產生secure url
      thumbor = Thumbor.create(getServerEndpoint(), getImageKey());
    } else {
      thumbor = Thumbor.create(getServerEndpoint());
    }

    if (watermarkImage) {
      if (width != 0 && height != 0) {
        imgUrl = thumbor.buildImage(imgUrl).crop(0, 0, height, width).resize(width, height).fitIn().filter(ThumborUrlBuilder.watermark(getEncodeString(watermarkImageUrl), x, y, t)).toUrl();
      } else if (width != 0 || height != 0) {
        imgUrl = thumbor.buildImage(imgUrl).resize(width, height).fitIn().filter(ThumborUrlBuilder.watermark(getEncodeString(watermarkImageUrl), x, y, t)).toUrl();
      } else {
        imgUrl = thumbor.buildImage(imgUrl).filter(ThumborUrlBuilder.watermark(getEncodeString(watermarkImageUrl), x, y, t)).toUrl();
      }
    } else {
      if (width != 0 && height != 0) {
        imgUrl = thumbor.buildImage(imgUrl).crop(0, 0, height, width).resize(width, height).fitIn().toUrl();
      } else if (width != 0 || height != 0) {
        imgUrl = thumbor.buildImage(imgUrl).resize(width, height).fitIn().toUrl();
      } else {
        imgUrl = thumbor.buildImage(imgUrl).toUrl();
      }
    }

    return imgUrl;
  }

  /**
   * The main method.
   * 
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {

    // setServerEndpoint("//img.eztravel.photo");

    String imgUrl = "http://www.eztravel.com.tw/img/FRT/FRT0000011982.gif";
    // imgUrl = "http://booking.hoshinoresort.com/upload/0000000121/images_u/plan/11.%20%E4%B8%AD%E5%BA%ADB_R507380.jpg"; //中文Encode
    // imgUrl = "http://booking.hoshinoresort.com/upload/0000000121/images_u/plan/11. 中庭B_R507380.jpg"; // 含中文
    imgUrl = "http://dimg04.c-ctrip.com/images/fd/hotelcomment/g4/M06%2FE4%2F95/CggYHFbF3Z6AWphCAAI35Tr8Rt0449_C_550_412.jpg";// 較為一般的

    System.out.println("imgUrl decode: " + getDecodeString(imgUrl));
    System.out.println("imgUrl encode: " + getEncodeString(imgUrl));

    System.out.println(getThumborUrl(imgUrl, null, null));

    System.exit(0);

  }
}
