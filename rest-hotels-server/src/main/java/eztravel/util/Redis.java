package eztravel.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.input.ClassLoaderObjectInputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Redis {
	private static final Logger log = LoggerFactory.getLogger(Redis.class);
	protected JedisPool pool;
	
	/**
	 * 初始化 Redis Connect
	 * @param dbIndex
	 */
	public Redis(int dbIndex) {
		Map<String, String> config = this.getEnvConfig();
		String address;
		String password;
		if (StringUtils.equals(config.get("app.environment"), "PROD")) {
			address = "10.10.2.169";
			password = config.get("redis.passwd");
		} else {
			// address = "127.0.0.1";
			address = "10.248.220.135";
			password = config.get("redis.passwd");
		}
		int port = 6379;
		int timeout = 2000;
		JedisPoolConfig jpc = new JedisPoolConfig();
		// jpc.setMaxTotal(500); // 最多2000連線
		// jpc.setMaxIdle(200); // 最多500等待
		// jpc.setMinIdle(20); // 最少50等待
		jpc.setTestOnBorrow(true);
		this.pool = new JedisPool(jpc, address, port, timeout, password, dbIndex);
	}
	
	/**
	 * 關閉 Redis Connect
	 */
	public void closeJedisPool() {
    this.pool.destroy();
  }

	/**
	 * 取得 Redis Value
	 * @param key
	 * @return
	 */
  public Object get(String key) {
    Jedis jedis = null;
    String cacheTxt = null;
    Object cacheResult = null;
    
    try {
    	jedis = this.pool.getResource();
      cacheTxt = jedis.get(key);
    } catch (Exception e) {
    	log.error("Redis getKey: " + key);
      log.error("Redis get Connection Error", e);
      if (jedis != null) {
        this.pool.returnBrokenResource(jedis);
        jedis = null;
      }
    } finally {
      if (jedis != null)
        this.pool.returnResource(jedis);
    }

    if (cacheTxt != null) {
    	ByteArrayInputStream bais = null;
      ClassLoaderObjectInputStream clo = null;
      
      try {
      	byte[] cacheBytes = Base64Coder.decode(cacheTxt);
        bais = new ByteArrayInputStream(cacheBytes);
        clo = new ClassLoaderObjectInputStream(this.getClass().getClassLoader(), bais);
        cacheResult = clo.readObject();
      } catch (Exception e) {
      	log.error("Redis GetDecode Error, key: " + key);
        log.error(e.getMessage(), e);
      } finally {
        try {
          if (bais != null)
            bais.close();
          if (clo != null)
            clo.close();
        } catch (Exception e) {
        }
      }
    }
    return cacheResult;
  }

  /**
   * 儲存 Redis
   * @param key
   * @param value
   * @return
   */
  public String set(String key, Object value) {
  	Jedis jedis = null;
    ByteArrayOutputStream bao = null;
    ObjectOutputStream oos = null;
    String cacheTxt = null;

    try {
      bao = new ByteArrayOutputStream();
      oos = new ObjectOutputStream(bao);
      oos.writeObject(value);
      oos.flush();
      oos.reset();
      cacheTxt = new String(Base64Coder.encode(bao.toByteArray()));
    } catch (IOException e) {
    	log.error("Redis SetDecode Error, key: " + key);
      log.error(e.getMessage(), e);
    } finally {
      try {
      	if (oos != null)
          oos.close();
      	if (bao != null)
          bao.close();
      } catch (Exception e) {
      }
    }
    
    try {
      jedis = this.pool.getResource();
      cacheTxt = jedis.set(key, cacheTxt);
    } catch (Exception e) {
    	log.error("Redis setKey: " + key);
      log.error("Redis set Connection Error", e);
      if (jedis != null) {
        this.pool.returnBrokenResource(jedis);
        jedis = null;
      }
    } finally {
      if (jedis != null)
        this.pool.returnResource(jedis);
    }
    return cacheTxt;
  }

  /**
   * 設定 Redis Key 時效
   * @param key
   * @param seconds
   * @return
   */
  public Long expire(final String key, final int seconds) {
    Jedis jedis = null;
    try {
      jedis = this.pool.getResource();
      return jedis.expire(key, seconds);
    } catch (Exception e) {
    	log.error("Redis expire Error, key: " + key + ", seconds: " + seconds);
      log.error("Redis expire Connection Error", e);
      if (jedis != null) {
        this.pool.returnBrokenResource(jedis);
        jedis = null;
      }
      return 0l;
    } finally {
      if (jedis != null)
        this.pool.returnResource(jedis);
    }
  }

  /**
   * flush current db's cacheKey & data
   */
  public void flushDb() {
    try {
      this.pool.getResource().flushDB();
    } catch (Exception e) {
      log.error("Redis flushDB Error", e);
    }
  }

	/**
	 * 取得 Keys
	 * @param path
	 * @return
	 */
	public Set<String> getKeys(String path){
		Jedis jedis = null;
		Set<String> result = null;
		try {
      jedis = this.pool.getResource();
      result = jedis.keys(path);
    } catch (Exception e) {
    	log.error("Redis getKeys Error, Path: " + path);
      log.error("Redis getKeys Connection Error", e);
      if (jedis != null) {
        this.pool.returnBrokenResource(jedis);
        jedis = null;
      }
    } finally {
      if (jedis != null)
        this.pool.returnResource(jedis);
    }
		return result;
	}
	
	/**
	 * 取得系統app.environment設定值
	 * 目前因Spring注入時機點可能抓不到config設定值
	 */
	public Map<String, String> getEnvConfig(){
		Map<String, String> configMap = new HashMap<String, String>();
    try {
    	Properties config = new Properties();
    	String envPath = System.getenv("HOTEL_CONF");
      if (StringUtils.isBlank(envPath))
      	envPath = "config.properties";
      else
      	envPath = envPath + "/jetty/config.properties";
    	config.load(new FileInputStream(envPath));
    	Enumeration<Object> keys = config.keys();
    	
    	while(keys.hasMoreElements()){
    		String key = keys.nextElement().toString();
    		configMap.put(key, config.getProperty(key));
    	}   	
    } catch (IOException e) {
    	log.error("Redis get Config Error:" + e.toString());
    }
    return configMap;
	}
	
	/**
	 * 刪除key
	 * @param keys
	 */
	public void deleteKey(String... keys){
      Jedis jedis = null;      
      try {
        jedis = this.pool.getResource();
        jedis.del(keys);
      } catch (Exception e) {      
        log.error("Redis getKeys Connection Error", e);
        if (jedis != null) {
          this.pool.returnBrokenResource(jedis);
          jedis = null;
        }
      } finally {
        if (jedis != null)
          this.pool.returnResource(jedis);
      }        
    }
	
	/**
	 * 刪除key
	 * @param pattenrn
	 */
	public void deleteKeys(String pattenrn){	  
      Jedis jedis = null;
      Set<String> keySet = null;
      try {
        
        jedis = this.pool.getResource();
        keySet = jedis.keys(pattenrn);
        if(keySet==null || keySet.size()<=0){
          return;
        }
        String[] keyArray = new String[keySet.size()];
        int i = 0;
        for (String keys : keySet) {
          keyArray[i] = keys;
          i++;
        }
        
        deleteKey(keyArray);   
        
      } catch (Exception e) {      
        log.error("Redis getKeys Connection Error", e);
        if (jedis != null) {
          this.pool.returnBrokenResource(jedis);
          jedis = null;
        }
      } finally {
        if (jedis != null)
          this.pool.returnResource(jedis);
      }        
    }
	
	/**
	 * 前贅字{param}刪除
	 * @param prefix
	 */
	public void deleteKeyByPrefix(String prefix){
	  if(prefix.length() != 0 || prefix.trim() != null){
	    deleteKeys(prefix+"*");
	  }
	}
	
	/**
	 * 刪除包含{param}的key
	 * @param contain
	 */
	public void deleteKeyByContain(String contain) {
		deleteKeys("*" + contain + "*");
	}

	public Set<String> getResultsCacheKeyByStartWith(String startStr) throws Exception {
		Set<String> list = new HashSet<String>();
		Jedis jedis = null;
		try {
			jedis = this.pool.getResource();
			list = jedis.keys(startStr + "*");
		} catch (Exception e) {
			if (jedis != null) {
				jedis.close();
				jedis = null;
			}
		} finally {
			if (jedis != null) {
				jedis.close();
				jedis = null;
			}
		}
		return list;
	}
}
