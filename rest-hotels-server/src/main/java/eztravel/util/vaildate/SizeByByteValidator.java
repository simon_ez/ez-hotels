package eztravel.util.vaildate;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SizeByByteValidator implements ConstraintValidator<SizeByByte, Object>{
  private int min;
  private int max;
  private boolean canNull;

  public void initialize(SizeByByte parameters) {
      min = parameters.min();
      max = parameters.max();
      canNull = parameters.canNull();
      validateParameters();
  }

  public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
		if(!canNull && obj == null){
			return false;
		}		
		if(obj != null){
			if(obj instanceof String){
				int length = ((String)obj).getBytes().length;
	      return length >= min && length <= max;
			}else if(obj instanceof Integer){
				return (Integer)obj >= min && (Integer)obj <= max;
			}else if(obj instanceof BigDecimal){
				return ((BigDecimal) obj).compareTo(new BigDecimal(min)) >= 0 && ((BigDecimal) obj).compareTo(new BigDecimal(max)) <= 0;
			}
		}
		return true;
  }

  private void validateParameters() {
      if ( min < 0 ) {
          throw new IllegalArgumentException( "The min parameter cannot be negative." );
      }
      if ( max < 0 ) {
          throw new IllegalArgumentException( "The max parameter cannot be negative." );
      }
      if ( max < min ) {
          throw new IllegalArgumentException( "The length cannot be negative." );
      }
  }
}
