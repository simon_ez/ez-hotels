package eztravel.util.vaildate;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = SizeByByteValidator.class)
@Documented
public @interface SizeByByte {
  int min() default 0;
  int max() default Integer.MAX_VALUE;
  boolean canNull() default true;
  String message() default "{javax.validation.constraints.Size.message}";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}
