package eztravel.util.vaildate;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import eztravel.util.exception.ParamException;



public class ValidateUtil { 
  
  private static Validator validator; // 它是线程安全的
  
  static { 
      ValidatorFactory factory = Validation.buildDefaultValidatorFactory(); 
      validator = factory.getValidator(); 
  } 

  /**
   * Model驗證入口
   * 1. 字元若是中文會加入兩個字元，使得驗證時中文為三碼長度驗證 (模擬DB中文字元3個byte)
   * 2. 有錯誤訊息僅回傳第一筆錯誤訊息`
   * @param t
   * @throws OrderException
   */
  public static <T> void validate(T t) throws ParamException  {
    Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
    if(constraintViolations.size() > 0) {
      for(ConstraintViolation<T> constraintViolation: constraintViolations){
      	throw new ParamException(constraintViolation.getMessage() + ", Input Value: " + constraintViolation.getInvalidValue());
      }
    }
  }   
} 