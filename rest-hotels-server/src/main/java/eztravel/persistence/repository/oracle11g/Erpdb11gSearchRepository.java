/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.common
 * @FileName: Erpdb11gSearchRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:39:20
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.CustomerDB;
import eztravel.core.pojo.DayPriceExtend;
import eztravel.core.pojo.InternetStatus;
import eztravel.core.service.hotel.extend.SearchMinsRoom;
import eztravel.rest.pojo.hotel.Bedsize;

/**
 * The Interface Erpdb11g SearchRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface Erpdb11gSearchRepository {

	/**
	 * get hotels' equipment.
	 * @param hotelIds
	 * @return the htls equip
	 */
	List<String> getHtlsEquip(@Param("hotelIds") List<String> hotelIds);
	
	/**
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param nationality
	 * @param agentType
	 * @return
	 */
	List<SearchMinsRoom> getHotelRoomsInfoExtend(@Param("hotelId") String hotelId, @Param("roomId") String roomId, @Param("roomQty") int roomQty, @Param("checkin") String checkin, @Param("checkout") String checkout, @Param("nationality") String nationality, @Param("agentType") String agentType);

	/**
	 * Gets the rooms internet status.
	 * @param hotelId
	 * @param roomIds
	 * @return the rooms internet status
	 */
	List<InternetStatus> getRoomsInternetStatus(@Param("hotelId") String hotelId, @Param("roomIds") List<SearchMinsRoom> roomIds);
	
	/**
	 * get room's each bed size.
	 * @param hotelId
	 * @param roomId
	 * @return the room bed size
	 */
	List<Bedsize> getRoomBedSize(@Param("hotelId") String hotelId, @Param("roomId") String roomId);
	
	/**
	 * Return room's photos. If there is no room's photos, it will return first photo of hotel.
	 * @param hotelId
	 * @param roomId
	 * @return the room photos
	 */
	List<String> getRoomPhotos(@Param("hotelId") String hotelId, @Param("roomId") String roomId);
	
	/**
	 * 取得每日價格sql
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param nationality
	 * @param agentType
	 * @return
	 */
	List<DayPriceExtend> getRoomDailyPriceExtend(@Param("hotelId") String hotelId, @Param("roomId") String roomId, @Param("roomQty") int roomQty, @Param("checkin") String checkin, @Param("checkout") String checkout, @Param("nationality") String nationality, @Param("agentType") String agentType);
	
	/**
	 * based on customer ID to decide which agent type.
	 * @param customerId
	 * @return agent type
	 */
	CustomerDB getCustomerInfo(@Param("customerId") String customerId);
}
