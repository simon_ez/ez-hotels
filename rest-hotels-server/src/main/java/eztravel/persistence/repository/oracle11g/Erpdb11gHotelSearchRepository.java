/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.common
 * @FileName: Erpdb11gHotelSearchRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:38:51
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.HtlMinPriceExtend;
import eztravel.rest.pojo.hotel.Range;

public interface Erpdb11gHotelSearchRepository {
	
	/**
	 * 飯店最低價
	 * @param hotelIds
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param roomTypes
	 * @param priceRanges
	 * @param nationality
	 * @param agentType
	 * @param srcType 被呼叫產生的頁面 ：prod＝>產品頁,hotelist=>飯店列表頁
	 * @return
	 */
	public List<HtlMinPriceExtend> getHotelsMinPrice(@Param("hotelIds") List<String> hotelIds
																								, @Param("roomQty") int roomQty
																								, @Param("checkin") String checkin
																								, @Param("checkout") String checkout
																								, @Param("roomTypes") List<Integer> roomTypes
																								, @Param("priceRanges") List<Range> priceRanges
																								, @Param("nationality") String nationality
																								, @Param("agentType") String agentType
																								, @Param("srcType") String srcType);

}
