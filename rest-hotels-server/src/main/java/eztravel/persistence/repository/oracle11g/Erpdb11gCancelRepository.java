/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.cancel
 * @FileName: Erpdb11gCancelRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:38:21
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.CancelDetail;
import eztravel.core.pojo.CancelOrderDetailDB;
import eztravel.rest.pojo.hotel.DayPrice;
import eztravel.rest.pojo.hotel.cancel.CancelReason;
import eztravel.rest.pojo.hotel.cancel.OnlineCancelOrder;
import eztravel.rest.pojo.hotel.cancel.OnlineCancelProd;

/**
 * The Interface Erpdb11gCancelRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface Erpdb11gCancelRepository {

  

  /**
   * Gets the cancel exception.
   * 
   * @param hotelId
   *          the hotel id
   * @param roomId
   *          the room id
   * @param checkIn
   *          the check in
   * @return the cancel exception
   */
  List<CancelDetail> getCancelExcep(@Param("hotelId")String hotelId , @Param("roomId")String roomId , @Param("checkIn")String checkIn );
  
  
  /**
   * Gets the cancel excep common.
   * 
   * @param hotelId
   *          the hotel id
   * @param checkIn
   *          the check in
   * @return the cancel excep common
   */
  List<CancelDetail> getCancelExcepCommon(@Param("hotelId")String hotelId  , @Param("checkIn")String checkIn );

  /**
   * Gets the cancel for roomtype no.
   * 
   * @param hotelId
   *          the hotel id
   * @param roomId
   *          the room id
   * @param checkIn
   *          the check in
   * @param agentType
   *          the agent type
   * @return the cancel for roomtype no
   */
  List<CancelDetail> getCancelForRoomtypeNo(@Param("hotelId")String hotelId , @Param("roomId")String roomId , @Param("checkIn")String checkIn ,@Param("agentType")String agentType );
  
  /**
   * Gets the cancel for roomtype x.
   * 
   * @param hotelId
   *          the hotel id
   * @param roomId
   *          the room id
   * @param checkIn
   *          the check in
   * @param agentType
   *          the agent type
   * @return the cancel for roomtype x
   */
  List<CancelDetail> getCancelForRoomtypeX(@Param("hotelId")String hotelId , @Param("roomId")String roomId , @Param("checkIn")String checkIn ,@Param("agentType")String agentType );
  
  /**
   * Gets the cancel for prod no.
   * 
   * @param hotelId
   *          the hotel id
   * @param roomId
   *          the room id
   * @param checkIn
   *          the check in
   * @param agentType
   *          the agent type
   * @return the cancel for prod no x
   */
  List<CancelDetail> getCancelForProdNo(@Param("hotelId")String hotelId , @Param("roomId")String roomId , @Param("checkIn")String checkIn ,@Param("agentType")String agentType);
  
  /**
   * Gets the cancel for prod no x.
   * 
   * @param agentType
   *          the agent type
   * @return the cancel for prod no x
   */
  List<CancelDetail> getCancelForProdNoX(@Param("agentType")String agentType );
  /**
   * Gets the list all day.
   * 
   * @param mindate
   *          the mindate
   * @param maxdate
   *          the maxdate
   * @return the list all day
   */
  List<String> getlistAllDay(@Param("mindate")String mindate , @Param("maxdate")String maxdate);
  
  
  /**
   * get each day's room price.
   * 
   * @param hotelId the hotel id
   * @param roomId the room id
   * @param roomQty the room qty
   * @param checkin the checkin
   * @param checkout the checkout
   * @param nationality the nationality
   * @param agentType the agent type
   * @param isERP the is ERP
   * @return the room daily price
   */
  public List<DayPrice> getRoomDailyPriceForCancel(@Param("hotelId") String hotelId,
      @Param("roomId") String roomId, @Param("roomQty") int roomQty,
                                          @Param("checkin") String checkin,
      @Param("checkout") String checkout, @Param("nationality") String nationality,
      @Param("agentType") String agentType , @Param("isERP") String isERP);
  
  /**
   * Gets the order details.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeq
   *          the prod seq
   * @return the order details
   */
  public List<CancelOrderDetailDB> getCancelOrderDetails(@Param("orderNo") String orderNo, @Param("prodSeq") String prodSeq);
  
  /**
   * Gets the order refund.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeq
   *          the prod seq
   * @return the order refund
   */
  public List<CancelDetail> getOrderRefund(@Param("orderNo") String orderNo, @Param("prodSeq") String prodSeq);

  /**
   * Gets the order refund exception.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeq
   *          the prod seq
   * @return the order refund exception
   */
  public List<CancelDetail> getOrderRefundException(@Param("orderNo") String orderNo,
                  @Param("prodSeq") String prodSeq);

  /**
   * Gets the prod seqs.
   * 
   * @param orderNo
   *          the order no
   * @return the prod seqs
   */
  public List<String> getProdSeqs(@Param("orderNo") String orderNo);
  
  /**
   * Gets the cancel change for room.
   * 
   * @param hotelId
   *          the hotel id
   * @param roomId
   *          the room id
   * @return the cancel change for room
   */
  public List<CancelDetail> getCancelChangeForRoom(@Param("hotelId") String hotelId,
      @Param("roomId") String roomId);
  
  /**
   * Gets the cancel change for hotel.
   * 
   * @param hotelId
   *          the hotel id
   * @return the cancel change for hotel
   */
  public List<CancelDetail> getCancelChangeForHotel(@Param("hotelId") String hotelId);

  /**
   * get order main info 取得訂單主要資訊.
   * 
   * @param orderNo
   *          the order no
   * @return the static order main
   */
  public List<OnlineCancelOrder> getStaticOrderMain(@Param("orderNo")String orderNo);
  
  /**
   * get order prod info 取得訂單下不同房型或是不同飯店的訂單資訊.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeqnos
   *          the prod seqnos
   * @param custSeqnos
   *          the cust seqnos
   * @return the static order prod
   */
  public List<OnlineCancelProd> getStaticOrderProd(@Param("orderNo")String orderNo,@Param("prodSeqno")String[] prodSeqnos,@Param("custSeqno")String[] custSeqnos);
  
  /**
   * get check in need info and cancel info 取得用來確認是否可以取消的資料且取得取消所需要的資料.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeqnos
   *          the prod seqnos
   * @param custSeqnos
   *          the cust seqnos
   * @return the list
   */
  public List<OnlineCancelProd> cancelCheck(@Param("orderNo")String orderNo,@Param("prodSeqno")String[] prodSeqnos,@Param("custSeqno")String[] custSeqnos);
  
  /**
   * get all part_seqno for cancel use 取得part_seqno為了取消使用.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeqno
   *          the prod seqno
   * @param custSeqno
   *          the cust seqno
   * @return the part seqno
   */
  public List<OnlineCancelProd> getPartSeqno(@Param("orderNo")String orderNo,@Param("prodSeqno")String[] prodSeqno,@Param("custSeqno")String[] custSeqno);
  
  /*
   * 修改傳真報名單注意事項使用 
   */
  /**
   * Update fax detail.
   * 
   * @param params
   *          the params
   */
  public void updateFaxDetail(Map<String, Object> params);
  
  /* 修改取消狀態
   * Update cancel status.
   */
  /**
   * Update cancel status.
   * 
   * @param params
   *          the params
   */
  public void updateCancelStatus(Map<String, Object> params);
  
  
  /**
   * record cancel reason status Applicant 記錄取消原因狀態與申請者.
   * 
   * @param params
   *          the params
   */
  public void createOnlineCancelRecord(Map<String, Object> params);
  
  /**
   * call cancel store procedure.
   * 
   * @param params
   *          the params
   */
  public void callCancelSP(Map<String, Object> params);
  
  /**
   * get Cancel reason .
   * 
   * @param codeType
   *          the code type
   * @return the cancel reason
   */
  public List<CancelReason> getCancelReason(String codeType);
  
  /**
   * URL定時內可以連結.
   * 
   * @param action
   *          the action
   * @param param
   *          the param
   * @param optionParam
   *          the option param
   * @param vaildTimeS
   *          the vaild time s
   * @param vaildTimeE
   *          the vaild time e
   */
  public void setSafeUrl(String action,String param,String optionParam,String vaildTimeS ,String vaildTimeE);
  
  /**
   * Check emp.
   * 
   * @param empId
   *          the emp id
   * @return the string
   */
  public String checkEmp(@Param("userId")String empId);
  
  /**
   * Check order.
   * 
   * @param custNo
   *          the custNo
   * @param orderNo
   *          the order no
   * @return the string
   */
  public String checkOrder(@Param("custNo")String custNo, @Param("orderNo")String orderNo);
  
  /**
   * Gets the cancel reason desc by code.
   * 
   * @param CODE_ID
   *          the code id
   * @return the cancel reason desc by code
   */
  public String getCancelReasonByCode(@Param("CODE_ID")String CODE_ID);
  
  
  
  /**
   * Gets the cancel reaso nm.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeqnos
   *          the prod seqnos
   * @return the cancel reaso nm
   */
  public String  getCancelReasoNm(@Param("orderNo")String orderNo , @Param("prodSeqno")String[] prodSeqnos);
  
  /**
   * Gets the cancel reaso code.
   * 
   * @param orderNo
   *          the order no
   * @param prodSeqnos
   *          the prod seqnos
   * @return the cancel reaso code
   */
  public String  getCancelReasoCode(@Param("orderNo")String orderNo , @Param("prodSeqno")String[] prodSeqnos);
  
}
