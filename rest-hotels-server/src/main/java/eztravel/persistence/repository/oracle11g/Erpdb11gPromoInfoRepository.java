/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.common
 * @FileName: Erpdb11gPromoInfoRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:39:03
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.hotel.PromoInfo;

import java.util.List;

/**
 * The Interface Erpdb11g PromoInfoRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface Erpdb11gPromoInfoRepository {

	/**
	 * Gets the early promo info.
	 * @param hotelId
	 * @param roomId
	 * @param checkin
	 * @param checkout
	 * @param promotype
	 * @return the early promo info
	 */
	List<PromoInfo> getEarlyPromoInfo(@Param("hotelId") String hotelId, @Param("roomId") String roomId, @Param("checkin") String checkin, @Param("checkout") String checkout, @Param("promotype") String promotype);

	/**
	 * Gets the last promo info.
	 * @param hotelId
	 * @param roomId
	 * @param checkin
	 * @param checkout
	 * @return the last promo info
	 */
	List<PromoInfo> getLastPromoInfo(@Param("hotelId") String hotelId, @Param("roomId") String roomId, @Param("checkin") String checkin, @Param("checkout") String checkout);

	/**
	 * Gets the common promo info.
	 * @param hotelId
	 * @param roomId
	 * @param checkin
	 * @param checkout
	 * @return the common promo info
	 */
	List<PromoInfo> getCommonPromoInfo(@Param("hotelId") String hotelId, @Param("roomId") String roomId, @Param("checkin") String checkin, @Param("checkout") String checkout);

	/**
	 * Gets the promo info.
	 * @param hotelIds
	 * @param checkin
	 * @return the promo info
	 */
	List<PromoInfo> getPromoInfo(@Param("hotelIds") List<String> hotelIds, @Param("checkin") String checkin);

}
