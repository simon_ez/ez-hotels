/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.hotel
 * @FileName: Erpdb11gFacilityRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:39:36
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import java.util.List;

import eztravel.core.pojo.HotelFacility;

/**
 * <pre>
 * FacilityRepository, 設施服務.
 * </pre>
 * @author Kent
 */
public interface Erpdb11gFacilityRepository {

	/**
	 * List hotel facilites. 飯店設施服務項目清單
	 * @param hotelId
	 * @return the list
	 */
	List<HotelFacility> listHotelFacilites(String hotelId);
}
