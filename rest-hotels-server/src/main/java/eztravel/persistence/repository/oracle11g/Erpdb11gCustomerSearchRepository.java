/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.oracle11g.common
 * @FileName: Erpdb11gCustomerSearchRepository.java
 * @author:   cano0530
 * @date:     2017/11/2, 下午 02:38:46
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle11g;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.CustomerDB;

/**
 * Created by jimin on 9/3/15.
 */
public interface Erpdb11gCustomerSearchRepository {

	/**
	 * based on customer ID to decide which agent type.
	 * @param customerId
	 * @return information of customer
	 */
	CustomerDB getCustomerInfo(@Param("customerId") String customerId);
	
	/**
	 * Vaild agent rule.
	 * @param hotelId
	 * @return the string
	 */
	String vaildAgentRule(@Param("hotelId") String hotelId);

}
