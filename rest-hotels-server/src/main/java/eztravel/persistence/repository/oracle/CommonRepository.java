package eztravel.persistence.repository.oracle;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface CommonRepository {

	/**
	 * 取得R86系統定價公式
	 * @return List<Map<String, String>>
	 */
	List<Map<String, String>> getPricingFormula();

	/**
	 * 取得Agent Rule
	 * @param prodNo
	 * @param roomtypeNo
	 * @param countryCd
	 * @param agentType
	 * @return Map<String, String>
	 */
	Map<String, Object> getAgentRule(@Param("prodNo") String prodNo, @Param("roomtypeNo") String roomtypeNo, @Param("countryCd") String countryCd, @Param("agentType") String agentType);
}
