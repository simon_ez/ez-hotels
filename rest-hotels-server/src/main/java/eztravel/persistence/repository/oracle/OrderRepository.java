package eztravel.persistence.repository.oracle;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface OrderRepository {
	/**
	 * 訂單反查飯店資訊
	 * @param orderNo
	 * @return
	 */
	List<Map<String, Object>> getHotelInfoByOrderNoOrProdNo(@Param("orderNo") String orderNo, @Param("hotelId") String hotelId);
}
