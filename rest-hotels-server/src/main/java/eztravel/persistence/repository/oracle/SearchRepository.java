/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.common
 * @FileName: SearchRepository.java
 * @author:   cano0530
 * @date:     2017/2/10, 上午 10:42:25
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.oracle;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.hotel.CodeDetail;
import eztravel.rest.pojo.hotel.HotelStandalone;

/**
 * The Interface SearchRepository.
 */
public interface SearchRepository {

	/**
	 * Gets the hotel photos.
	 * @param hotelId
	 * @return the hotel photos
	 */
	List<String> getHotelPhotos(@Param("hotelId") String hotelId);

	/**
	 * get hotel's information.
	 * @param hotelId
	 * @return the hotel info
	 */
	HotelStandalone getHotelInfo(@Param("hotelId") String hotelId);

	/**
	 * get hotel's view point(ex. 中山站, 松山區...etc)
	 * @param hotelId
	 * @return the htl view
	 */
	List<CodeDetail> getHtlView(@Param("hotelId") String hotelId);
	
	/**
	 * It's used for getting the equipments we need to display on the web.
	 * @return the used equip id
	 */
	List<Integer> getUsedEquipId();
	
	/**
	 * 城市或鄰近景點反查取得飯店資訊.
	 * @param city
	 * @param view
	 * @return the hotel id by city view
	 */
	List<Map<String, Object>> getHotelIdByCityView(@Param("city") String city, @Param("view") String view);
}
