package eztravel.persistence.repository.oracle;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.common.CodeInfo;

public interface CodeRepository {

	/**
	 * 檢核帳號層級api
	 * @param userId
	 * @return List<CodeInfo>
	 */
	List<CodeInfo> checkEmp(@Param("userId") String userId);
}