package eztravel.persistence.repository.model;

import eztravel.util.vaildate.SizeByByte;

/**
 * The Class Operator.
 */
public class Operator {
	
	/** The creator. 建檔人員 */
	@SizeByByte(max=20, message="Creator cann't be more than 10 yards")
	private String creator;
	
	/** The create dt. 建檔時間 */
	@SizeByByte(max=14, message="CreateDt cann't be more than 14 yards")
	private String createDt;
	
	/** The moder. 異動人員 */
	@SizeByByte(max=20, message="Moder cann't be more than 10 yards")
	private String moder;
	
	/** The mod dt. 異動時間 */
	@SizeByByte(max=14, message="ModDt cann't be more than 14 yards")
	private String modDt;
	
	/**
	 * Gets the creator.
	 *
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}
	
	/**
	 * Sets the creator.
	 *
	 * @param creator the new creator
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	/**
	 * Gets the creates the dt.
	 *
	 * @return the creates the dt
	 */
	public String getCreateDt() {
		return createDt;
	}
	
	/**
	 * Sets the creates the dt.
	 *
	 * @param createDt the new creates the dt
	 */
	public void setCreateDt(String createDt) {
		this.createDt = createDt;
	}
	
	/**
	 * Gets the moder.
	 *
	 * @return the moder
	 */
	public String getModer() {
		return moder;
	}
	
	/**
	 * Sets the moder.
	 *
	 * @param moder the new moder
	 */
	public void setModer(String moder) {
		this.moder = moder;
	}
	
	/**
	 * Gets the mod dt.
	 *
	 * @return the mod dt
	 */
	public String getModDt() {
		return modDt;
	}
	
	/**
	 * Sets the mod dt.
	 *
	 * @param modDt the new mod dt
	 */
	public void setModDt(String modDt) {
		this.modDt = modDt;
	}
}
