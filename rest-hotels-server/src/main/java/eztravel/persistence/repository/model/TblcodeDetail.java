package eztravel.persistence.repository.model;

import eztravel.util.vaildate.SizeByByte;

public class TblcodeDetail extends Operator{
	
	@SizeByByte(max=3, message="Item Id cann't be more than 3 yards and not Null", canNull=false)
	private String itemId;
	
	@SizeByByte(max=5, message="Code Id cann't be more than 5 yards and not Null", canNull=false)
	private String codeId;
	
	@SizeByByte(max=150, message="Code Name cann't be more than 150 yards and not Null", canNull=false)
	private String codeName;
	
	@SizeByByte(max=150, message="Code Desc cann't be more than 150 yards and not Null", canNull=false)
	private String codeDesc;
	
	@SizeByByte(max=3, message="Code Type cann't be more than 3 yards and not Null", canNull=false)
	private String codeType;
	
	@SizeByByte(min=0, max=999, message="Display Seq cann't be more than 999", canNull=false)
	private Integer displaySeq;
	
	@SizeByByte(max=150, message="code Name'S cann't be more than 150 yards", canNull=true)
	private String codeNameS;

	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getCodeId() {
		return codeId;
	}
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getCodeDesc() {
		return codeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public Integer getDisplaySeq() {
		return displaySeq;
	}
	public void setDisplaySeq(Integer displaySeq) {
		this.displaySeq = displaySeq;
	}
	public String getCodeNameS() {
		return codeNameS;
	}
	public void setCodeNameS(String codeNameS) {
		this.codeNameS = codeNameS;
	}
}
