package eztravel.persistence.repository.util;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

public class EzTypeHandler<T> implements TypeHandler<T>{

	@Override
	public void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
		// TODO Auto-generated method stub
		
		switch(jdbcType){
			case VARCHAR:
				if(parameter == null || "".equals(parameter))
					ps.setString(i, " ");
				else
					ps.setString(i, (String) parameter);
				break;
			case INTEGER:
				if(parameter == null)
					ps.setInt(i, 0);
				else if(parameter instanceof String)
					ps.setInt(i, Integer.parseInt((String) parameter));
				else if(parameter instanceof BigDecimal)
					ps.setInt(i, ((BigDecimal) parameter).intValue());
				else
					ps.setInt(i, (Integer)parameter);
				break;
		case ARRAY:
			break;
		case BIGINT:
			break;
		case BINARY:
			break;
		case BIT:
			break;
		case BLOB:
			break;
		case BOOLEAN:
			break;
		case CHAR:
			break;
		case CLOB:
			break;
		case CURSOR:
			break;
		case DATE:
			break;
		case DECIMAL:
			break;
		case DOUBLE:
			break;
		case FLOAT:
			break;
		case LONGVARBINARY:
			break;
		case LONGVARCHAR:
			break;
		case NCHAR:
			break;
		case NCLOB:
			break;
		case NULL:
			break;
		case NUMERIC:
			if(parameter instanceof Integer)
				ps.setInt(i, (Integer) parameter);
			else if(parameter instanceof BigDecimal)
				ps.setBigDecimal(i, (BigDecimal)parameter);
			else
				ps.setInt(i, 0);
			break;
		case NVARCHAR:
			break;
		case OTHER:
			break;
		case REAL:
			break;
		case SMALLINT:
			break;
		case STRUCT:
			break;
		case TIME:
			break;
		case TIMESTAMP:
			break;
		case TINYINT:
			break;
		case UNDEFINED:
			break;
		case VARBINARY:
			break;
		default:
			break;
		}
	}

	@Override
	public T getResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T getResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
