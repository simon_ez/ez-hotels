package eztravel.persistence.repository.util;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

public class StringListTypeHandler extends BaseTypeHandler<List<String>> {

  @Override
  public List<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
    List<String> result = null;
    try {
      result = Arrays.asList(rs.getString(columnName).split(","));
    } catch (Exception e) {}
    return result;
  }

  @Override
  public List<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
    List<String> result = null;
    try {
      result = Arrays.asList(rs.getString(columnIndex).split(","));
    } catch (Exception e) {}
    return result;
  }

  @Override
  public List<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
    List<String> result = null;
    try {
      result = Arrays.asList(cs.getString(columnIndex).split(","));
    } catch (Exception e) {}
    return result;
  }

  @Override
  public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter,
      JdbcType jdbcType) throws SQLException {
    StringBuilder param = new StringBuilder();
    try {
      for (String p : parameter) {
        param.append(p);
        if (!p.equals(parameter.get(parameter.size() - 1))) {
          param.append(",");
        }
      }
    } catch (Exception e) {}
    ps.setString(i, param.toString());
  }

}
