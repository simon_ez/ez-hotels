package eztravel.persistence.repository.edb;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SampleRepository {

  /**
   * 查詢
   * @return List<Map<String, String>>
   */
  public List<Map<String, String>> query(@Param("value") String value);
  
  /**
   * 儲存
   * @return List<Map<String, String>>
   */
  public void insert(@Param("value") String value);
  
  /**
   * 更新
   * @return List<Map<String, String>>
   */
  public void update(@Param("value") String value);
  
  /**
   * 刪除
   * @return List<Map<String, String>>
   */
  public void delete(@Param("value") String value);
}
