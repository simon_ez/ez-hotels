package eztravel.persistence.repository.edb;

import org.apache.ibatis.annotations.Param;

public interface RoomQtyEdbRepository {

	/*
	 * 
	 */
	String getMinMasterPq(@Param("hotelId") String hotelId, @Param("masterRoomId") String masterRoomId, @Param("checkIn") String checkIn, @Param("checkOut") String checkOut);

}