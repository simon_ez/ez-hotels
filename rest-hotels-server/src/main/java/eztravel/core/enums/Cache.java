package eztravel.core.enums;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by jimin on 7/22/15.
 */
public class Cache<T> implements Serializable {

	private static final long serialVersionUID = -7527568225789847749L;

	private DateTime expiredTime;
	private T content;
	
	public enum ApiType {
		NORMAL, WEB, MOBILE, MWEB
	}

	public enum DataType {
		HOTEL, ROOM
	}

	/**
	 * @param content
	 * @param days
	 * @param hours
	 * @param minutes
	 */
	public Cache(T content, int days, int hours, int minutes) {
		DateTime currentDate = new DateTime();
		expiredTime = currentDate.plusDays(days).plusHours(hours).plusMinutes(minutes);
		this.content = content;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public DateTime getExpiredTime() {
		return expiredTime;
	}

	public void setExpiredTime(DateTime expiredTime) {
		this.expiredTime = expiredTime;
	}
}
