package eztravel.core.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import eztravel.rest.pojo.common.RestError;
import eztravel.rest.pojo.common.RestResource;
import eztravel.util.Redis;
import eztravel.util.exception.BsLogicException;

public abstract class CommonService {
	private static final Logger log = LoggerFactory.getLogger(CommonService.class);
	protected final static Redis redis = new Redis(9);
	protected final int sessionTimeOut = 10800;
	protected final String RedisSessionPath = "PLATFORM:session:"; //Redis session預設存放路徑
	protected final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd"); //此為常用格式，在此建立以備共用
	protected final SimpleDateFormat yyyyMMddHHmm = new SimpleDateFormat("yyyyMMddHHmm");
	protected final SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
	protected final String Y = "Y", N = "N", X = "X", COLON=":"; //常數

	@Value("${cache.flag}")
	protected String cacheFlag;
	
	@Autowired
	protected HttpServletRequest request;
		
	/**
	 * 耗時計算 by Thread
	 */
	Map<Long, Long> keepTime = new HashMap<Long, Long>();
	/**
	 * 設定Thread開始時間點
	 */
	public void startTx(){
		this.clearKeepTime();
		keepTime.put(Thread.currentThread().getId(), System.currentTimeMillis());
	}
	
	/**
	 * 返回Thread設定結束點的耗時
	 * @return
	 */
	public String endTx(){
		if(keepTime.containsKey(Thread.currentThread().getId())){
			Long useTime = System.currentTimeMillis() - keepTime.get(Thread.currentThread().getId());
			keepTime.remove(Thread.currentThread().getId());
			return String.valueOf(useTime);
		}else{
			return "";
		}
	}
	
	/**
	 *若記憶體佔用超過1小時，清除keepTime中留存的紀錄
	 * 2018-05-09 疑似可能多執行序再取key的時候內容被修改導致ConcurrentModificationException，暫先使用keys替代使用觀察
	 */
	private void clearKeepTime(){
		try{
			Set<Long> keys = keepTime.keySet();
			for(Long threadId: keys){
				if(System.currentTimeMillis() - keepTime.get(threadId) > (6000*60*60))
					keepTime.remove(threadId);
			}
		}catch(Exception e){
			//清除時間記錄若出錯不應影響作動紀錄結果
		}
	}
	
	/**
	 * [GET] 呼叫其他線別API
	 * @param url
	 * @param responseType
	 * @return
	 * @throws Exception
	 */
	public <T> List<T> doGet(String url, ParameterizedTypeReference<RestResource<T>> responseType) throws Exception {
		this.startTx();
		List<T> result = null;
		try{
			ResponseEntity<RestResource<T>> res = new RestTemplate().exchange(url, HttpMethod.GET, null, responseType);
			if (res != null) {
				log.debug("呼叫API(" + url + ")共花費 " + this.endTx() + " ms, HTTP Status：" + res.getStatusCode());
				if (HttpStatus.OK.equals(res.getStatusCode())){ // Http Status check
					RestResource<T> body = res.getBody();
					if(body != null){
						if(HttpStatus.OK.equals(body.getStatus())){ //EZ Status check
							//成功
							result = new ArrayList<T>();
						}else{
							List<RestError> errors = body.getErrors();
							if (errors != null) {
								for (RestError error : errors)
									log.warn("異常訊息: {}", error.getMessage());
							}
						}
					}else{
						result = new ArrayList<T>();
					}
				}else{
					throw new BsLogicException(url.concat("，Http Code: ") + res.getStatusCode());
				}
			}else{
				throw new BsLogicException(url.concat(": Content Null"));
			}
		}catch(Exception e){
			log.error("使用其他線別API異常: {}", e.toString());
			throw e;
		}
		return result;
	}

}
