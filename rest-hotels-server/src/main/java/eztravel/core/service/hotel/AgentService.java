/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.common
 * @FileName: AgentService.java
 * @author:   Kent
 * @date:     2017/6/13, 上午 11:13:47
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import org.apache.commons.lang3.StringUtils;

/**
 * The Interface AgentService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface AgentService {
	
  
    /**
     * Usf htl agent rule M.
     *
     * @param formulaCode the formula code
     * @param price the price
     * @param cost the cost
     * @return the int
     */
    int usfHtlAgentRuleM(String formulaCode, int price, int cost);
    
	/**
     * 系統商定價.
     *
     * @param rule the rule
     * @return int
     * @throws Exception the exception
     */
	int usfHtlAgentRuleM(Rule rule) throws Exception;
	
	/**
     * 取得系統商減價描述.
     *
     * @param prodNo the prod no
     * @param roomtypeNo the roomtype no
     * @param countryCd the country cd
     * @param agentType the agent type
     * @return String
     */
	String getAgentRuleDesc(String prodNo, String roomtypeNo, String countryCd, String agentType);
	
	/**
	   * set up agent type which is based on customer id
	   * 查出會員身份對應代碼 (A , E ,B開頭) 
	   * @param customerId
	   * @param source
	   * @param checkin
	   * 手機企業身份：
	   *     E21    企業初級APP-60
	   *     E22    企業中級APP-60
	   *     E23    企業高級APP-60
	   *     E24    企業初級APP當日入住
	   *     E25    企業中級APP當日入住
	   *     E26    企業高級APP當日入住
	   */
	String getAgentType(String customerId, String source, String checkin);
	
	/**
     * The Class Rule.
     * 
     * <pre>
     * 
     * </pre>
     */
	class Rule{
		
		/** The level. */
		private int level;
		
		/** The rule type 1. */
		private String ruleType1;
		
		/** The rule type 2. */
		private String ruleType2;
		
		/** The rule type 3. */
		private String ruleType3;
		
		/** The rule type 4. */
		private String ruleType4;
		
		/** The price. */
		private int price;
		
		/** The cost. */
		private int cost;
		
		/**
         * Gets the level.
         *
         * @return the level
         */
		public int getLevel() {
			return level;
		}
		
		/**
         * Sets the level.
         *
         * @param level the new level
         */
		public void setLevel(int level) {
			this.level = level;
		}
		
		/**
         * Gets the rule type 1.
         *
         * @return the rule type 1
         */
		public String getRuleType1() {
			return ruleType1;
		}
		
		/**
         * Sets the rule type 1.
         *
         * @param ruleType1 the new rule type 1
         */
		public void setRuleType1(String ruleType1) {
			this.ruleType1 = ruleType1;
		}
		
		/**
         * Gets the rule type 2.
         *
         * @return the rule type 2
         */
		public String getRuleType2() {
			return ruleType2;
		}
		
		/**
         * Sets the rule type 2.
         *
         * @param ruleType2 the new rule type 2
         */
		public void setRuleType2(String ruleType2) {
			this.ruleType2 = ruleType2;
		}
		
		/**
         * Gets the rule type 3.
         *
         * @return the rule type 3
         */
		public String getRuleType3() {
			return ruleType3;
		}
		
		/**
         * Sets the rule type 3.
         *
         * @param ruleType3 the new rule type 3
         */
		public void setRuleType3(String ruleType3) {
			this.ruleType3 = ruleType3;
		}
		
		/**
         * Gets the rule type 4.
         *
         * @return the rule type 4
         */
		public String getRuleType4() {
			return ruleType4;
		}
		
		/**
         * Sets the rule type 4.
         *
         * @param ruleType4 the new rule type 4
         */
		public void setRuleType4(String ruleType4) {
			this.ruleType4 = ruleType4;
		}
		
		/**
         * Gets the price.
         *
         * @return the price
         */
		public int getPrice() {
			return price;
		}
		
		/**
         * Sets the price.
         *
         * @param price the new price
         */
		public void setPrice(int price) {
			this.price = price;
		}
		
		/**
         * Gets the cost.
         *
         * @return the cost
         */
		public int getCost() {
			return cost;
		}
		
		/**
         * Sets the cost.
         *
         * @param cost the new cost
         */
		public void setCost(int cost) {
			this.cost = cost;
		}
	}
	
	/**
     * 計算符.
     *
     * @author Simon
     */
	enum Breakets {
		
		/** The add. */
		ADD("+"),
		
		/** The subtra. */
		SUBTRA("-"),
		
		/** The multi. */
		MULTI("*"),
		
		/** The div. */
		DIV("/"),
		
		/** The amount. */
		AMOUNT("="),
		
		/** The floor. */
		FLOOR("無條件捨去"),
		
		/** The price. */
		PRICE("P"),
		
		/** The cost. */
		COST("C"),
		
		/** The space one. */
		SPACE_ONE(" "),
		
		/** The space. */
		SPACE("");
		
		/** The breakets. */
		String breakets;
		
		/**
         * Instantiates a new breakets.
         *
         * @param breakets the breakets
         */
		Breakets(String breakets){
			this.breakets = breakets;
		}
		
		/**
         * Gets the breakets.
         *
         * @return the breakets
         */
		public String getBreakets() {
			return breakets;
		}
	}
	
	/**
     * 標記計算符.
     *
     * @author Simon
     */
	enum BreaketsMark {
		
		/** The left. */
		LEFT('('),
		
		/** The right. */
		RIGHT(')'),
		
		/** The add. */
		ADD('+'),
		
		/** The subtra. */
		SUBTRA('-'),
		
		/** The multi. */
		MULTI('*'),
		
		/** The div. */
		DIV('/'),
		
		/** The amount. */
		AMOUNT('=');
		
		/** The breakets. */
		char breakets;
		
		/**
         * Instantiates a new breakets mark.
         *
         * @param breakets the breakets
         */
		BreaketsMark(char breakets){
			this.breakets = breakets;
		}
		
		/**
         * Gets the breakets.
         *
         * @return the breakets
         */
		public char getBreakets() {
			return breakets;
		}
	}
	
	/**
     * 計算公式代碼.
     *
     * @author Simon
     */
	enum RuleType {
		
		/** The r000. */
		R000("R00.0"),
		
		/** The r011. */
		R011("R01.1"), 
 /** The r012. */
 R012("R01.2"),	
	/** The r013. */
	R013("R01.3"), 
 /** The r014. */
 R014("R01.4"),	
	/** The r015. */
	R015("R01.5"),
		
		/** The r016. */
		R016("R01.6"), 
 /** The r020. */
 R020("R02.0"),
		
		/** The r021. */
		R021("R02.1"), 
 /** The r022. */
 R022("R02.2"), 
 /** The r023. */
 R023("R02.3"), 
 /** The r024. */
 R024("R02.4"), 
 /** The r025. */
 R025("R02.5"),
		
		/** The r026. */
		R026("R02.6"), 
 /** The r027. */
 R027("R02.7"), 
 /** The r028. */
 R028("R02.8"), 
 /** The r029. */
 R029("R02.9"),
		
		/** The r02a. */
		R02A("R02.A"), 
 /** The r02b. */
 R02B("R02.B"),	
	/** The r02c. */
	R02C("R02.C"), 
 /** The r02d. */
 R02D("R02.D"),	
	/** The r02e. */
	R02E("R02.E"),
		
		/** The r02f. */
		R02F("R02.F"),
		
		/** The r031. */
		R031("R03.1"), 
 /** The r032. */
 R032("R03.2"),
/** The r033. */
R033("R03.3"),
/** The r034. */
R034("R03.4"),
		
		/** The r041. */
		R041("R04.1"),
		
		/** The r050. */
		R050("R05.0"),
		
		/** The r051. */
		R051("R05.1"), 
 /** The r052. */
 R052("R05.2"), 
 /** The r053. */
 R053("R05.3"), 
 /** The r054. */
	R054("R05.4"),
	R055("R05.5"),
	R056("R05.6"),
	R057("R05.7"),
	R058("R05.8"),
	R059("R05.9"),
	R05A("R05.A"),

		/** The r060. */
		R060("R06.0"), 
		
		/** The r061. */
		R061("R06.1"), 
 /** The r062. */
 R062("R06.2"), 
 /** The r063. */
 R063("R06.3"), 
 /** The r064. */
 R064("R06.4"), 
 /** The r065. */
 R065("R06.5"),
		
		/** The r066. */
		R066("R06.6"), 
 /** The r067. */
 R067("R06.7"),	
	/** The r068. */
	R068("R06.8"), 
 /** The r069. */
 R069("R06.9"),
		
		/** The r06a. */
		R06A("R06.A"), 
 /** The r06b. */
 R06B("R06.B"),	
	/** The r06c. */
	R06C("R06.C"), 
 /** The r06d. */
 R06D("R06.D"),	
	/** The r06e. */
	R06E("R06.E"),
		
		/** The r06f. */
		R06F("R06.F"), 
 /** The r06g. */
 R06G("R06.G"),	
	/** The r06h. */
	R06H("R06.H"), 
 /** The r06i. */
 R06I("R06.I"),
 	R06J("R06.J"),
 	R06K("R06.K"),
 	R06L("R06.L"),
 	R06M("R06.M"),
 	R06N("R06.N"),
 	R06O("R06.O"),
 	R06P("R06.P"),
 	R06Q("R06.Q"),
 	R06R("R06.R"),
 	R06S("R06.S"),
		
		/** The r071. */
		R071("R07.1"), 
 /** The r072. */
 R072("R07.2"), 
 /** The r073. */
 R073("R07.3"), 
 /** The r074. */
 R074("R07.4"),	
	/** The r075. */
	R075("R07.5"),
		
		/** The r076. */
		R076("R07.6"), 
 /** The r077. */
 R077("R07.7"),	
	/** The r078. */
	R078("R07.8"),
		
		/** The r099. */
		R099("R09.9");
		
		/** The code. */
		String code;
		
		/**
         * Instantiates a new rule type.
         *
         * @param code the code
         */
		RuleType(String code){
			this.code = code;
		}
		
		/**
         * Gets the code.
         *
         * @return the code
         */
		public String getCode() {
			return code;
		}
		
		/**
         * Match.
         *
         * @param ruleType the rule type
         * @return the rule type
         */
		public static RuleType match(String ruleType){
			for(RuleType enumObj: RuleType.values()){
				if(StringUtils.equals(enumObj.getCode(), ruleType))
					return enumObj;
			}
			return null;
		}
	}
}
