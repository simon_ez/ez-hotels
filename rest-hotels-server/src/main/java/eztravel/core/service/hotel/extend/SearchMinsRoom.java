/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.hotel
 * @FileName: HotelSearch.java
 * @author:   cano0530
 * @date:     2015/10/28, 下午 12:15:27
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */

package eztravel.core.service.hotel.extend;

import java.io.Serializable;

import eztravel.rest.pojo.hotel.Room;



/**
 * <pre> HotelSearch, 飯店查詢列表用飯店資料. </pre>
 *
 * @author Kent
 */
public class SearchMinsRoom  extends Room implements Serializable{

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2797078765465252752L;
  private String discountYn;


  public String getDiscountYn() {
    return discountYn;
  }

  public void setDiscountYn(String discountYn) {
    this.discountYn = discountYn;
  }
  
  public Room getRoom() {
    Room room = new Room();
    room.setBathtub(this.getBathtub());
    room.setBathtubDesc(this.getBathtubDesc());
    room.setBedDescription(this.getBedDescription());
    room.setBedSize(this.getBedSize());
    room.setBreakfast(this.getBreakfast());
    room.setBreakfastDesc(this.getBreakfastDesc());
    room.setBreakfastFeeDescription(this.getBreakfastFeeDescription());
    room.setBreakfastType(this.getBreakfastType());
    room.setCancelIconDescs(this.getCancelIconDescs());
    room.setCancelDescs(this.getCancelDescs());
    room.setCancelLimit(this.getCancelLimit());
    room.setDayPrice(this.getDayPrice());
    room.setGuestCounts(this.getGuestCounts());
    room.setInternetDescription(this.getInternetDescription());
    room.setInternetFeeDescription(this.getInternetFeeDescription());
    room.setLimitStayDays(this.getLimitStayDays());
    room.setMobileSalePriceAvg(this.getMobileSalePriceAvg());
    room.setSendAddressYn(this.getSendAddressYn());
    room.setPetFeeDescription(this.getPetFeeDescription());
    room.setPetYn(this.isPetYn());
    room.setPhotoURLs(this.getPhotoURLs());
    room.setPriceAvg(this.getPriceAvg());
    room.setProjectRemark(this.getProjectRemark());
    room.setPromoInfo(this.getPromoInfo());
    room.setRoomArea(this.getRoomArea());
    room.setRoomCheckinTime(this.getRoomCheckinTime());
    room.setRoomCheckinTimeWeekday(this.getRoomCheckinTimeWeekday());
    room.setRoomCheckoutTime(this.getRoomCheckoutTime());
    room.setRoomCheckoutTimeWeekday(this.getRoomCheckoutTimeWeekday());
    room.setRoomDescription(this.getRoomDescription());
    room.setRoomFacilities(this.getRoomFacilities());
    room.setRoomId(this.getRoomId());
    room.setRoomName(this.getRoomName());
    room.setRoomProjectName(this.getRoomProjectName());
    room.setRoomQty(this.getRoomQty());
    room.setRoomstatus(this.getRoomstatus());
    room.setRoomtype(this.getRoomtype());
    room.setRoomTypeNum(this.getRoomTypeNum());
    room.setSalePriceAvg(this.getSalePriceAvg());
    room.setHighestSalePriceAvg(this.getHighestSalePriceAvg());
    room.setSellPoints(this.getSellPoints());
    room.setWifi(this.getWifi());
    room.setWindowtype(this.getWindowtype());
    room.setCancelChangeDesc(this.getCancelChangeDesc());
    room.setChargeDateList(this.getChargeDateList());
    room.setMasterId(this.getMasterId());
    room.setMasterIdQty(this.getMasterIdQty());
    room.setCommonQtyYN(this.getCommonQtyYN());
    room.setIsSiteMinder(this.getIsSiteMinder());
    return room;
  }
}
