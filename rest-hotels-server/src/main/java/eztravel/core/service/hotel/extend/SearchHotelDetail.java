package eztravel.core.service.hotel.extend;

import eztravel.persistence.repository.oracle.SearchRepository;

/**
 * Created by jimin on 6/9/15.
 */
public class SearchHotelDetail implements SearchHotelInfo {

	private SearchRepository searchRepository;

	public SearchHotelDetail(SearchRepository searchRepository) {
		super();
		this.searchRepository = searchRepository;
	}
	
	public <T> T getInfo(String hotelId) {
		return (T) searchRepository.getHotelInfo(hotelId);
	}
}
