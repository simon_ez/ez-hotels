/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.common
 * @FileName: OrderCommonService.java
 * @author:   Kent
 * @date:     2015/6/5, 下午 04:23:29
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.List;

import eztravel.core.pojo.DayPriceExtend;
import eztravel.core.pojo.HtlMinPrice;
import eztravel.core.pojo.HtlMinPriceExtend;
import eztravel.rest.pojo.hotel.DayPrice;
import eztravel.rest.pojo.hotel.PromoInfo;

/**
 * <pre>
 * OrderCommonService, TODO: add Class Javadoc here.
 * </pre>
 *
 * @author Kent
 */
public interface RoomCommonService {

	List<DayPrice> caculatRooomDailyPriceList(List<DayPriceExtend> roomPrices, List<PromoInfo> hotelPromoList, String agentType, String checkin, String checkout, String hotelId, String roomId);

	/**
	 * 計算促銷價供應商最低價.
	 * 
	 * @param htlsMinPrices
	 * @param agentType
	 * @param checkin
	 * @param checkout
	 * @param hotelIds
	 * @param type
	 * @return List<HtlMinPrice>
	 */
	List<List<HtlMinPrice>> caculatHotelLowPriceList(List<List<HtlMinPriceExtend>> htlsMinPrices, String agentType, String checkin, String checkout, List<List<String>> hotelIds, String type);
}
