package eztravel.core.service.hotel;


public interface RedisService {

	/**
	 * 瀏覽商品數量統計
	 * @param city
	 * @param view
	 * @param hotelNo
	 */
	void execBrowseRecord(String hotelNo);
	
	/**
	 * Redis記錄類別設定
	 * @author Simon
	 */
	enum RecordType{
		TIMERECORD("最後更新統計時間", "timeRecord:", "Record", 0, 0, 0),
		BOOKING("訂房數量統計", "bookingRecord:", "Order", 1, 60*60*24*7, 60),
		BROWSE("瀏覽數量統計", "browseRecord:", "Browse", 1, 60*60*24*8, 60),
		BOOKINGNEAR("最近被預訂設定", "", "", 0, 60*60*24, 0),
		AREA_HAVING_HOTELS("關聯城市景點飯店", "hotelsByArea:", "Component", 0, 60*60*24, 0),
		HOTELS_BY_UPYN("儲存依上架狀態的飯店清單", "hotelsByUpyn:", "", 0, 60*3, 0)
		;

		RecordType(String type, String path, String contentKey, int minCount, int overTime, int queryCheckTime){
			this.type = type;
			this.path = path;
			this.contentKey = contentKey;
			this.minCount = minCount;
			this.overTime = overTime;
			this.queryCheckTime = queryCheckTime;
		}
		
		private String type; // 類別說明
		private String path; //使用redis title 鍵值
		private String contentKey; //Value使用的鍵值
		private int minCount; //最低保留筆數(最低限度)
		private int overTime; //大於最低限度時，逾時時效設定(Seconds)
		private int queryCheckTime; //查詢時，逾時更新設定(Minute)

		public String getType() {
			return type;
		}
		public String getPath() {
			return path;
		}
		public int getMinCount() {
			return minCount;
		}
		public int getOverTime() {
			return overTime;
		}
		public String getContentKey() {
			return contentKey;
		}
		public int getQueryCheckTime() {
			return queryCheckTime;
		}
	}
	
	/**
	 * 供統計記錄查詢排序使用
	 * @author Simon
	 */
	class StatisticalHtl {
		String key;
		Integer count;
		String lastBookingTime;
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public Integer getCount() {
			return count;
		}
		public void setCount(Integer count) {
			this.count = count;
		}
		public String getLastBookingTime() {
			return lastBookingTime;
		}
		public void setLastBookingTime(String lastBookingTime) {
			this.lastBookingTime = lastBookingTime;
		}
	}
}
