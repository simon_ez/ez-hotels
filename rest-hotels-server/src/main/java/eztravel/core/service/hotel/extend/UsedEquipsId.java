package eztravel.core.service.hotel.extend;

import java.util.List;

import eztravel.persistence.repository.oracle.SearchRepository;

/**
 * Created by jimin on 7/22/15.
 */
public class UsedEquipsId {

	private static List<Integer> usedEquips;

	private UsedEquipsId(SearchRepository searchRepository) {
		usedEquips = searchRepository.getUsedEquipId();
	}

	public synchronized static List<Integer> getUsedEquipsId(SearchRepository searchRepository) {
		if (usedEquips == null || usedEquips.isEmpty()) 
			new UsedEquipsId(searchRepository);
		return usedEquips;
	}
}
