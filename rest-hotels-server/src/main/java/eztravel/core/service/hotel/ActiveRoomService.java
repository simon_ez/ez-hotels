/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: ActiveRoomService.java
 * @author:   cano0530
 * @date:     2017/3/9, 下午 05:33:02
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.List;

import eztravel.rest.pojo.hotel.Room;

public interface ActiveRoomService {

	/**
	 * 更新寫入cache.
	 * 
	 * @param hotelId
	 * @param cityCd
	 * @param travelType
	 * @param roomTypes
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 * @param roomList
	 * @param srcAgentType
	 * 
	 *  1. 只更新產品頁所過來的城市,入住日,退房日還有身份的cache資料
	 */
	void AsyncHotelList(String hotelId, String cityCd, String travelType, List<Integer> roomTypes, String checkin, String checkout, String customerId, String source, List<Room> roomList, String srcAgentType);

}
