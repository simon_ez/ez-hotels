package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.pojo.CancelDatePrice;
import eztravel.core.pojo.CancelDetail;
import eztravel.core.pojo.CustomerDB;
import eztravel.persistence.repository.oracle11g.Erpdb11gCancelRepository;
import eztravel.persistence.repository.oracle11g.Erpdb11gSearchRepository;
import eztravel.rest.pojo.hotel.DayPrice;
import eztravel.rest.pojo.hotel.Room;
import eztravel.util.UtilDate;

public class CancelServiceImpl implements CancelService {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CancelServiceImpl.class);
	
	@Autowired
	private Erpdb11gCancelRepository erpdb11gCancelRepository;
	@Autowired
	private Erpdb11gSearchRepository erpdb11gSearchRepository;

	/**
	 * 取消規則描述
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param nationality
	 * @param source ex:agentType A01
	 * @return List
	 */
	@Override
	public List<String> getCancelDescs(final String hotelId, String roomId, final int roomQty, final String checkin, final String checkout, String customerId, String nationality, String source, String isErp) {
		CancelDatePrice canceldetail = new CancelDatePrice();
		List<String> cancelType = new ArrayList<String>();
		long startTime = System.currentTimeMillis();
		logger.debug("### getCancelDetail START ###");
		try {
			boolean changeType = this.getCancelType(hotelId, roomId, checkin, checkout);
			if (changeType) {
				cancelType.add("EXCEPTIONCANCEL");
			} else {
				canceldetail = this.CancelDetail(hotelId, roomId, roomQty, checkin, checkout, customerId, nationality, source, isErp);
				if (canceldetail != null) {
					int totalprice = canceldetail.getTotalSalePrice();
					int count = 0;
					String desc = "";
					String orderDate = UtilDate.getToday("yyyyMMdd");
					for (DayPrice cancelcharge : canceldetail.getCancelDayPrice()) {
						if (cancelcharge.getDay().equals(orderDate)) {
							if (cancelcharge.getSalePrice() == 0) {
								for (DayPrice charge : canceldetail.getCancelDayPrice()) {
									if (charge.getSalePrice() == 0) {
										if (UtilDate.getDiffDays(orderDate, charge.getDay()) >= 0) {// 過濾超過訂購日期
											String date = UtilDate.toSimpleDate(charge.getDay());
											desc = "FREE" + "," + date;
										}
									}
								}
								break;
							} else if (cancelcharge.getSalePrice() == totalprice) {
								for (DayPrice charge : canceldetail.getCancelDayPrice()) {
									if (charge.getSalePrice() == totalprice) {
										desc = "NOCANCEL";
										break;
									}
								}
								break;
							} else {
								desc = "CANCELRULE";
								break;
							}
						} else {
							count++;
							if (count == canceldetail.getCancelDayPrice().size()) {
								int size = canceldetail.getCancelDayPrice().size();
								int chargeprice = canceldetail.getCancelDayPrice().get(size - 1).getSalePrice();
								if (chargeprice == totalprice) {
									desc = "NOCANCEL";
									break;
								} else if (chargeprice == 0) {
									String chargedate = canceldetail.getCancelDayPrice().get(size - 1).getDay();
									String date = UtilDate.toSimpleDate(chargedate);
									desc = "FREE" + "," + date;
									break;
								} else {
									desc = "CANCELRULE";
									break;
								}
							} else {

							}
						}
					}
					cancelType.add(desc);
				} else {
					cancelType.add(null);
				}
			}
		} catch (Exception e) {
			logger.error("getCancelDescs Error ");
			logger.error(e.getMessage(), e);
			cancelType.add(null);
		}
		logger.debug("### getCancelDescs END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return cancelType;
	}

	/**
	 * 更改規定描述
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param checkIn
	 * @param checkOut
	 * @return
	 */
	public List<String> getCancelChangeDesc(String hotelId, String roomId, String checkIn, String checkOut) {
		long startTime = System.currentTimeMillis();
		logger.debug("### getCancelChangeDesc START ###");
		List<String> result = new ArrayList<String>();
		List<CancelDetail> canceldetail = new ArrayList<CancelDetail>();
		int before = 0;
		boolean changeType = this.getCancelType(hotelId, roomId, checkIn, checkOut);
		try {
			if (!changeType) {
				canceldetail = erpdb11gCancelRepository.getCancelChangeForRoom(hotelId, roomId);
				if (canceldetail != null && canceldetail.size() > 0) {
					for (int i = 0; i < canceldetail.size(); i++) {
						if (canceldetail.get(i) != null && canceldetail.get(i).getRuleBefore() != null) {
							before = (canceldetail.get(i).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
						} else {
							canceldetail = erpdb11gCancelRepository.getCancelChangeForHotel(hotelId);
							if (canceldetail != null && canceldetail.size() > 0) {
								for (int j = 0; j < canceldetail.size(); j++) {
									if (canceldetail.get(j) != null && canceldetail.get(j).getRuleBefore() != null) {
										before = (canceldetail.get(j).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
									} else {
										result.add(null);
										return result;
									}
								}
								String beforedate = UtilDate.getBeforeDate(checkIn, before); // 前幾天的日期
								String today = UtilDate.getToday("yyyyMMdd");
								int diffday = UtilDate.getDiffDays(today, beforedate);
								if (diffday > 0) {
									String str = "若需更改訂房，請於" + UtilDate.getDateTransform(beforedate) + "前辦理(限原飯店更改、限更改一次)，如有價差多退少補，經更改後則無法取消，謝謝";
									result.add(str);
								} else {
									// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
									result.add(null);
								}
							} else {
								// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
								result.add(null);
							}
							return result;
						}
					}
					String beforedate = UtilDate.getBeforeDate(checkIn, before); // 前幾天的日期
					String today = UtilDate.getToday("yyyyMMdd");
					int diffday = UtilDate.getDiffDays(today, beforedate);
					if (diffday > 0) {
						String str = "若需更改訂房，請於" + UtilDate.getDateTransform(beforedate) + "前辦理(限原飯店更改、限更改一次)，如有價差多退少補，經更改後則無法取消，謝謝";
						result.add(str);
					} else {
						// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
						result.add(null);
					}
				} else {
					canceldetail = erpdb11gCancelRepository.getCancelChangeForHotel(hotelId);
					if (canceldetail != null && canceldetail.size() > 0) {
						for (int i = 0; i < canceldetail.size(); i++) {
							if (canceldetail.get(i) != null && canceldetail.get(i).getRuleBefore() != null) {
								before = (canceldetail.get(i).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
							} else {
								result.add(null);
								return result;
							}
						}
						String beforedate = UtilDate.getBeforeDate(checkIn, before); // 前幾天的日期
						String today = UtilDate.getToday("yyyyMMdd");
						int diffday = UtilDate.getDiffDays(today, beforedate);
						if (diffday > 0) {
							String str = "若需更改訂房，請於" + UtilDate.getDateTransform(beforedate) + "前辦理(限原飯店更改、限更改一次)，如有價差多退少補，經更改後則無法取消，謝謝";
							result.add(str);
						} else {
							// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
							result.add(null);
						}
					} else {
						// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
						result.add(null);
					}
				}
			} else {
				// String str= "經預定完成後恕無法更改訂房，造成您的不便，敬請見諒";
				result.add(null);
			}
		} catch (Exception e) {
			logger.error("取得更改規定 Error ");
			logger.error(e.getMessage(), e);
			result.add(null);
			return result;
		}
		logger.debug("### getCancelChangeDesc END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return result;

	}

	/**
	 * 是否為例外取消規則
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param checkin
	 * @param checkout
	 * @return boolean
	 */
	public boolean getCancelType(final String hotelId, String roomId, final String checkin, final String checkout) {
		List<CancelDetail> canceldetail = new ArrayList<CancelDetail>();
		boolean cancelExcFlag = false;
		boolean result = false;
		long startTime = System.currentTimeMillis();
		logger.debug("### getCancelType START ###");
		try {
			// 取得房型每日最低價格
			List<String> datelist = this.getlistCheckInDay(checkin, checkout);
			for (String checkDay : datelist) {// ex:入住1/4~1/6,會先看1/5取消規定再往前推
				String checkinDT = checkDay;
				// 先看是否符合cancel exception
				canceldetail = erpdb11gCancelRepository.getCancelExcep(hotelId, roomId, checkinDT);
				if (canceldetail != null && canceldetail.size() > 0) {
					String refundType = canceldetail.get(0).getRefundType();
					if (refundType.equals("D")) {// 全不退
						cancelExcFlag = true;
					}
				} else {
					canceldetail = erpdb11gCancelRepository.getCancelExcepCommon(hotelId, checkinDT);
					if (canceldetail != null && canceldetail.size() > 0) {
						String refundType = canceldetail.get(0).getRefundType();
						if (refundType.equals("D")) {// 全不退
							cancelExcFlag = true;
						}
					}
				}
			}
			result = cancelExcFlag;
		} catch (Exception e) {
			logger.error("getCancelType Error ");
			logger.error(e.getMessage(), e);
		}
		logger.debug("### getCancelType END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return result;
	}

	/**
	 * 每日取消規則價錢
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param nationality
	 * @param source
	 *            ex:agentType A01
	 * @return CancelDatePrice
	 */
	public CancelDatePrice CancelDetail(final String hotelId, String roomId, final int roomQty, final String checkin, final String checkout, String customerId, String nationality, String agentType, String isERP) {
		List<CancelDetail> canceldetail = new ArrayList<CancelDetail>();
		long startTime = System.currentTimeMillis();
		logger.debug("### CancelDetail START ###");
		Room room = new Room();
		List<DayPrice> dailyPriceResults = new ArrayList<DayPrice>();
		CancelDatePrice cancelDatePriceLists = new CancelDatePrice();
		try {
			// 取得房型每日最低價格
			dailyPriceResults = erpdb11gCancelRepository.getRoomDailyPriceForCancel(hotelId, roomId, roomQty, checkin, checkout, "TW", (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType, isERP);
			room.setDayPrice(dailyPriceResults);
			int totalPrice = 0;
			for (DayPrice checkDay : room.getDayPrice()) {
				totalPrice = totalPrice + checkDay.getSalePrice();
			}
			String firsCheckIn = checkin;
			List<String> checkedDateList = new ArrayList<String>();
			for (DayPrice checkDay : room.getDayPrice()) {// ex:入住1/4~1/5,會先看1/5取消規定再往前推
				String checkinDT = checkDay.getDay();
				int checkinPrice = checkDay.getSalePrice();
				// 先看是否符合cancel exception
				canceldetail = erpdb11gCancelRepository.getCancelExcep(hotelId, roomId, checkinDT);
				if (canceldetail != null && canceldetail.size() > 0) {
					String refundType = canceldetail.get(0).getRefundType();
					if (refundType.equals("D")) {// 全不退
						int value = 1;
						caculateExceptionCancelDateList(hotelId, roomId, cancelDatePriceLists, checkinDT, checkinPrice, room, value, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
					} else if (refundType.equals("R")) {// 全退
						int value = 0;
						caculateExceptionCancelDateList(hotelId, roomId, cancelDatePriceLists, checkinDT, checkinPrice, room, value, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
					}
				} else {
					// 看是否符合cancel exception 公版
					canceldetail = erpdb11gCancelRepository.getCancelExcepCommon(hotelId, checkinDT);
					if (canceldetail != null && canceldetail.size() > 0) {
						String refundType = canceldetail.get(0).getRefundType();
						if (refundType.equals("D")) {// 全不退
							int value = 1;
							caculateExceptionCancelDateList(hotelId, roomId, cancelDatePriceLists, checkinDT, checkinPrice, room, value, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
						} else if (refundType.equals("R")) {// 全退
							int value = 0;
							caculateExceptionCancelDateList(hotelId, roomId, cancelDatePriceLists, checkinDT, checkinPrice, room, value, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
						}
					} else {
						// 在看是否符合cancel 房型取消
						canceldetail = erpdb11gCancelRepository.getCancelForRoomtypeNo(hotelId, roomId, checkinDT, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
						if (canceldetail != null && canceldetail.size() > 0) {
							if (cancelDatePriceLists.getCancelDayPrice() == null || cancelDatePriceLists.getCancelDayPrice().size() == 0) {
								List<String> dateList = caculateCancelDateList(hotelId, roomId, room.getDayPrice(), (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
								// 塞入所有取消日期列表
								List<DayPrice> dateprice = new ArrayList<DayPrice>();
								for (int i = 0; i < dateList.size(); i++) {
									DayPrice mdayprice = new DayPrice();
									mdayprice.setDay(dateList.get(i));
									dateprice.add(mdayprice);
								}
								cancelDatePriceLists.setCancelDayPrice(dateprice);
							}
							List<DayPrice> mcancelDatePriceLists = cancelDatePriceLists.getCancelDayPrice();
							mcancelDatePriceLists = caculateCancelCharge(canceldetail, checkinDT, mcancelDatePriceLists, checkinPrice, firsCheckIn, room.getDayPrice(), checkedDateList);
							checkedDateList.add(checkinDT);
							cancelDatePriceLists.setCancelDayPrice(mcancelDatePriceLists);
						} else {
							// 在看是否符合cancel 房型公版取消
							canceldetail = erpdb11gCancelRepository.getCancelForRoomtypeX(hotelId, roomId, checkinDT, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
							if (canceldetail != null && canceldetail.size() > 0) {
								if (cancelDatePriceLists.getCancelDayPrice() == null || cancelDatePriceLists.getCancelDayPrice().size() == 0) {
									List<String> dateList = caculateCancelDateList(hotelId, roomId, room.getDayPrice(), (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
									// 塞入所有取消日期列表
									List<DayPrice> dateprice = new ArrayList<DayPrice>();
									for (int i = 0; i < dateList.size(); i++) {
										DayPrice mdayprice = new DayPrice();
										mdayprice.setDay(dateList.get(i));
										dateprice.add(mdayprice);
									}
									cancelDatePriceLists.setCancelDayPrice(dateprice);
								}
								List<DayPrice> mcancelDatePriceLists = cancelDatePriceLists.getCancelDayPrice();
								mcancelDatePriceLists = caculateCancelCharge(canceldetail, checkinDT, mcancelDatePriceLists, checkinPrice, firsCheckIn, room.getDayPrice(), checkedDateList);
								checkedDateList.add(checkinDT);
								cancelDatePriceLists.setCancelDayPrice(mcancelDatePriceLists);
							} else {
								// 在看是否符合cancel 飯店取消
								canceldetail = erpdb11gCancelRepository.getCancelForProdNo(hotelId, roomId, checkinDT, (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
								if (canceldetail != null && canceldetail.size() > 0) {
									if (cancelDatePriceLists.getCancelDayPrice() == null || cancelDatePriceLists.getCancelDayPrice().size() == 0) {
										List<String> dateList = caculateCancelDateList(hotelId, roomId, room.getDayPrice(), (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
										// 塞入所有取消日期列表
										List<DayPrice> dateprice = new ArrayList<DayPrice>();
										for (int i = 0; i < dateList.size(); i++) {
											DayPrice mdayprice = new DayPrice();
											mdayprice.setDay(dateList.get(i));
											dateprice.add(mdayprice);
										}
										cancelDatePriceLists.setCancelDayPrice(dateprice);
									}
									List<DayPrice> mcancelDatePriceLists = cancelDatePriceLists.getCancelDayPrice();
									mcancelDatePriceLists = caculateCancelCharge(canceldetail, checkinDT, mcancelDatePriceLists, checkinPrice, firsCheckIn, room.getDayPrice(), checkedDateList);
									checkedDateList.add(checkinDT);
									cancelDatePriceLists.setCancelDayPrice(mcancelDatePriceLists);
								} else {
									// 在看是否符合cancel 飯店公版取消
									canceldetail = erpdb11gCancelRepository.getCancelForProdNoX((agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
									if (canceldetail != null && canceldetail.size() > 0) {
										if (cancelDatePriceLists.getCancelDayPrice() == null || cancelDatePriceLists.getCancelDayPrice().size() == 0) {
											List<String> dateList = caculateCancelDateList(hotelId, roomId, room.getDayPrice(), (agentType == null || agentType.length() == 0) ? this.setCustomerInfo(customerId) : agentType);
											// 塞入所有取消日期列表
											List<DayPrice> dateprice = new ArrayList<DayPrice>();
											for (int i = 0; i < dateList.size(); i++) {
												DayPrice mdayprice = new DayPrice();
												mdayprice.setDay(dateList.get(i));
												dateprice.add(mdayprice);
											}
											cancelDatePriceLists.setCancelDayPrice(dateprice);
										}
										List<DayPrice> mcancelDatePriceLists = cancelDatePriceLists.getCancelDayPrice();
										mcancelDatePriceLists = caculateCancelCharge(canceldetail, checkinDT, mcancelDatePriceLists, checkinPrice, firsCheckIn, room.getDayPrice(), checkedDateList);
										checkedDateList.add(checkinDT);
										cancelDatePriceLists.setCancelDayPrice(mcancelDatePriceLists);
									}
								}
							}
						}
					}
				}
			}
			cancelDatePriceLists.setTotalSalePrice(totalPrice);
		} catch (Exception e) {
			logger.error("CancelDetail Error ");
			logger.error(e.getMessage(), e);
		}
		logger.debug("### CancelDetail END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return cancelDatePriceLists;
	}
	
	/**
	 * 
	 * @param checkIn
	 * @param checkOut
	 * @return
	 */
	private List<String> getlistCheckInDay(String checkIn, String checkOut) {
		List<String> datelist = new ArrayList<String>();
		int diffday = UtilDate.getDiffDays(checkIn, checkOut);
		for (int i = 0; i < diffday; i++) {
			String date = UtilDate.getAfterDate(checkIn, i);
			datelist.add(date);
		}
		return datelist;
	}
	
	/**
	 * set up agent type which is based on customer id
	 * @param customerId
	 */
	private String setCustomerInfo(String customerId) {
		String agentType = "A01";
		if (customerId != null && customerId.trim().length() > 0) {
			CustomerDB customer = erpdb11gSearchRepository.getCustomerInfo(customerId);
			agentType = customer.getAgentType();
		}
		return agentType;
	}
	
	/**
	 * 例外取消規則每日價錢
	 * @param hotelId
	 * @param roomId
	 * @param cancelDatePriceLists
	 * @param checkinDT
	 * @param checkinPrice
	 * @param room
	 * @param value
	 * @return CancelDatePrice
	 */
	public CancelDatePrice caculateExceptionCancelDateList(String hotelId, String roomId, CancelDatePrice cancelDatePriceLists, String checkinDT, int checkinPrice, Room room, int value, String agentType) {
		long startTime = System.currentTimeMillis();
		logger.debug("### caculateExceptionCancelDateList START ###");
		CancelDatePrice result = new CancelDatePrice();
		if (cancelDatePriceLists.getCancelDayPrice() != null && cancelDatePriceLists.getCancelDayPrice().size() > 0) {// 有取消列表日期
			for (DayPrice cancelDt : cancelDatePriceLists.getCancelDayPrice()) {
				int diffday = UtilDate.getDiffDays(cancelDt.getDay(), checkinDT);
				if (diffday > 0) { // 該區消日期,在入住區間內,但不包含當日, 且包含在取消日期列表中的日期
					int chargePrice = checkinPrice * value;// 收取全部費用
					cancelDt.setSalePrice(cancelDt.getSalePrice() + chargePrice);
				} else if (diffday == 0) {// 等於入住日期
					for (DayPrice cancelDay : cancelDatePriceLists.getCancelDayPrice()) {
						int mdiffday = UtilDate.getDiffDays(cancelDay.getDay(), checkinDT);
						// 該區消日期,在入住區間內,且包含在取消日期列表中的日期
						if (mdiffday >= 0) {
							for (DayPrice checkDayPrice : room.getDayPrice()) {
								if (cancelDay.getDay().equals(checkDayPrice.getDay())) {
									int chargePrice = checkDayPrice.getSalePrice() * value;// 收取當晚入住費用
									cancelDt.setSalePrice(cancelDt.getSalePrice() + chargePrice);
								}
							}
						}
					}
				}
			}
		} else {// 沒有取消列表日期
			if (room.getDayPrice().size() == 1) {// 入住天數只有一天
				int chargePrice = checkinPrice * value;// 收取全部費用
				DayPrice dayprice = new DayPrice();
				dayprice.setDay(checkinDT);
				dayprice.setSalePrice(chargePrice);
				List<DayPrice> dateprice = new ArrayList<DayPrice>();
				dateprice.add(dayprice);
				cancelDatePriceLists.setCancelDayPrice(dateprice);
			} else {// 入住天數超過一天以上
				List<String> dateList = caculateCancelDateList(hotelId, roomId, room.getDayPrice(), agentType);
				// 塞入所有取消日期列表
				List<DayPrice> dateprice = new ArrayList<DayPrice>();
				for (int i = 0; i < dateList.size(); i++) {
					DayPrice mdayprice = new DayPrice();
					mdayprice.setDay(dateList.get(i));
					dateprice.add(mdayprice);
				}
				cancelDatePriceLists.setCancelDayPrice(dateprice);
				// 塞入所有取消日期列表中的價錢
				for (DayPrice cancelDt : cancelDatePriceLists.getCancelDayPrice()) {
					int diffday = UtilDate.getDiffDays(cancelDt.getDay(), checkinDT);
					if (diffday > 0) { // 該區消日期,在入住區間內,但不包含當日, 且包含在取消日期列表中的日期
						int chargePrice = checkinPrice * value;// 收取全部費用
						cancelDt.setSalePrice(cancelDt.getSalePrice() + chargePrice);
					} else if (diffday == 0) {// 等於入住日期
						for (DayPrice cancelDay : cancelDatePriceLists.getCancelDayPrice()) {
							int mdiffday = UtilDate.getDiffDays(cancelDay.getDay(), checkinDT);
							// 該區消日期,在入住區間內,且包含在取消日期列表中的日期
							if (mdiffday >= 0) {
								for (DayPrice checkDayPrice : room.getDayPrice()) {
									if (cancelDay.getDay().equals(checkDayPrice.getDay())) {
										int chargePrice = checkDayPrice.getSalePrice() * value;// 收取當晚入住費用
										cancelDt.setSalePrice(cancelDt.getSalePrice() + chargePrice);
									}
								}
							}
						}
					}
				}
			}
		}
		result = cancelDatePriceLists;
		logger.debug("### caculateExceptionCancelDateList END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return result;
	}
	
	/**
	 * 符合取消規則中所有日期
	 * @param hotelId
	 * @param roomId
	 * @param checkdate
	 * @return List
	 */
	public List<String> caculateCancelDateList(String hotelId, String roomId, List<DayPrice> checkdate, String agentType) {
		long startTime = System.currentTimeMillis();
		logger.debug("### caculateCancelDateList START ###");
		List<CancelDetail> cancelDetail = new ArrayList<CancelDetail>();
		List<String> datelist = new ArrayList<String>();
		try {
			for (int i = 0; i < checkdate.size(); i++) {
				String checkDT = checkdate.get(i).getDay();
				cancelDetail = erpdb11gCancelRepository.getCancelExcep(hotelId, roomId, checkDT);
				if (cancelDetail != null && cancelDetail.size() > 0) {
					if (!datelist.contains(checkDT)) {
						String temp = checkDT;
						datelist.add(temp);
					}
				} else {
					cancelDetail = erpdb11gCancelRepository.getCancelForRoomtypeNo(hotelId, roomId, checkDT, agentType);
					if (cancelDetail != null && cancelDetail.size() > 0) {
						int before = 0;
						int size = cancelDetail.size();
						if (cancelDetail.get(size - 1).getRuleBefore() == 99999) {
							int index = size - 2;
							before = (cancelDetail.get(index).getRuleBefore() / 24);// 將小時轉成天數 144/24 = 6天
						} else {
							before = (cancelDetail.get(size - 1).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
						}

						String beforedate = UtilDate.getBeforeDate(checkDT, before); // 前幾天的日期
						List<String> tempdatelist = this.getlistAllDay(beforedate, checkDT);// 把入住日前幾天的日期列出來
						if (datelist.size() == 0) {
							for (int j = 0; j < tempdatelist.size(); j++) {
								String temp = tempdatelist.get(j);
								datelist.add(temp);
							}
						} else {
							for (int j = 0; j < tempdatelist.size(); j++) {
								if (!datelist.contains(tempdatelist.get(j))) {
									String temp = tempdatelist.get(j);
									datelist.add(temp);
								}
							}
						}
					} else {
						// 在看是否符合cancel 房型公版取消
						cancelDetail = erpdb11gCancelRepository.getCancelForRoomtypeX(hotelId, roomId, checkDT, agentType);
						if (cancelDetail != null && cancelDetail.size() > 0) {
							int before = 0;
							int size = cancelDetail.size();
							if (cancelDetail.get(size - 1).getRuleBefore() == 99999) {
								int index = size - 2;
								before = (cancelDetail.get(index).getRuleBefore() / 24);// 將小時轉成天數 144/24 = 6天
							} else {
								before = (cancelDetail.get(size - 1).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
							}
							String beforedate = UtilDate.getBeforeDate(checkDT, before); // 前幾天的日期
							List<String> tempdatelist = this.getlistAllDay(beforedate, checkDT);// 把入住日前幾天的日期列出來
							if (datelist.size() == 0) {
								for (int j = 0; j < tempdatelist.size(); j++) {
									String temp = tempdatelist.get(j);
									datelist.add(temp);
								}
							} else {
								for (int j = 0; j < tempdatelist.size(); j++) {
									if (!datelist.contains(tempdatelist.get(j))) {

										String temp = tempdatelist.get(j);
										datelist.add(temp);
									}
								}
							}
						} else {
							// 在看是否符合cancel 飯店公版取消
							cancelDetail = erpdb11gCancelRepository.getCancelForProdNo(hotelId, roomId, checkDT, agentType);
							if (cancelDetail != null && cancelDetail.size() > 0) {
								int before = 0;
								int size = cancelDetail.size();
								if (cancelDetail.get(size - 1).getRuleBefore() == 99999) {
									int index = size - 2;
									before = (cancelDetail.get(index).getRuleBefore() / 24);// 將小時轉成天數 144/24 = 6天
								} else {
									before = (cancelDetail.get(size - 1).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
								}
								String beforedate = UtilDate.getBeforeDate(checkDT, before); // 前幾天的日期
								List<String> tempdatelist = this.getlistAllDay(beforedate, checkDT);// 把入住日前幾天的日期列出來
								if (datelist.size() == 0) {
									for (int j = 0; j < tempdatelist.size(); j++) {
										String temp = tempdatelist.get(j);
										datelist.add(temp);
									}
								} else {
									for (int j = 0; j < tempdatelist.size(); j++) {
										if (!datelist.contains(tempdatelist.get(j))) {
											String temp = tempdatelist.get(j);
											datelist.add(temp);
										}
									}
								}
							} else {
								cancelDetail = erpdb11gCancelRepository.getCancelForProdNoX(agentType);
								if (cancelDetail != null && cancelDetail.size() > 0) {
									int before = 0;
									int size = cancelDetail.size();
									if (cancelDetail.get(size - 1).getRuleBefore() == 99999) {
										int index = size - 2;
										before = (cancelDetail.get(index).getRuleBefore() / 24);// 將小時轉成天數 144/24 = 6天
									} else {
										before = (cancelDetail.get(size - 1).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
									}
									String beforedate = UtilDate.getBeforeDate(checkDT, before); // 前幾天的日期
									List<String> tempdatelist = this.getlistAllDay(beforedate, checkDT);// 把入住日前幾天的日期列出來
									if (datelist.size() == 0) {
										for (int j = 0; j < tempdatelist.size(); j++) {
											String temp = tempdatelist.get(j);
											datelist.add(temp);
										}
									} else {
										for (int j = 0; j < tempdatelist.size(); j++) {
											if (!datelist.contains(tempdatelist.get(j))) {
												String temp = tempdatelist.get(j);
												datelist.add(temp);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("caculateCancelDateList Error ");
			logger.error(e.getMessage(), e);
		}
		logger.debug("### caculateCancelDateList END {} ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		return datelist;
	}
	
	/**
	 * 把入住日前幾天的日期列出來
	 * @param checkBeforMaxDt
	 * @param checkDate
	 * @return
	 */
	private List<String> getlistAllDay(String checkBeforMaxDt, String checkDate) {
		List<String> datelist = new ArrayList<String>();
		int diffday = UtilDate.getDiffDays(checkBeforMaxDt, checkDate);
		for (int i = 0; i <= diffday; i++) {
			String date = UtilDate.getBeforeDate(checkDate, i);
			datelist.add(date);
		}
		return datelist;
	}
	
	/**
	 * 一般取消規則每日價錢
	 * @param canceldetail
	 * @param checkDT
	 * @param cancelChargeDateLists
	 * @param checkinPrice
	 * @param firstCheckIn
	 * @param orginalDayPrices
	 * @return List
	 */
	public List<DayPrice> caculateCancelCharge(List<CancelDetail> canceldetail, String checkDT, List<DayPrice> cancelChargeDateLists, int checkinPrice, String firstCheckIn, List<DayPrice> orginalDayPrices, List<String> checkedDateList) {
		long startTime = System.currentTimeMillis();
		logger.debug("### caculateCancelCharge START ###");
		List<DayPrice> result = new ArrayList<DayPrice>();
		if (canceldetail != null && canceldetail.size() > 0) {
			int before = 0;
			// 取得該入住日取消規定中,最大區間的天數
			int size = canceldetail.size();
			if (canceldetail.get(size - 1).getRuleBefore() == 99999) {
				int index = size - 2;
				before = (canceldetail.get(index).getRuleBefore() / 24);// 將小時轉成天數 144/24 = 6天
			} else {
				before = (canceldetail.get(size - 1).getRuleBefore() / 24) - 1;// 將小時轉成天數 144/24 = 6天
			}
			String beforedate = UtilDate.getBeforeDate(checkDT, before); // 查出入住日,往前推最大天數的日期
			List<String> cancelRuleAllDate = this.getlistAllDay(beforedate, checkDT);// 把該入住日取消規則中所有的日期列出來
			// 判斷再取消規則中的日期,所需收取的費用
			int max = 0;
			int min = 0;
			int CancelDetailCount = 0;
			// 入住日的所有取消規則
			for (CancelDetail detail : canceldetail) {
				int diffbefore = 0;
				if (detail.getRuleValue() == 0) {
					diffbefore = detail.getRuleBefore() / 24;// 將小時轉成天數 144/24 = 6天
					max = diffbefore - 1; // 取消有效期,因為當日算0,前一天算1,依此類推
				} else {
					diffbefore = detail.getRuleBefore() / 24;// 將小時轉成天數 144/24 = 6天
					max = diffbefore - 1;
				}
				// 計算從入住日開始算起的所有日期的取消價錢
				for (int i = 0; i < cancelRuleAllDate.size(); i++) {
					String cancelThisday = cancelRuleAllDate.get(i);// 欲取消日期
					int diffday = UtilDate.getDiffDays(cancelThisday, checkDT);// 欲取消日期跟入住日的天數差
					if (CancelDetailCount == 0) {
						// 是否再取消規則天數中
						if (diffday <= max) {
							for (DayPrice cancelChargeDate : cancelChargeDateLists) {
								if (checkedDateList.size() == 0 || checkedDateList == null || !checkedDateList.contains(cancelChargeDate.getDay())) {
									if (cancelChargeDate.getDay().equals(cancelThisday)) {// 取消規則日期列表中,是否有跟欲取消日期相同
										if (!cancelChargeDate.getDay().equals(checkDT)) {// 欲取消日期不等於入住日
											if (detail.getRuleType().equals("3")) {// 扣首日
												if (firstCheckIn.equals(checkDT)) {// 是否為第一晚入住日,
													int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) checkinPrice);
													cancelChargeDate.setSalePrice(chargePrice);
												} else {
													int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + 0);
													cancelChargeDate.setSalePrice(chargePrice);
												}
											} else {// 非扣首日,看取消規則的折扣%
												int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) Math.round(checkinPrice * detail.getRuleValue()));
												cancelChargeDate.setSalePrice(chargePrice);
											}
											break;
										} else {// 欲取消日期等於入住日
											if (detail.getRuleType().equals("3")) {// 扣首日
												for (DayPrice mchargedt : cancelChargeDateLists) {// 取消日期列表
													String enddate = mchargedt.getDay();
													int mdiffday = UtilDate.getDiffDays(enddate, checkDT);// 是否有大於該查詢取消
													if (mdiffday > 0) {
														for (DayPrice orginalDayPrice : orginalDayPrices) {
															if (mchargedt.getDay().equals(orginalDayPrice.getDay())) {
																int chargePrice = orginalDayPrice.getSalePrice();// 收取全部費用
																cancelChargeDate.setSalePrice(cancelChargeDate.getSalePrice() + chargePrice);
															}
														}
													} else if (mdiffday == 0) {
														int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) checkinPrice);
														cancelChargeDate.setSalePrice(chargePrice);
													}
												}
												break;
											} else {// 非扣首日
												for (DayPrice mchargedt : cancelChargeDateLists) {
													String enddate = mchargedt.getDay();
													int mdiffday = UtilDate.getDiffDays(enddate, checkDT);// 是否有大於該查詢取消
													if (mdiffday > 0) {
														for (DayPrice orginalDayPrice : orginalDayPrices) {
															if (mchargedt.getDay().equals(orginalDayPrice.getDay())) {
																if (detail.getRuleValue().toString().equals("0.0")) {
																	int chargePrice = (int) ((double) Math.round(orginalDayPrice.getSalePrice() * detail.getRuleValue()));// 收取全部費用
																	cancelChargeDate.setSalePrice(cancelChargeDate.getSalePrice() + chargePrice);
																} else {
																	int chargePrice = (int) ((double) Math.round(orginalDayPrice.getSalePrice() * detail.getRuleValue()));// 收取全部費用
																	cancelChargeDate.setSalePrice(cancelChargeDate.getSalePrice() + chargePrice);
																}
															}
														}
													} else if (mdiffday == 0) {
														if (detail.getRuleValue().toString().equals("0.0")) {
															int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) Math.round(checkinPrice * detail.getRuleValue()));
															cancelChargeDate.setSalePrice(chargePrice);
														} else {
															int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) Math.round(checkinPrice * detail.getRuleValue()));
															cancelChargeDate.setSalePrice(chargePrice);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					} else {
						if (diffday > min && diffday <= max) {
							for (DayPrice cancelChargeDate : cancelChargeDateLists) {
								if (cancelChargeDate.getDay().equals(cancelThisday)) {// 取消規則日期列表中,是否有跟欲取消日期相同
									if (!cancelChargeDate.getDay().equals(checkDT)) {// 欲取消日期不等於入住日
										if (detail.getRuleType().equals("3")) {// 扣首日
											if (firstCheckIn.equals(checkDT)) {// 是否為第一晚入住日,
												int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) checkinPrice);
												cancelChargeDate.setSalePrice(chargePrice);
											} else {
												int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + 0);
												cancelChargeDate.setSalePrice(chargePrice);
											}
										} else {// 非扣首日,看取消規則的折扣%
											int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) Math.round(checkinPrice * detail.getRuleValue()));
											cancelChargeDate.setSalePrice(chargePrice);
										}
										break;
									} else {// 欲取消日期等於入住日
										if (detail.getRuleType().equals("3")) {// 扣首日
											for (DayPrice mchargedt : cancelChargeDateLists) {// 取消日期列表
												String enddate = mchargedt.getDay();
												int mdiffday = UtilDate.getDiffDays(enddate, checkDT);// 是否有大於該查詢取消
												if (mdiffday > 0) {
													for (DayPrice orginalDayPrice : orginalDayPrices) {
														if (mchargedt.getDay().equals(orginalDayPrice.getDay())) {
															int chargePrice = orginalDayPrice.getSalePrice();// 收取全部費用
															cancelChargeDate.setSalePrice(cancelChargeDate.getSalePrice() + chargePrice);
														}
													}
												} else if (mdiffday == 0) {
													int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) checkinPrice);
													cancelChargeDate.setSalePrice(chargePrice);
												}
											}
											break;
										} else {// 非扣首日
											for (DayPrice mchargedt : cancelChargeDateLists) {
												String enddate = mchargedt.getDay();
												int mdiffday = UtilDate.getDiffDays(enddate, checkDT);// 是否有大於該查詢取消
												if (mdiffday > 0) {
													for (DayPrice orginalDayPrice : orginalDayPrices) {
														if (mchargedt.getDay().equals(orginalDayPrice.getDay())) {
															int chargePrice = orginalDayPrice.getSalePrice();// 收取全部費用
															cancelChargeDate.setSalePrice(cancelChargeDate.getSalePrice() + chargePrice);
														}
													}
												} else if (mdiffday == 0) {
													int chargePrice = (int) ((double) cancelChargeDate.getSalePrice() + (double) checkinPrice);
													cancelChargeDate.setSalePrice(chargePrice);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				min = max;
				CancelDetailCount++;
			}
		}
		result = cancelChargeDateLists;
		logger.debug("### caculateCancelCharge END {} ### ({}ms)", System.currentTimeMillis() - startTime);
		return result;
	}
}
