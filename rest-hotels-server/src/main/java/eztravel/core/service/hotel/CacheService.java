/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: CacheService.java
 * @author:   002766
 * @date:     2017/11/7, 下午 05:11:45
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.Set;

import eztravel.core.enums.Cache;
import eztravel.rest.pojo.hotel.CacheWrapper;

/**
 * Created by jimin on 7/22/15.
 */
public interface CacheService {

	/**
	 * Gets the hotel key.
	 * @param apiType
	 * @param pojo
	 * @param hotelId
	 * @return the hotel key
	 */
	String getHotelKey(Cache.ApiType apiType, String pojo, String hotelId);

	/**
	 * Gets the room list key.
	 * @param apiType
	 * @param pojo
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param agentType
	 * @return the room list key
	 */
	String getRoomListKey(Cache.ApiType apiType, String pojo, String hotelId, String roomId, int roomQty, String checkin, String checkout, String agentType);

	/**
	 * 使用CacheWrapper來redis.
	 * @param cacheKey
	 * @param expireTime
	 * @return the cache
	 */
	CacheWrapper getCache(String cacheKey, int expireTime);
	
	/**
	 * 取得CacheWrapper的cache data.
	 * @param key
	 * @param cacheObj
	 * @param expireTime
	 */
	void setCache(String key, CacheWrapper cacheObj, int expireTime);
	
	/**
	 * 取得reids關鍵字key中所有的cache key.
	 * @param startStr
	 * @return the results cache key by start with
	 * @throws Exception
	 */
	Set<String> getResultsCacheKeyByStartWith(String startStr) throws Exception;
	
	/**
	 * Gets the htl min price key.
	 * @param apiType
	 * @param pojo
	 * @param city
	 * @param travelType
	 * @param checkIn
	 * @param checkOut
	 * @param agentType
	 * @param roomType
	 * @return the htl min price key
	 */
	String getHtlMinPriceKey(Cache.ApiType apiType, String pojo, String city, String travelType, String checkIn, String checkOut, String agentType, String roomType);

}
