package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eztravel.persistence.repository.oracle11g.Erpdb11gPromoInfoRepository;
import eztravel.rest.pojo.hotel.PromoInfo;
import eztravel.util.ThreadPoolCenter;
import eztravel.util.UtilDate;

public class PromoServiceImpl implements PromoService{
	private static final Logger logger = LoggerFactory.getLogger(PromoServiceImpl.class);
	    
	@Autowired 
	private Erpdb11gPromoInfoRepository erpdb11gPromoInfoRepository;
	
  /** The flash rangeTime. */
  @Value("${flash.rangeTime}")
  private int flashRange;
	
	/**
	 * 取得房型區間適用促銷
	 * @param hotelId
	 * @param roomId
	 * @param checkInDate
	 * @param checkOutDate
	 * @return List<PromoInfo>
	 */
	public List<PromoInfo> getRoomPromoInfo(final String hotelId, final String roomId, final String checkin, final String checkout) throws Exception {
		return this.getRoomPromoInfo(hotelId, roomId, checkin, checkout ,null);
	}
	public List<PromoInfo> getRoomPromoInfo(final String hotelId, final String roomId, final String checkin, final String checkout, FilterType filter) throws Exception {
		long startTime = System.currentTimeMillis();
    logger.debug("###PromoService getRoomPromoInfo START ###");
		
    List<PromoInfo> results = new ArrayList<PromoInfo>();
    
    ExecutorService promoInfoPool = ThreadPoolCenter.getIoPool();
    ExecutorService promoInfoPmPool = ThreadPoolCenter.getIoPool();
    ExecutorService promoAppInfoPool = ThreadPoolCenter.getIoPool();
    ExecutorService promoSysAppInfoPool = ThreadPoolCenter.getIoPool();
    
    //app訂購日的促銷資訊
    List<Future<List<PromoInfo>>> promoAppInfoResults = new ArrayList<Future<List<PromoInfo>>>();
    promoAppInfoResults.add(promoAppInfoPool.submit(new Callable<List<PromoInfo>>() {
          @Override 
          public List<PromoInfo> call() throws Exception {
            return erpdb11gPromoInfoRepository.getEarlyPromoInfo(hotelId, roomId, checkin, checkout, "003");
          }
        }));
  
    //app系統日的促銷資訊
    List<Future<List<PromoInfo>>> promoSysInfoResults = new ArrayList<Future<List<PromoInfo>>>();
    promoSysInfoResults.add(promoSysAppInfoPool.submit(new Callable<List<PromoInfo>>() {
          @Override 
          public List<PromoInfo> call() throws Exception {
             return erpdb11gPromoInfoRepository.getCommonPromoInfo(hotelId, roomId, checkin, checkout);
          }
        }));

    //早鳥促銷資訊 
    List<Future<List<PromoInfo>>> promoInfoResults = new ArrayList<Future<List<PromoInfo>>>();
    promoInfoResults.add(promoInfoPool.submit(new Callable<List<PromoInfo>>(){
          @Override 
          public List<PromoInfo> call() throws Exception {
            return erpdb11gPromoInfoRepository.getEarlyPromoInfo(hotelId, roomId, checkin, checkout, "001");
          }
        }));
  //晚鳥促銷資訊 
    List<Future<List<PromoInfo>>> promoInfoLastResults = new ArrayList<Future<List<PromoInfo>>>();
    promoInfoLastResults.add(promoInfoPmPool.submit(new Callable<List<PromoInfo>>(){
          @Override 
          public List<PromoInfo> call() throws Exception {
            return erpdb11gPromoInfoRepository.getLastPromoInfo(hotelId, roomId, checkin, checkout);
          }
        }));

    try{
    	results.addAll(this.convertPromoFuture(promoAppInfoResults));
    	results.addAll(this.convertPromoFuture(promoSysInfoResults));
    	results.addAll(this.convertPromoFuture(promoInfoResults));
    	results.addAll(this.convertPromoFuture(promoInfoLastResults));
    }catch(Exception e){
    	e.printStackTrace();
    	logger.error(e.getMessage(), e);
    }
    
    //重組過濾篩選promo資訊
    results = this.convertRoomPromo(results, filter);
    
    //排序promo資訊
    if(filter != null && !filter.name().equals("WEB"))
    	results = this.sortRoomPromo(results, filter);

    logger.debug("###booking getRoomPromoInfo END ### (" + (System.currentTimeMillis() - startTime) + "ms)");
    return results;
   
  }
	
	
	public List<PromoInfo> getHotelPromoInfo(final List<String> hotelIds, final String checkin, final String checkout, FilterType filter) throws Exception {
    long startTime = System.currentTimeMillis();
    logger.debug("###PromoService getHotelPromoInfo START ###");
    
    List<PromoInfo> results = new ArrayList<PromoInfo>();
    
    List<String> checkinListDay = this.getlistAllDay(checkin , checkout);
    //撈出所有促銷資訊 
    List<PromoInfo> hotelpromoInfoResults = new ArrayList<PromoInfo>();
	for (String checkinDate : checkinListDay) {
		hotelpromoInfoResults = erpdb11gPromoInfoRepository.getPromoInfo(hotelIds, checkinDate);
		results.addAll((hotelpromoInfoResults));
	}
    //重組過濾篩選promo資訊
    results = this.convertRoomPromo(results, filter);
    
    //排序promo資訊
    if(filter != null && !filter.name().equals("WEB"))
      results = this.sortRoomPromo(results, filter);

    logger.debug("###booking getHotelPromoInfo END ### (" + (System.currentTimeMillis() - startTime) + "ms)");
    return results;
   
  }
	
	
	/**
	 * Promo 執行序返回 Future 轉換
	 * @param futureList
	 * @return
	 */
	private List<PromoInfo> convertPromoFuture(List<Future<List<PromoInfo>>> futureList) throws Exception {
		List<PromoInfo> result = new ArrayList<PromoInfo>();
		
		try{
  		for(int p = 0; p < futureList.size() ; p++){
  		  if(futureList.get(p).get() != null ){
          for(int k = 0 ; k < futureList.get(p).get().size(); k++){
            result.add(futureList.get(p).get().get(k));
          }
  		  }
      } 
		}catch(Exception e){
      logger.error(e.getMessage(), e);
    }
		return result;
	}
  
  /**
   * 1.重組過濾篩選promo資訊
   * 2.十六小時內且為特價003才有快閃標籤
   * 3.FilterType :
   *    app,算app價錢及app畫面顯示的促銷 ;
   *    web,算web價錢 ;
   *    null:web畫面要顯示的促銷
   * @param promos
   * @return List<PromoInfo>
   */
  private List<PromoInfo> convertRoomPromo(List<PromoInfo> promos, FilterType filterType) {
    List<PromoInfo> results = new ArrayList<PromoInfo>();
    try{
      for(PromoInfo promo: promos){
        boolean promoCheck =false;
        if(Integer.parseInt(promo.getHhmmEnd()) < Integer.parseInt(promo.getHhmmBeg())){//當起日大於迄日時間
          int hhmmEnd = Integer.parseInt(promo.getHhmmEnd())+2400;//迄日2400
          int hhmmStart =Integer.parseInt(promo.getHhmmBeg());
          //六小時內且為特價005才有快閃標籤
          if(hhmmEnd-hhmmStart <= flashRange  && promo.getPromoType().equals("005")){
            promo.setIsFlash(true);
          }
          if(Integer.parseInt(UtilDate.getTodayHHMM())< hhmmStart){//當起日大於系統日
            int syshhmm = Integer.parseInt(UtilDate.getTodayHHMM())+2400;
            if(syshhmm >= hhmmStart && syshhmm <=hhmmEnd){
              promoCheck=true;
            }else{
              promoCheck=false;
            }
          }else{//當系統日大於起日
            int syshhmm = Integer.parseInt(UtilDate.getTodayHHMM());
            if(syshhmm >= hhmmStart && syshhmm <=hhmmEnd){
              promoCheck=true;
            }else{
              promoCheck=false;
            }
          }
        }else{//當迄日大於起日
          int hhmmEnd = Integer.parseInt(promo.getHhmmEnd());
          int hhmmStart =Integer.parseInt(promo.getHhmmBeg());
          //六小時內且為特價005才有快閃標籤
          if(hhmmEnd-hhmmStart <= flashRange && promo.getPromoType().equals("005")){
            promo.setIsFlash(true);
          }
          int syshhmm = Integer.parseInt(UtilDate.getTodayHHMM());
          if(syshhmm >= hhmmStart && syshhmm <=hhmmEnd){
           
              promoCheck=true;
           
          }else{
            promoCheck=false;
          }
        }
        if(promoCheck){
        	if(filterType == null || !filterType.getFilter().contains(promo.getAppRange())){//有篩選條件
        		if(StringUtils.equals(promo.getDateTypeYn(), "N")){//不用看日別
              if(filterType == null ){//非特價且非app適用
                if("005".equals(promo.getPromoType())){
                  if(!"3".equals(promo.getAppRange())){
                    results.add(promo);
                  }
                }else{
                  results.add(promo);
                }
              }else if(filterType != null ){
                results.add(promo);
              }
        		}else if(StringUtils.equals(promo.getDateTypeYn(), "Y")){//要看日別
        		  if(promo.getUpDtType().equals("1")){ //依入住日
                if(UtilDate.convertDateType(promo.getDateType()).contains(UtilDate.getWeek(promo.getAppDate()))){
                  if(filterType == null ){//非特價且非app適用
                    if("005".equals(promo.getPromoType())){
                      if(!"3".equals(promo.getAppRange())){
                        results.add(promo);
                      }
                    }else{
                      results.add(promo);
                    }
                  }else if(filterType != null ){
                    results.add(promo);
                  }
                }
        		  }else if(promo.getUpDtType().equals("2")){//依訂購日
        		    if(UtilDate.convertDateType(promo.getDateType()).contains(UtilDate.getWeek(UtilDate.getToday("yyyyMMdd")))){
        		      if(filterType == null ){//非特價且非app適用
                    if("005".equals(promo.getPromoType())){
                      if(!"3".equals(promo.getAppRange())){
                        results.add(promo);
                      }
                    }else{
                      results.add(promo);
                    }
                  }else if(filterType != null ){
                    results.add(promo);
                  }
                }
        		  }
            }
        	}
        }
      }
    }catch(Exception e){
      logger.error(e.getMessage(), e);
    }
    return results;
  }

  /**
   * 排序promo資訊
   * @param promos
   * @param filterType
   * @return
   */
  private List<PromoInfo> sortRoomPromo(List<PromoInfo> promos, FilterType filterType) {
  	List<String> sort = filterType.getSort();
  	List<PromoInfo> results = null;
  	if(sort != null && !sort.isEmpty()){
  		results = new ArrayList<PromoInfo>();
  		//先將需排序的放置最前面
  		for(String promoCode: sort){
  			for(PromoInfo info: promos){
  				if(StringUtils.equals(promoCode, info.getPromoType()))
  					results.add(info);
  			}
  		}
  		
  		//處理未排序的
  		if(results.size() != promos.size()){
  			for(PromoInfo info: promos){
  				if(!sort.contains(info.getPromoType()))
  					results.add(info);
  			}
  		}
  	}
    return results;
  }
  
  
  //把入住日後幾天的日期列出來(不包含退房日)
  private List<String> getlistAllDay(String checkinDt, String checkoutDt){
    List<String> datelist = new ArrayList<String>();
    
    int diffday = UtilDate.getDiffDays(checkinDt, checkoutDt);
    
    
    for(int i = 0 ; i< diffday ; i ++){
      String date = UtilDate.getAfterDate(checkinDt, i);
      datelist.add(date);
    }
    
    return datelist;
  }
}
