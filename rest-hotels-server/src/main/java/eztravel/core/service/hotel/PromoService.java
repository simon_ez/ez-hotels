package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.List;

import eztravel.rest.pojo.hotel.PromoInfo;

public interface PromoService {
	
	/**
	 * 取得房型區間適用促銷
	 * @param hotelId
	 * @param roomId
	 * @param checkInDate
	 * @param checkOutDate
	 * @return List<PromoInfo>
	 */
	List<PromoInfo> getRoomPromoInfo(final String hotelId, final String roomId, final String checkInDate, final String checkOutDate) throws Exception ;
	List<PromoInfo> getRoomPromoInfo(final String hotelId, final String roomId, final String checkInDate, final String checkOutDate, FilterType filterType) throws Exception ;
	
	List<PromoInfo> getHotelPromoInfo(final List<String> hotelIds, final String checkin, final String checkout, FilterType filter)throws Exception ;

	/**
	 * 查詢返回過濾不顯示促銷標籤
	 */
	enum FilterType{
		@SuppressWarnings("serial")
		WEB("WEB過濾促銷適用範圍", new ArrayList<String>(){{add("3");}}, null),
		@SuppressWarnings("serial")
		APP("APP過濾促銷適用範圍", new ArrayList<String>(){{add("2");}}, new ArrayList<String>(){{add("004");add("001");add("002");}}),
		@SuppressWarnings("serial")
		BOOKING_WEB("訂單WEB過濾促銷適用範圍", new ArrayList<String>(){{add("3");}}, new ArrayList<String>(){{add("001");add("002");add("004");}}),
		@SuppressWarnings("serial")
		BOOKING_APP("訂單APP過濾促銷適用範圍", new ArrayList<String>(){{add("2");}}, new ArrayList<String>(){{add("001");add("002");add("004");}});
		
		private String source;
		private List<String> filter;
		private List<String> sort;
		
		FilterType(String source, List<String> filter, List<String> sort){
			this.source = source;
			this.filter = filter;
			this.sort = sort;
		}

		public String getSource() {
			return source;
		}
		public List<String> getFilter() {
			return filter;
		}
		public List<String> getSort(){
			return sort;
		}
	}
}
