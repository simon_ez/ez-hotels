/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.hotel
 * @FileName: FacilityService.java
 * @author:   Kent
 * @date:     2015/5/5, 下午 05:59:30
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * FacilityService, 飯店設施.
 * </pre>
 * @author Kent
 */
public interface FacilityService {

	/**
	 * List hotel facilites.　飯店設施服務項目清單
	 * @param hotelId
	 * @return the map
	 */
	Map<String, List<String>> listHotelFacilites(String hotelId);
}
