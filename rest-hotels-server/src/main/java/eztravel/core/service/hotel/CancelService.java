/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.cancel
 * @FileName: CancelService.java
 * @author:   cano0530
 * @date:     2017/9/25, 上午 11:47:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.List;

/**
 * The Interface CancelService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface CancelService {

	/**
	 * Gets the cancel descs.
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param nationality
	 * @param source
	 * @param isErp
	 * @return
	 */
	List<String> getCancelDescs(final String hotelId, String roomId, final int roomQty, final String checkin, final String checkout, String customerId, String nationality, String source, String isErp);

	/**
	 * Gets the cancel change desc.
	 * @param hotelId
	 * @param roomId
	 * @param checkIn
	 * @param checkOut
	 * @return
	 */
	List<String> getCancelChangeDesc(String hotelId, String roomId, String checkIn, String checkOut);

}
