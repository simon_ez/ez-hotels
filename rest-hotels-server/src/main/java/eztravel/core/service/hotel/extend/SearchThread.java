package eztravel.core.service.hotel.extend;

import java.util.concurrent.Callable;

/**
 * Created by jimin on 6/9/15.
 */
public class SearchThread<T> implements Callable<T> {
	SearchHotelInfo search;
	String hotelId;

	public SearchThread(SearchHotelInfo search, String hotelId) {
		this.search = search;
		this.hotelId = hotelId;
	}

	@Override
	public T call() throws Exception {
		return search.getInfo(hotelId);
	}
}
