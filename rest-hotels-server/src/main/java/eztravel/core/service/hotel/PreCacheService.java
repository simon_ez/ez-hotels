/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: PreCacheService.java
 * @author:   002766
 * @date:     2018/4/30, 下午 12:04:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.List;

import eztravel.core.pojo.HtlMinPrice;
import eztravel.rest.pojo.hotel.Room;

/**
 * The Interface PreCacheService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface PreCacheService {
	/**
	 * 從redis取得產品頁下半部的房型列表清單.
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 * @return the cache room list
	 */
	List<Room> getCacheRoomList(String hotelId, String roomId, int roomQty, String checkin, String checkout, String customerId, String source);

	/**
	 * 將產品頁下半部的房型列表存到redis.
	 * 
	 * @param rooms
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 */
	void setCacheRoomlList(List<Room> rooms, String hotelId, String roomId, int roomQty, String checkin, String checkout, String customerId, String source);
	
	/**
	 * Sets the cache htl min price.
	 * 
	 * @param htlMinPriceResult
	 * @param cityCd
	 * @param travelType
	 * @param checkin
	 * @param checkout
	 * @param roomTypes
	 * @param customerId
	 * @param source
	 * @param agentType
	 */
	void setCacheHtlMinPrice(List<List<HtlMinPrice>> htlMinPriceResult, String cityCd, String travelType, String checkin, String checkout, List<Integer> roomTypes, String customerId, String source, String agentType);

}
