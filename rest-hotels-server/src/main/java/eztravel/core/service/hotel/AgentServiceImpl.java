package eztravel.core.service.hotel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.pojo.CustomerDB;
import eztravel.persistence.repository.oracle.CommonRepository;
import eztravel.persistence.repository.oracle11g.Erpdb11gCustomerSearchRepository;
import eztravel.util.UtilDate;

public class AgentServiceImpl implements AgentService{
	private static final Logger logger = LoggerFactory.getLogger(AgentServiceImpl.class);
	private Map<String, String> PricingFormula = new HashMap<String, String>(); //系統商定價表公式
	private Map<String, String> PricingFormulaDesc = new HashMap<String, String>(); //系統商定價表描述
	
	@Autowired
	private CommonRepository commonRepository;
	@Autowired
	protected Erpdb11gCustomerSearchRepository erpdb11gCustomerSearchRepository;
	
	public AgentServiceImpl(CommonRepository commonRepository){
		try{
			this.commonRepository = commonRepository;
			if(PricingFormula.isEmpty()){
				for(Map<String, String> R86: commonRepository.getPricingFormula()){
					PricingFormula.put(R86.get("CODE_ID"), R86.get("CODE_DESC"));
					PricingFormulaDesc.put(R86.get("CODE_ID"), R86.get("CODE_NAME"));
				}
			}
		}catch(Exception e){
			logger.error("系統商定價表取得失敗");
			e.printStackTrace();
		}
	}
	
  public int usfHtlAgentRuleM(String formulaCode, int price, int cost) {
    try {
      String formula = PricingFormula.get(formulaCode);
      // 公式參數轉換
      formula = this.converParamOnFormula(formula, price, cost);

      // 判斷特殊進位方式
      boolean floor = false;
      if (formula.contains(Breakets.FLOOR.getBreakets())) {
        floor = true;
        formula = formula.replace(Breakets.FLOOR.getBreakets(), Breakets.SPACE.getBreakets());
      }

      // 計算機
      return Integer.parseInt(calculation(formula, floor));
    } catch (Exception e) {
      logger.error(
          "系統商定價公式代碼: " + formulaCode + ", Price: " + price + ", Cost: " + cost);
      throw e;
    }
  }

	/**
	 * 系統商定價
	 * @param rule
	 * @return int
	 */
	public int usfHtlAgentRuleM(Rule rule) throws Exception {
		int result = 0;
		RuleType ruleType = this.getUseRuleType(rule);
		if(ruleType == null){ //彈性公式(慢)
			String formula = PricingFormula.get(this.getUseRuleTypeFormula(rule));
			try{
				//公式參數轉換
				formula = this.converParamOnFormula(formula, rule.getPrice(), rule.getCost());
				
				//判斷特殊進位方式
				boolean floor = false;
				if(formula.contains(Breakets.FLOOR.getBreakets())){
					floor = true;
					formula = formula.replace(Breakets.FLOOR.getBreakets(), Breakets.SPACE.getBreakets());
				}
				
				//計算機
				result = Integer.parseInt(calculation(formula, floor));
			}catch(Exception e){
				logger.error("系統商定價公式: " + formula + ", Price: " + rule.getPrice() + ", Cost: " + rule.getCost());
				throw e;
			}
		}else{ //有程式代碼部分仍走舊有計算公式(快)
			result = this.getAmtByRuleType(ruleType, rule.getPrice(), rule.getCost());
		}
		return result;
	}
	
	/**
	 * 轉換參數為計算公式(文字)
	 * @param formula
	 * @param price
	 * @param cost
	 * @return String
	 */
	private String converParamOnFormula(String formula, int price, int cost){
		formula = formula.replace(Breakets.SPACE_ONE.getBreakets(), Breakets.SPACE.getBreakets());
		formula = formula.replace(Breakets.PRICE.getBreakets(), String.valueOf(price));
		formula = formula.replace(Breakets.COST.getBreakets(), String.valueOf(cost));
		return formula;
	}
	
	/**
	 * 計算最內層括號
	 * @param formula
	 * @return String
	 */
	private String calculation(String formula, boolean floor){
		//判斷特殊進位方式
		if(formula.contains(Breakets.FLOOR.getBreakets())){
			floor = true;
			formula.replace(Breakets.FLOOR.getBreakets(), Breakets.SPACE.getBreakets());
		}
		
		//取得最內層的括弧內容
		int bracketLeft = 9999;
		int bracketRight = 9999;
		for(int i = 0; i < formula.length(); i++){
			if(BreaketsMark.LEFT.getBreakets() == formula.charAt(i)){
				bracketLeft = i;
			}else if(BreaketsMark.RIGHT.getBreakets() == formula.charAt(i)){
				bracketRight = i;
				break;
			}
		}
		
		//取得計算內容
		String calStr = null;
		if(bracketLeft != 9999 && bracketRight != 9999)
			calStr = formula.substring(bracketLeft + 1, bracketRight);
		else
			calStr = formula;
		
		//拆解符號與數字
		List<String> content = new ArrayList<String>();
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < calStr.length(); i++){
			if(calStr.charAt(i) == BreaketsMark.MULTI.getBreakets() || calStr.charAt(i) == BreaketsMark.DIV.getBreakets() || 
				 calStr.charAt(i) == BreaketsMark.ADD.getBreakets() || calStr.charAt(i) == BreaketsMark.SUBTRA.getBreakets()){
				content.add(str.toString());
				content.add(String.valueOf(calStr.charAt(i)));
				str.delete(0, str.length());
			}else{
				str.append(String.valueOf(calStr.charAt(i)));
			}
		}
		content.add(str.toString()); //最後的尾數補上
		
		//先乘除
		if(content.contains(Breakets.MULTI.getBreakets()))
			content = this.calculation(content, Breakets.MULTI, floor);
		if(content.contains(Breakets.DIV.getBreakets()))
			content = this.calculation(content, Breakets.DIV, floor);
		//後加減
		if(content.contains(Breakets.ADD.getBreakets()))
			content = this.calculation(content, Breakets.ADD, floor);
		if(content.contains(Breakets.SUBTRA.getBreakets()))	
			content = this.calculation(content, Breakets.SUBTRA, floor);
		
		//指定價格
		if(content.contains(Breakets.AMOUNT.getBreakets()))	
			content = this.calculation(content, Breakets.AMOUNT, floor);

		//若已經沒有任何括弧，為最後計算式
		if(bracketLeft == bracketRight)
			formula = content.get(0);
		else
			formula = formula.replace(formula.substring(bracketLeft, bracketRight+1), content.get(0));
		
		//判斷是否仍有未計算公式
		if(formula.contains(Breakets.ADD.getBreakets()) || formula.contains(Breakets.SUBTRA.getBreakets()) || 
			 formula.contains(Breakets.MULTI.getBreakets()) || formula.contains(Breakets.DIV.getBreakets()) || 
			 formula.contains(Breakets.AMOUNT.getBreakets()))
			formula = this.calculation(formula, floor);
		
		return formula;
	}

	/**
	 * 計算機
	 * @return
	 */
	private List<String> calculation(List<String> str, Breakets breakets, boolean floor){
		int breaket = str.indexOf(breakets.getBreakets());
		int sum = 0;
		if(floor){
			switch(breakets){
				case ADD:
					sum = new BigDecimal(str.get(breaket-1)).add(new BigDecimal(str.get(breaket+1))).intValue();
					break;
				case SUBTRA:
					sum = new BigDecimal(str.get(breaket-1)).subtract(new BigDecimal(str.get(breaket+1))).intValue();
					break;
				case MULTI:
					sum = new BigDecimal(str.get(breaket-1)).multiply(new BigDecimal(str.get(breaket+1))).intValue();
					break;
				case DIV:
					sum = new BigDecimal(str.get(breaket-1)).divide(new BigDecimal(str.get(breaket+1))).intValue();
					break;
				case AMOUNT:
					sum = Integer.parseInt(str.get(breaket+1));
					break;
				default:
					break;
			}
		}else{
			switch(breakets){
				case ADD:
					sum = new BigDecimal(str.get(breaket-1)).add(new BigDecimal(str.get(breaket+1))).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
					break;
				case SUBTRA:
					sum = new BigDecimal(str.get(breaket-1)).subtract(new BigDecimal(str.get(breaket+1))).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
					break;
				case MULTI:
					sum = new BigDecimal(str.get(breaket-1)).multiply(new BigDecimal(str.get(breaket+1))).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
					break;
				case DIV:
					sum = new BigDecimal(str.get(breaket-1)).divide(new BigDecimal(str.get(breaket+1))).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
					break;
				case AMOUNT:
					sum = new BigDecimal(str.get(breaket+1)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
					break;
				default:
					break;
			}
		}
		
		str.add(0, String.valueOf(sum));
		for(int i = 0; i < 3; i++)
			str.remove(breaket);
		
		if(str.contains(breaket))
			str = this.calculation(str, breakets, floor);
		return str;
	}
	
	
	
	/**
	 * 取得使用算式層級
	 * @param rule
	 * @return String
	 */
	private String getUseRuleTypeFormula(Rule rule){
		switch(rule.getLevel()){
			case 1:
				return rule.getRuleType1();
			case 3:
				return rule.getRuleType3();
			case 4:
				return rule.getRuleType4();
			default: //預設第二階
				return rule.getRuleType2();
		}
	}
	
	/**
	 * 取得使用算式層級 Enum
	 * @param rule
	 * @return RuleType
	 */
	private RuleType getUseRuleType(Rule rule){
		return RuleType.match(this.getUseRuleTypeFormula(rule));
	}
	
	/**
	 * 系統商定價公式 (舊有SP方式，另已提供彈性公式)
	 * @param ruleType
	 * @return int
	 */
	private int getAmtByRuleType(RuleType ruleType, int price, int cost){
		switch(ruleType){
			case R000:
				return 0;  
			case R011:
				return price;
			case R012:
				return price + 100;   
			case R013:
				return price + 200; 
			case R014:
				return price + 300; 
			case R015:
				return price + 400; 
			case R016:
				return price + 500;   
			case R021:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.94).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R022:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.98).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R023:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.95).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R024:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.97).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R025:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.93).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R026:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.96).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R027:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.9).setScale(1, BigDecimal.ROUND_HALF_UP));
			case R028:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.945).setScale(3, BigDecimal.ROUND_HALF_UP));
			case R020:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.99).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R029:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.92).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02A:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.91).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02B:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.89).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02C:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.88).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02D:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.87).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02E:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.86).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R02F:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.85).setScale(2, BigDecimal.ROUND_HALF_UP));
			case R031:
				return price - 100;
			case R032:
				return price - 60;
			case R033:
        return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.98).setScale(2, BigDecimal.ROUND_HALF_UP)) -60;
			case R034:
        return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(0.97).setScale(2, BigDecimal.ROUND_HALF_UP)) -60;
			case R041:
				return this.priceMultiplyRule(new BigDecimal(price), new BigDecimal(1.05).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R050:
				return cost;   
			case R051:
				return cost + 500;
			case R052:
				return cost + 100;
			case R053:
				return cost + 200;
			case R054:
				return cost + 300;
			case R055:
				return cost + 400;
			case R056:
				return cost + 600;
			case R057:
				return cost + 700;
			case R058:
				return cost + 800;
			case R059:
				return cost + 900;
			case R05A:
				return cost + 1000;
			case R060:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.07).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R061:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.05).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R062:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.06).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R063:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.03).setScale(2, BigDecimal.ROUND_HALF_UP), 0); 		   
			case R064:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.15).setScale(2, BigDecimal.ROUND_HALF_UP), 0);    
			case R065:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.04).setScale(2, BigDecimal.ROUND_HALF_UP), 0);           
			case R066:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.25).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R067:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.01).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R068:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.02).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R069:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.1).setScale(1, BigDecimal.ROUND_HALF_UP), 0);
			case R06A:
				return this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.99).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06B:
				return this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.98).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06C:
				return this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.97).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06D:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.08).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06E:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.09).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06F:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.11).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06G:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.12).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06H:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.13).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06I:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.14).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06J:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.16).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06K:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.17).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06L:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.18).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06M:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.19).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06N:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.2).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06O:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.21).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06P:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.22).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06Q:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.23).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06R:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.24).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R06S:
				return cost + this.priceMultiplyRule(new BigDecimal(cost), new BigDecimal(0.5).setScale(2, BigDecimal.ROUND_HALF_UP), 0);
			case R071:
				return this.priceSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), 0) + cost;
			case R072:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.97).setScale(2, BigDecimal.ROUND_HALF_UP), 0) + cost;
			case R073:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.96).setScale(2, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R074:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.95).setScale(2, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R075:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.94).setScale(2, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R076:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.9).setScale(1, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R077:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.93).setScale(2, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R078:
				return this.priceMultiplyRuleSubtractCostDiv2(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(0.945).setScale(3, BigDecimal.ROUND_HALF_UP),0) + cost;
			case R099:
				return price * 100;
		}
		return price;
	}
	
	/**
	 * Price * Rule 無條件捨去至整數
	 * @param price
	 * @param rule
	 * @return int
	 */
	private int priceMultiplyRule(BigDecimal price, BigDecimal rule){
		return price.multiply(rule).intValue();
	}
	
	/**
	 * Price * Rule 針對指定小數位進行四捨五入
	 * @param price
	 * @param rule
	 * @param round
	 * @return int
	 */
	private int priceMultiplyRule(BigDecimal price, BigDecimal rule, int round){
		return price.multiply(rule).setScale(round, BigDecimal.ROUND_HALF_UP).intValue();
	}
	
	/**
	 * (Price - cost) / 2 針對指定小數位進行四捨五入
	 * @param price
	 * @param cost
	 * @param round
	 * @return int
	 */
	private int priceSubtractCostDiv2(BigDecimal price, BigDecimal cost, int round){
		return price.subtract(cost).divide(new BigDecimal(2)).setScale(round, BigDecimal.ROUND_HALF_UP).intValue();
	}
	
	/**
	 * (Price * Rule - cost) / 2 針對指定小數位進行四捨五入
	 * @param price
	 * @param cost
	 * @param rule
	 * @param round
	 * @return int
	 */
	private int priceMultiplyRuleSubtractCostDiv2(BigDecimal price, BigDecimal cost, BigDecimal rule, int round){
		return price.multiply(rule).subtract(cost).divide(new BigDecimal(2)).setScale(round, BigDecimal.ROUND_HALF_UP).intValue();
	}
	
	/**
	 * 取得系統商減價描述
	 * @param prodNo
	 * @param roomtypeNo
	 * @param countryCd
	 * @param agentType
	 * @return String
	 */
	public String getAgentRuleDesc(String prodNo, String roomtypeNo, String countryCd, String agentType){
		String result = null;
		Rule rule = null;	
		try{
			result = PricingFormulaDesc.get(this.getUseRuleType(rule).getCode());
		}catch(Exception e){
			logger.error("取得系統商減價描述: {}, {}, {}, {}", prodNo, roomtypeNo, countryCd, agentType);
			logger.error(e.toString());
			result = "";
		}
		return result;
	}
	
	/**
	   * set up agent type which is based on customer id
	   * 查出會員身份對應代碼 (A , E ,B開頭) 
	   * @param customerId
	   * @param source
	   * @param checkin
	   * 手機企業身份：
	   *     E21    企業初級APP-60
	   *     E22    企業中級APP-60
	   *     E23    企業高級APP-60
	   *     E24    企業初級APP當日入住
	   *     E25    企業中級APP當日入住
	   *     E26    企業高級APP當日入住
	   */
	public String getAgentType(String customerId, String source, String checkin) {
		String agentType = "A01";
		if (customerId != null && customerId.trim().length() > 0) {
			CustomerDB customer = erpdb11gCustomerSearchRepository.getCustomerInfo(customerId);
			agentType = customer.getAgentType();
			if (source != null && source.toLowerCase().equals("mobile") && agentType.substring(0, 1).equals("E")) {
				String todayDte = UtilDate.getToday("yyyyMMdd");
				if (!checkin.equals(todayDte)) { // 當日入住
					String par1 = agentType.substring(0, 1);
					String par3 = agentType.substring(2, 3);
					agentType = par1 + "2" + par3;
				} else {
					String par1 = agentType.substring(0, 1);
					String par3 = agentType.substring(2, 3);
					if (par3.equals("1")) {
						par3 = "4";
					} else if (par3.equals("2")) {
						par3 = "5";
					} else if (par3.equals("3")) {
						par3 = "6";
					}
					agentType = par1 + "2" + par3;
				}
			}
		}
		return agentType;
	}
}
