package eztravel.core.service.hotel.extend;

/**
 * Created by jimin on 6/9/15.
 */
public interface SearchHotelInfo {
	public <T> T getInfo(String hotelId);
}
