/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.common
 * @FileName: RoomCommonServiceImpl.java
 * @author:   cano0530
 * @date:     2017/8/2, 下午 04:13:02
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.pojo.DayPriceExtend;
import eztravel.core.pojo.HotelPromoInfo;
import eztravel.core.pojo.HtlMinPrice;
import eztravel.core.pojo.HtlMinPriceExtend;
import eztravel.core.service.hotel.AgentService.Rule;
import eztravel.core.service.hotel.PromoService.FilterType;
import eztravel.rest.pojo.hotel.DayPrice;
import eztravel.rest.pojo.hotel.PromoInfo;
import eztravel.util.UtilDate;

/**
 * <pre> RoomCommonServiceImpl, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class RoomCommonServiceImpl implements RoomCommonService {

  private static final Logger logger = LoggerFactory.getLogger(RoomCommonServiceImpl.class);
  
  @Autowired 
  private PromoService promoService;
  @Autowired 
  private AgentService agentService;
  
  /**
   * 
   */
  public List<DayPrice> caculatRooomDailyPriceList (List<DayPriceExtend> roomPrices ,List<PromoInfo> hotelPromoList ,String agentType ,String checkin,String  checkout  ,String hotelId , String roomId){
    logger.debug("### caculatRooomDailyPriceList START ###");
    List<DayPrice> results = new ArrayList<DayPrice>();
    try{
      
      List<DayPriceExtend> theRoomsPrices = new ArrayList<DayPriceExtend>();
      for(DayPriceExtend roomPrice: roomPrices){
        if(roomId.equals(roomPrice.getRoomId())){
          theRoomsPrices.add(roomPrice);
        }
      } 
      // 取得所有飯店中每房入住區間的所有促銷
      List<PromoInfo> roomsPromos = new ArrayList<PromoInfo>();
      if(hotelPromoList == null || hotelPromoList.size()==0){//沒帶入促銷
        List<String> hotelIds = new ArrayList<String>();
        hotelIds.add(hotelId);
        if(agentType.substring(0, 1).contains("M") || agentType.substring(0, 2).contains("E2")){
          
          List<PromoInfo> hotelspromo = promoService.getHotelPromoInfo(hotelIds, checkin, checkout, FilterType.APP);
          for(PromoInfo promo :  hotelspromo){
            if(promo.getRoomId().equals(roomId)){
                roomsPromos.add(promo);
            }
          }
        }else{
          List<PromoInfo> hotelspromo = promoService.getHotelPromoInfo(hotelIds, checkin, checkout, FilterType.WEB);
          for(PromoInfo promo :  hotelspromo){
            if(promo.getRoomId().equals(roomId)){
                roomsPromos.add(promo);
            }
          }
        }
      }else{//有帶入促銷
        for(PromoInfo promo :  hotelPromoList){
          if(promo.getRoomId().equals(roomId)){
              roomsPromos.add(promo);
          }
        }
      }
      ////mapping promoinfo
      List<DayPriceExtend> promomappRoomsPrices = new ArrayList<DayPriceExtend>();
      List<DayPriceExtend> mapperPromo = this.roomPromoMapping( theRoomsPrices , roomsPromos);
      promomappRoomsPrices = mapperPromo;

      //計算促銷價格

        for ( DayPriceExtend roomPrice : promomappRoomsPrices) {
          if(roomPrice.getDiscountYN() !=null  ){ //如果促銷否沒有打勾,才會去看有無早晚鳥促銷說明
            if(roomPrice.getDiscountYN().trim().equals("N") || roomPrice.getDiscountYN().trim().length()==0){
              int saleprice = this.caculatePromoPrice( roomPrice.getHotelPromo() , roomPrice.getSitePrice() ,roomPrice.getBegDt(),  agentType);
              roomPrice.setPromoprice(saleprice);
              roomPrice.setComputedPromoPrice(roomPrice.getSitePrice()-saleprice);
            }
          }else{
            int saleprice = this.caculatePromoPrice( roomPrice.getHotelPromo() , roomPrice.getSitePrice() ,roomPrice.getBegDt(),  agentType);
            roomPrice.setPromoprice(saleprice);
            roomPrice.setComputedPromoPrice(roomPrice.getSitePrice()-saleprice);
          }
        }

      
      //計算供應商價格
        for ( DayPriceExtend roomPrice : promomappRoomsPrices) {
          if(agentType.substring(0,1).equals("A") || agentType.substring(0,1).equals("B")){
            if(roomPrice.getDiscountYN() !=null  ){ //如果促銷否沒有打勾,才會去看有無早晚鳥促銷說明
              if(roomPrice.getDiscountYN().trim().equals("N") || roomPrice.getDiscountYN().trim().length()==0){
  
                Rule rule = new AgentService.Rule();
                rule.setCost(roomPrice.getCost());
                rule.setPrice(roomPrice.getPromoprice());
                rule.setLevel(Integer.parseInt(roomPrice.getLevelType()));
                rule.setRuleType1(roomPrice.getLevel1_r2());
                rule.setRuleType2(roomPrice.getLevel2_r2());
                rule.setRuleType3(roomPrice.getLevel3_r2());
                rule.setRuleType4(roomPrice.getLevel4_r2());
                int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
                roomPrice.setComputedAgentPrice(roomPrice.getPromoprice() - salepriceAgentRule);
                roomPrice.setPromoprice(salepriceAgentRule); 

              }
            }else{
              Rule rule = new AgentService.Rule();
              rule.setCost(roomPrice.getCost());
              rule.setPrice(roomPrice.getPromoprice());
              rule.setLevel(Integer.parseInt(roomPrice.getLevelType()));
              rule.setRuleType1(roomPrice.getLevel1_r2());
              rule.setRuleType2(roomPrice.getLevel2_r2());
              rule.setRuleType3(roomPrice.getLevel3_r2());
              rule.setRuleType4(roomPrice.getLevel4_r2());
              int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
              roomPrice.setComputedAgentPrice(roomPrice.getPromoprice() - salepriceAgentRule);
              roomPrice.setPromoprice(salepriceAgentRule); 

            }
          }else if(agentType.substring(0,1).equals("M") || agentType.substring(0,1).equals("E")){
              Rule rule = new AgentService.Rule();
              rule.setCost(roomPrice.getCost());
              rule.setPrice(roomPrice.getPromoprice());
              rule.setLevel(Integer.parseInt(roomPrice.getLevelType()));
              rule.setRuleType1(roomPrice.getLevel1_r2());
              rule.setRuleType2(roomPrice.getLevel2_r2());
              rule.setRuleType3(roomPrice.getLevel3_r2());
              rule.setRuleType4(roomPrice.getLevel4_r2());
              int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
              roomPrice.setComputedAgentPrice(roomPrice.getPromoprice() - salepriceAgentRule);
              roomPrice.setPromoprice(salepriceAgentRule); 
          }
        }
      
        results = this.convertRoomDayPrice(theRoomsPrices, checkin, checkout, agentType);
    
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.debug("### caculatRooomDailyPriceList END ###");
    return results;
  }

  
  private List<DayPriceExtend> roomPromoMapping(List<DayPriceExtend> roomPrice , List<PromoInfo> promoInfos) {
    long startTime = System.currentTimeMillis();

    logger.debug("### roomPromoMapping START ###");
    
    List<DayPriceExtend> result = new ArrayList<DayPriceExtend>();
    try{
      for(DayPriceExtend rprice : roomPrice){
        List<HotelPromoInfo> promos = new ArrayList<HotelPromoInfo>();
        for(PromoInfo promoInfo :promoInfos){
          if(rprice.getBegDt().equals(promoInfo.getAppDate())){
            HotelPromoInfo promo = new HotelPromoInfo();
            promo.setAppRange(promoInfo.getAppRange());
            promo.setBegDate(promoInfo.getAppDate());
            promo.setCostType(promoInfo.getCostType());
            promo.setDiscountType(promoInfo.getDiscountType());
            promo.setPromoType(promoInfo.getPromoType());
            promo.setPromoDay(promoInfo.getPromoDay());
            promo.setPromoRate(promoInfo.getPromoRate());
            promos.add(promo);
          } 
        }
        rprice.setHotelPromo(promos);
        
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    result = roomPrice;

    logger.debug("### roomPromoMapping END ### (" + (System.currentTimeMillis() - startTime) + " ms)");
    return result;
  }
  
  
  private int caculatePromoPrice( List<HotelPromoInfo> promoInfos , int sitePrice ,String begDt, String agentType) {
    long startTime = System.currentTimeMillis();

    logger.debug("### caculatepromoprice START ###");
    BigDecimal salePrice = new BigDecimal(Integer.toString(sitePrice));
    List<Double> percentpromorate = new ArrayList<Double>();
    List<Double> intpromorate = new ArrayList<Double>();
    
    try{
      for(HotelPromoInfo  promoInfo :promoInfos){
        if(!agentType.substring(0, 1).equals("M") && !agentType.substring(0, 2).equals("E2")){//非app
          if(!promoInfo.getAppRange().equals("3")){//不包含app only
            if(begDt.equals(promoInfo.getBegDate())){
              if(promoInfo.getDiscountType().equals("1")){ //百分比  
                percentpromorate.add(promoInfo.getPromoRate());
              }else if(promoInfo.getDiscountType().equals("2")){//扣定額
                intpromorate.add(promoInfo.getPromoRate());
              }
            }
          }
        }else{ //app或app b2e
          if(!promoInfo.getAppRange().equals("2")){//不包含web
            if(begDt.equals(promoInfo.getBegDate())){
              if(promoInfo.getDiscountType().equals("1")){ //百分比  
                percentpromorate.add(promoInfo.getPromoRate());
              }else if(promoInfo.getDiscountType().equals("2")){//扣定額
                intpromorate.add(promoInfo.getPromoRate());
              }
            }
          }
        }
      }
      //促銷後再最後做四捨五入取整位數,算促銷的過程中不做任何取捨,成本算法也是這麼做
      //先算折扣
      
      for(int i=0 ; i<percentpromorate.size() ; i++){
    	BigDecimal bPercentpromorate = new BigDecimal(Double.toString(percentpromorate.get(i)));
    	salePrice = salePrice.multiply(bPercentpromorate);
      }
      salePrice = salePrice.setScale(0, BigDecimal.ROUND_HALF_UP);
      //再算扣定額
      for(int i=0 ; i<intpromorate.size() ; i++){
    	BigDecimal bIntpromorate = new BigDecimal(Double.toString(intpromorate.get(i)));
    	salePrice = salePrice.subtract(bIntpromorate);
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.debug("### caculatepromoprice END ### (" + (System.currentTimeMillis() - startTime) + " ms)");
    return salePrice.intValue();
  }
  
  
private List<DayPrice> convertRoomDayPrice(List<DayPriceExtend> roomPrices ,String checkin , String checkout ,String  agentType){
    
    
    List<DayPrice> results = new ArrayList<DayPrice>();
    
    try{
      
      //過濾不等於入住天數  成本小於最低價
      for(DayPriceExtend result: roomPrices){

          if(agentType.subSequence(0, 1).equals("B")) {//b2b售價要大於 成本
            if(result.getPromoprice() > result.getCost() ){
              DayPrice dayprice = new DayPrice();
              dayprice.setDay(result.getBegDt());
              dayprice.setPrice(result.getSitePrice());
              dayprice.setQty(result.getQty());
              dayprice.setSalePrice(result.getPromoprice());
              dayprice.setComputedAgentPrice(result.getComputedAgentPrice());
              dayprice.setComputedPromoPrice(result.getComputedPromoPrice());
              results.add(dayprice);
            }
          }else if(agentType.subSequence(0, 1).equals("A") || agentType.subSequence(0, 1).equals("E")){ //b2e b2c售價要大於 0
            if(result.getPromoprice() > 0 ){
              DayPrice dayprice = new DayPrice();
              dayprice.setDay(result.getBegDt());
              dayprice.setPrice(result.getSitePrice());
              dayprice.setQty(result.getQty());
              dayprice.setSalePrice(result.getPromoprice());
              if(agentType.subSequence(0, 2).equals("E2")){
                dayprice.setMobileSalePrice(result.getPromoprice());
              }
              dayprice.setComputedAgentPrice(result.getComputedAgentPrice());
              dayprice.setComputedPromoPrice(result.getComputedPromoPrice());
              results.add(dayprice);
            }
          }else{ //mobile售價 不用比較是否低於成本
            DayPrice dayprice = new DayPrice();
            dayprice.setDay(result.getBegDt());
            dayprice.setPrice(result.getSitePrice());
            dayprice.setQty(result.getQty());
            dayprice.setSalePrice(result.getPromoprice());
            dayprice.setMobileSalePrice(result.getPromoprice());
            dayprice.setComputedAgentPrice(result.getComputedAgentPrice());
            dayprice.setComputedPromoPrice(result.getComputedPromoPrice());
            results.add(dayprice);
          }

      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    return results;
  }



/**
 * 計算促銷價供應商最低價.
 * 
 * @param htlsMinPrices
 *          the htls min prices
 * @param agentType
 *          the agent type
 * @param checkin
 *          the checkin
 * @param checkout
 *          the checkout
 * @param hotelIds
 *          the hotel ids
 * @param type
 *          the type
 *         all:全部房型房價 ; mins:最低價的房型
 * @return List<HtlMinPrice>
 */ 
  @Override
  public List<List<HtlMinPrice>> caculatHotelLowPriceList (List<List<HtlMinPriceExtend>> htlsMinPrices ,String agentType ,String checkin,String  checkout , List<List<String>> hotelIds ,String type){
    logger.debug("### caculatHotelLowPriceList START ###");
    List<List<HtlMinPrice>> results = new ArrayList<>(hotelIds.size());
    try{
      // 取得所有飯店中每房入住區間的所有促銷
      List<List<PromoInfo>> hotelsPromos = new ArrayList<>();
      for (List<String> partOfHotelIds : hotelIds) {
        if(agentType.substring(0, 1).contains("M") || agentType.substring(0, 2).contains("E2")){
          List<PromoInfo> hotelspromo = promoService.getHotelPromoInfo(partOfHotelIds, checkin, checkout, FilterType.APP);
          hotelsPromos.add(hotelspromo);
        }else{
          List<PromoInfo> hotelspromo = promoService.getHotelPromoInfo(partOfHotelIds, checkin, checkout, FilterType.WEB);
          hotelsPromos.add(hotelspromo);
        }
      }
  
      ////mapping promoinfo
      List<List<HtlMinPriceExtend>> promomapphtlsMinPrices = new ArrayList<>(hotelIds.size());
      for (int i=0 ;i<htlsMinPrices.size() ;i++) {
        
          List<HtlMinPriceExtend> mapperPromo = this.hotelPromoMapping( htlsMinPrices.get(i) , hotelsPromos.get(i));
          promomapphtlsMinPrices.add(mapperPromo);
        
      }
      
      
      //計算促銷價格
      for (List<HtlMinPriceExtend> partOfHtlMinPrices : promomapphtlsMinPrices) {
        for (final HtlMinPriceExtend htlMinPrice : partOfHtlMinPrices) {
          if(htlMinPrice.getDiscountYN() !=null  ){ //如果促銷否沒有打勾,才會去看有無早晚鳥促銷說明
            if(htlMinPrice.getDiscountYN().trim().equals("N") || htlMinPrice.getDiscountYN().trim().length()==0){
              int saleprice = this.caculatePromoPrice( htlMinPrice.getHotelPromo() , htlMinPrice.getSitePrice() ,htlMinPrice.getBegDt(),  agentType);
              htlMinPrice.setPromoprice(saleprice);
            }
          }else{
            int saleprice = this.caculatePromoPrice( htlMinPrice.getHotelPromo() , htlMinPrice.getSitePrice() ,htlMinPrice.getBegDt(),  agentType);
            htlMinPrice.setPromoprice(saleprice);
          }
        }
      }
      
      //計算供應商價格
      for (List<HtlMinPriceExtend> partOfHtlMinPrices : promomapphtlsMinPrices) {
        for (final HtlMinPriceExtend htlMinPrice : partOfHtlMinPrices) {
          if(agentType.substring(0,1).equals("A") || agentType.substring(0,1).equals("B")){
            if(htlMinPrice.getDiscountYN() !=null  ){ //如果促銷否沒有打勾,才會去看有無早晚鳥促銷說明
              if(htlMinPrice.getDiscountYN().trim().equals("N") || htlMinPrice.getDiscountYN().trim().length()==0){
  
                Rule rule = new AgentService.Rule();
                rule.setCost(htlMinPrice.getCost());
                rule.setPrice(htlMinPrice.getPromoprice());
                rule.setLevel(Integer.parseInt(htlMinPrice.getLevelType()));
                rule.setRuleType1(htlMinPrice.getLevel1_r2());
                rule.setRuleType2(htlMinPrice.getLevel2_r2());
                rule.setRuleType3(htlMinPrice.getLevel3_r2());
                rule.setRuleType4(htlMinPrice.getLevel4_r2());
                int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
                htlMinPrice.setPromoprice(salepriceAgentRule); 

              }
            }else{
              Rule rule = new AgentService.Rule();
              rule.setCost(htlMinPrice.getCost());
              rule.setPrice(htlMinPrice.getPromoprice());
              rule.setLevel(Integer.parseInt(htlMinPrice.getLevelType()));
              rule.setRuleType1(htlMinPrice.getLevel1_r2());
              rule.setRuleType2(htlMinPrice.getLevel2_r2());
              rule.setRuleType3(htlMinPrice.getLevel3_r2());
              rule.setRuleType4(htlMinPrice.getLevel4_r2());
              int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
              htlMinPrice.setPromoprice(salepriceAgentRule); 
            }
          }else if(agentType.substring(0,1).equals("M") || agentType.substring(0,1).equals("E")){
              Rule rule = new AgentService.Rule();
              rule.setCost(htlMinPrice.getCost());
              rule.setPrice(htlMinPrice.getPromoprice());
              rule.setLevel(Integer.parseInt(htlMinPrice.getLevelType()));
              rule.setRuleType1(htlMinPrice.getLevel1_r2());
              rule.setRuleType2(htlMinPrice.getLevel2_r2());
              rule.setRuleType3(htlMinPrice.getLevel3_r2());
              rule.setRuleType4(htlMinPrice.getLevel4_r2());
              int salepriceAgentRule=  agentService.usfHtlAgentRuleM(rule);
              htlMinPrice.setPromoprice(salepriceAgentRule); 
          }
        }
      }
      
      //將所有天數加總起來
      List<List<HtlMinPrice>> mergerhtlsMinPrices = new ArrayList<>(hotelIds.size());
      for (List<HtlMinPriceExtend> partOfHtlMinPrices : promomapphtlsMinPrices) {
        List<HtlMinPrice> hotelRoomMeger = this.groupHtlMinPriceByRooms(partOfHtlMinPrices ,checkin, checkout ,agentType);
        mergerhtlsMinPrices.add(hotelRoomMeger);
      }
      if(type.equals("mins")){
        //比對出每一飯店最小的總價
        List<List<HtlMinPrice>> lowesthtlsMinPrices= new ArrayList<>(hotelIds.size());
          for (List<HtlMinPrice> partOfHtlMinPrices : mergerhtlsMinPrices) {
            List<HtlMinPrice> hotelRoomLowest = this.compareLowesttPrice(partOfHtlMinPrices);
            lowesthtlsMinPrices.add(hotelRoomLowest);
          }
          
          results= lowesthtlsMinPrices;
      }else{
        results = mergerhtlsMinPrices;
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.debug("### caculatHotelLowPriceList END ###");
    return results;
  }
  
  /**
   * 將入住天數的所有價錢加總起來並篩選,不等於入住天數 成本小於最低價.
   * 
   * @param byDays
   *          the by days
   * @param checkin
   *          the checkin
   * @param checkout
   *          the checkout
   * @param agentType
   *          the agent type
   * @return List<HtlMinPrice>
   */  
private List<HtlMinPrice> groupHtlMinPriceByRooms(List<HtlMinPriceExtend> byDays ,String checkin , String checkout ,String  agentType){
    
    
    List<HtlMinPrice> results = new ArrayList<HtlMinPrice>();
    
    try{
      int diffday = UtilDate.getDiffDays(checkin , checkout);//入住天數
      //原入住日pojo職換成房型彙整pojo
      //int count =0;//計數有假錢的日期
      Map<String, ConverHtlMinPrice> cov = new HashMap<String, ConverHtlMinPrice>();
      for(HtlMinPriceExtend roomDay: byDays){
        ConverHtlMinPrice model = null;
        //if(roomDay.getHotelId().equals("HTLI000000516")){
        //  System.out.println(roomDay.getHotelId());
        //}
        // 篩選飯店數量為0
      	if("001".equals(roomDay.getTravelType()) && roomDay.getQty() == 0){
      		continue;
      	}
        model = cov.get(roomDay.getHotelId() + roomDay.getRoomId());
        if(model == null){
          model = new ConverHtlMinPrice();
          BeanUtils.copyProperties(roomDay, model );
          model.setAvgSalePrice(model.getAvgSalePrice() + roomDay.getPromoprice());
          model.setAvgSitePrice(model.getAvgSitePrice() + roomDay.getSitePrice());
          model.setCosts(model.getCosts() + roomDay.getCost());
          model.setTotaldays(model.getTotaldays() +1);
        }else{
          model.setAvgSalePrice(model.getAvgSalePrice() + roomDay.getPromoprice());
          model.setAvgSitePrice(model.getAvgSitePrice() + roomDay.getSitePrice());
          model.setCosts(model.getCosts() + roomDay.getCost());
          model.setTotaldays(model.getTotaldays() +1);
          
        }
        cov.put(roomDay.getHotelId() + roomDay.getRoomId(), model);
      }
      //過濾不等於入住天數  成本小於最低價
      for(ConverHtlMinPrice result: cov.values()){
        if(diffday == result.getTotaldays()){
          if(agentType.subSequence(0, 1).equals("B")) {//b2b售價要大於 成本
            if(result.getAvgSalePrice() > result.getCosts() ){
              result.setAvgSalePrice(Math.round(result.getAvgSalePrice()/diffday));
              result.setAvgSitePrice(Math.round(result.getAvgSitePrice()/diffday));
              result.setCosts(Math.round(result.getCosts()/diffday));
              results.add(result);
            }
          }else if(agentType.subSequence(0, 1).equals("A") || agentType.subSequence(0, 1).equals("E")){ //b2e b2c售價要大於 0
            if(result.getAvgSalePrice() > 0 ){
              result.setAvgSalePrice(Math.round(result.getAvgSalePrice()/diffday));
              result.setAvgSitePrice(Math.round(result.getAvgSitePrice()/diffday));
              result.setCosts(Math.round(result.getCosts()/diffday));
              results.add(result);
            }
          }else{ //mobile售價 不用比較是否低於成本
            result.setAvgSalePrice(Math.round(result.getAvgSalePrice()/diffday));
            result.setAvgSitePrice(Math.round(result.getAvgSitePrice()/diffday));
            result.setCosts(Math.round(result.getCosts()/diffday));
            if(result.getHotelPromo()!=null && result.getHotelPromo().size()>0){//是否有促銷
              for(int i=0 ;i<result.getHotelPromo().size() ; i++){
                if(result.getHotelPromo().get(i).getPromoType().equals("004")){//看有無app only促銷
                  result.setPromotionType(result.getHotelPromo().get(i).getPromoType());
                }
              }

            }
            results.add(result);
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    return results;
  }

/**
 * 篩選房型最低價.
 * 
 * @param htlMinPrices
 *          the htl min prices
 * @return List<HtlMinPrice>
 */
private List<HtlMinPrice> compareLowesttPrice(List<HtlMinPrice> htlMinPrices){
  Map<String, HtlMinPrice> result = new HashMap<String, HtlMinPrice>();
  Map<String, Integer> compare = new HashMap<String, Integer>();
  
  try{
    for (HtlMinPrice hotel : htlMinPrices) {
      //if(hotel.getHotelId().equals("HTLI000000516")){
      //  System.out.println(hotel.getHotelId());
      //}
      
      if(compare.containsKey(hotel.getHotelId())){
        if(hotel.getAvgSalePrice() < compare.get(hotel.getHotelId())){
          result.put(hotel.getHotelId(), hotel);
          compare.put(hotel.getHotelId(), hotel.getAvgSalePrice());
        }else{

          if(hotel.getAvgSalePrice() == compare.get(hotel.getHotelId())){//如果最低價相同
        
            if(hotel.getPromotionType()!= null && hotel.getPromotionType().equals("004")){//有app only 優先顯示
              result.put(hotel.getHotelId(), hotel);
              compare.put(hotel.getHotelId(), hotel.getAvgSalePrice());
            }
          }
        }
      }else{
        result.put(hotel.getHotelId(), hotel);
        compare.put(hotel.getHotelId(), hotel.getAvgSalePrice());
      }
    }
  } catch (Exception e) {
    logger.error(e.getMessage(), e);
  }
  List<HtlMinPrice> valuesList = new ArrayList<HtlMinPrice>(result.values());
  
  return valuesList;
}

/**
 * mapping promoinfo in to HtlMinPriceExtend.
 * 
 * @param htlMinPrices
 *          the htl min prices
 * @param promoInfos
 *          the promo infos
 * @return List<HtlMinPriceExtend>
 */
private List<HtlMinPriceExtend> hotelPromoMapping(List<HtlMinPriceExtend> htlMinPrices , List<PromoInfo> promoInfos) {
  long startTime = System.currentTimeMillis();

  logger.debug("### hotelPromoMapping START ###");
  
  List<HtlMinPriceExtend> result = new ArrayList<HtlMinPriceExtend>();
  try{
    for(HtlMinPriceExtend htlMinPrice : htlMinPrices){
      List<HotelPromoInfo> promos = new ArrayList<HotelPromoInfo>();
      //if(htlMinPrice.getHotelId().equals("HTLI000000516")){
      //  System.out.println(htlMinPrice.getHotelId());
      //}
      for(PromoInfo promoInfo :promoInfos){
        if(htlMinPrice.getHotelId().equals(promoInfo.getHotelId())){
          if(htlMinPrice.getRoomId().equals(promoInfo.getRoomId())){
            if(htlMinPrice.getBegDt().equals(promoInfo.getAppDate())){
              HotelPromoInfo promo = new HotelPromoInfo();
              promo.setAppRange(promoInfo.getAppRange());
              promo.setBegDate(promoInfo.getAppDate());
              promo.setCostType(promoInfo.getCostType());
              promo.setDiscountType(promoInfo.getDiscountType());
              promo.setPromoType(promoInfo.getPromoType());
              promo.setPromoDay(promoInfo.getPromoDay());
              promo.setPromoRate(promoInfo.getPromoRate());
              promos.add(promo);
            }
            
          }
          
        }
        
      }
      htlMinPrice.setHotelPromo(promos);
      
    }
  } catch (Exception e) {
    logger.error(e.getMessage(), e);
  }
  result = htlMinPrices;

  logger.debug("### hotelPromoMapping END ### (" + (System.currentTimeMillis() - startTime) + " ms)");
  return result;
}

/**
 * 彙整房型總價使用.
 */
class ConverHtlMinPrice extends HtlMinPrice {
  
  /** The hotel promo. */
  private List<HotelPromoInfo> hotelPromo;
  
  /** The costs. */
  private int costs;
  
  /** The totaldays. */
  private int totaldays;
  

  
  /**
   * Gets the hotel promo.
   * 
   * @return the hotel promo
   */
  public List<HotelPromoInfo> getHotelPromo() {
    return hotelPromo;
  }
  
  /**
   * Sets the hotel promo.
   * 
   * @param hotelPromo
   *          the new hotel promo
   */
  public void setHotelPromo(List<HotelPromoInfo> hotelPromo) {
    this.hotelPromo = hotelPromo;
  }
  
  /**
   * Gets the costs.
   * 
   * @return the costs
   */
  public int getCosts() {
    return costs;
  }
  
  /**
   * Sets the costs.
   * 
   * @param costs
   *          the new costs
   */
  public void setCosts(int costs) {
    this.costs = costs;
  }
  
  /**
   * Gets the totaldays.
   * 
   * @return the totaldays
   */
  public int getTotaldays() {
    return totaldays;
  }
  
  /**
   * Sets the totaldays.
   * 
   * @param totaldays
   *          the new totaldays
   */
  public void setTotaldays(int totaldays) {
    this.totaldays = totaldays;
  }
  
  
}
}
