package eztravel.core.service.hotel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.enums.Cache;
import eztravel.core.pojo.DayPriceExtend;
import eztravel.core.pojo.HtlMinPrice;
import eztravel.core.pojo.HtlMinPriceExtend;
import eztravel.core.pojo.InternetStatus;
import eztravel.core.service.CommonService;
import eztravel.core.service.hotel.PromoService.FilterType;
import eztravel.core.service.hotel.extend.SearchFacility;
import eztravel.core.service.hotel.extend.SearchHotelDetail;
import eztravel.core.service.hotel.extend.SearchMinsRoom;
import eztravel.core.service.hotel.extend.SearchPhoto;
import eztravel.core.service.hotel.extend.SearchThread;
import eztravel.core.service.hotel.extend.SearchViews;
import eztravel.core.service.hotel.extend.UsedEquipsId;
import eztravel.persistence.repository.edb.RoomQtyEdbRepository;
import eztravel.persistence.repository.oracle.SearchRepository;
import eztravel.persistence.repository.oracle11g.Erpdb11gCustomerSearchRepository;
import eztravel.persistence.repository.oracle11g.Erpdb11gHotelSearchRepository;
import eztravel.persistence.repository.oracle11g.Erpdb11gSearchRepository;
import eztravel.rest.pojo.hotel.Bedsize;
import eztravel.rest.pojo.hotel.CacheWrapper;
import eztravel.rest.pojo.hotel.CodeDetail;
import eztravel.rest.pojo.hotel.DayPrice;
import eztravel.rest.pojo.hotel.HotelFacility;
import eztravel.rest.pojo.hotel.HotelStandalone;
import eztravel.rest.pojo.hotel.PromoInfo;
import eztravel.rest.pojo.hotel.Range;
import eztravel.rest.pojo.hotel.Room;
import eztravel.util.HotelUtil;
import eztravel.util.ThreadPoolCenter;
import eztravel.util.UtilDate;

public class SearchServiceImpl extends CommonService implements SearchService {
	private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
	private String nationality = "TW";
	List<Integer> usedEquipIds;

	@Autowired
	private CacheService cacheService;
	@Autowired
	private RedisService redisService;
	@Autowired
	private PreCacheService preCacheService;
	@Autowired
	private RoomCommonService roomCommonService;
	@Autowired
	private ActiveRoomService activeRoomService ;
	@Autowired
	private PromoService promoService;
	@Autowired
	private CancelService cancelService;
	@Autowired
	private AgentService agentService;
	@Autowired
	private FacilityService facilityService;

	@Autowired
	private SearchRepository searchRepository;
	@Autowired
	private Erpdb11gSearchRepository erpdb11gSearchRepository;
	@Autowired
	private Erpdb11gHotelSearchRepository erpdb11gHotelSearchRepository;
	@Autowired
	private RoomQtyEdbRepository roomQtyEdbRepository;
	@Autowired
	private Erpdb11gCustomerSearchRepository erpdb11gCustomerSearchRepository;

	/**
	 * 取得該飯店所有可售房型列表資料及價錢(產品頁下半部). 
	 * 1. WEB頁面上的價錢是看agentType(customerId所對應到的身份)來算出價錢
	 * 2. APP頁面上的web價錢,因為APP目前傳入的customerId都是A01,所以APP的web價錢都是A01 
	 * 3. APP的售價是看mobileAgentType :當日則為M02,預設為M01 
	 * 4. 使用cache如果撈不到才撈db 
	 * 5.會更新該飯店所屬city列表的資料 
	 * 6. version來自v1或v2(有包含ctrip台灣房)
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param priceRanges
	 * @param customerId
	 * @param source (app會帶mobile , 其他都不會帶)
	 * @param version
	 * @return the hotel rooms
	 * @throws Exception
	 */
	@Override
	public List<Room> getHotelRooms(final String hotelId, String roomId, final int roomQty, final String checkin, final String checkout, List<Range> priceRanges, String customerId, final String source, final String version) throws Exception {
		long startTime = System.currentTimeMillis();
		logger.debug("### getHotelRooms START {} ###", hotelId);
		List<Room> results = new ArrayList<Room>();
		List<SearchMinsRoom> allRooms = new ArrayList<SearchMinsRoom>();
		final String customer = customerId;
		final String Source = source;
		long tmpTime = System.currentTimeMillis();

		// 判斷customer身分 start
		String agentType = agentService.getAgentType(customerId, source, checkin);// 先取得到customer的身份 EX:A0X ,B0X ,E0X ,E2X
		String srcAgentType = "";// 最終身份
		String mobileAgentType = "M01";
		if (source != null && source.toLowerCase().equals("mobile")) {// 在看若是app 是否為非當日或當日的 ex:M01 OR M02
			// 當日訂房app使用 M02 AGENT type -6%折扣
			String todayDte = UtilDate.getToday("yyyyMMdd");
			// 是否為當日
			if (checkin.equals(todayDte)) {
				String agentTypeDB = erpdb11gCustomerSearchRepository.vaildAgentRule(hotelId);
				if (agentTypeDB != null && agentTypeDB != "" && agentTypeDB != " ") {
					mobileAgentType = agentTypeDB;
				}
			}
			if (agentType.substring(0, 1).equals("E") || agentType.substring(0, 1).equals("B")) {// 若來源app b2e或B2B身份登入,則為 E2X or B0X
				srcAgentType = agentType;
			} else { // 若來源app 一般身份登入,則為 M01 ,M02
				srcAgentType = mobileAgentType;
			}
		} else { // 若來源非app,則身分就為customer
			srcAgentType = agentType;
		}
		// 判斷customer身分 end
		HotelStandalone htlInfo = this.getHotelInfo(hotelId);
		results = preCacheService.getCacheRoomList(hotelId, roomId, roomQty, checkin, checkout, customerId, source);
		if (results == null || results.size() == 0) {
			allRooms = this.caculatehtlRoomsSalesPrice(hotelId, roomId, roomQty, checkin, checkout, srcAgentType);
			if (allRooms != null && allRooms.size() == 0) {
				logger.debug("### getHotelRooms END - empty {} ### ({}ms)", hotelId, System.currentTimeMillis() - tmpTime);
				activeRoomService.AsyncHotelList(hotelId, htlInfo.getCityCd(), null, null, checkin, checkout, customerId, Source, null, srcAgentType);
			}
			allRooms = this.getMasterMinRoomQty(allRooms, hotelId, checkin, checkout);
			if (allRooms != null && allRooms.size() > 0) {

				// 取得個房型的網路描述代碼
				List<InternetStatus> internetStatus = erpdb11gSearchRepository.getRoomsInternetStatus(hotelId, allRooms);
				List<InternetStatus> roomsInternetStatus = this.mappingSortByRoomList(allRooms, internetStatus);
				tmpTime = System.currentTimeMillis();
				ExecutorService bedPool = ThreadPoolCenter.getIoPool();
				List<Future<List<Bedsize>>> bedResults = new ArrayList<Future<List<Bedsize>>>(allRooms.size());
				ExecutorService photoPool = ThreadPoolCenter.getIoPool();
				List<Future<List<String>>> photoResults = new ArrayList<Future<List<String>>>(allRooms.size());
				ExecutorService cancelPool = ThreadPoolCenter.getIoPool();
				List<Future<List<String>>> cancelResults = new ArrayList<Future<List<String>>>(allRooms.size());
				ExecutorService internetPool = ThreadPoolCenter.getIoPool();
				List<Future<String>> internetDescResults = new ArrayList<>(allRooms.size());
				ExecutorService changePool = ThreadPoolCenter.getIoPool();
				List<Future<List<String>>> changeResults = new ArrayList<Future<List<String>>>(allRooms.size());
				int i = 0;
				for (SearchMinsRoom room : allRooms) {
					// 取得床型尺寸
					@SuppressWarnings("unchecked")
					Future<List<Bedsize>> futureBeds = bedPool.submit(new Callable<List<Bedsize>>() {
						private String roomId;
						private Callable init(String roomId) {
							this.roomId = roomId;
							return this;
						}
						@Override
						public List<Bedsize> call() throws Exception {
							return erpdb11gSearchRepository.getRoomBedSize(hotelId, roomId);
						}
					}.init(room.getRoomId()));

					// 取得房型照片
					@SuppressWarnings("unchecked")
					Future<List<String>> futurePhoto = photoPool.submit(new Callable<List<String>>() {
						private String roomId;
						private Callable init(String roomId) {
							this.roomId = roomId;
							return this;
						}
						@Override
						public List<String> call() throws Exception {
							return erpdb11gSearchRepository.getRoomPhotos(hotelId, roomId);
						}
					}.init(room.getRoomId()));

					@SuppressWarnings("unchecked")
					Future<List<String>> futureCancel = cancelPool.submit(new Callable<List<String>>() {
						private String roomId;
						private String nationality;
						private String agentType;

						private Callable init(String roomId, String nationality, String agentType) {
							this.roomId = roomId;
							this.nationality = nationality;
							this.agentType = agentType;
							return this;
						}

						@Override
						public List<String> call() throws Exception {
							return cancelService.getCancelDescs(hotelId, roomId, roomQty, checkin, checkout, customer, nationality, agentType, "N");
						}
					}.init(room.getRoomId(), this.nationality, srcAgentType));

					@SuppressWarnings("unchecked")
					Future<List<String>> futureChange = changePool.submit(new Callable<List<String>>() {
						private String roomId;
						private Callable init(String roomId) {
							this.roomId = roomId;
							return this;
						}
						@Override
						public List<String> call() throws Exception {
							return cancelService.getCancelChangeDesc(hotelId, roomId, checkin, checkout);
						}
					}.init(room.getRoomId()));

					@SuppressWarnings("unchecked")
					Future<String> futureInternet = internetPool.submit(new Callable<String>() {
						private InternetStatus internetStatus;
						private Callable init(InternetStatus internetStatus) {
							this.internetStatus = internetStatus;
							return this;
						}
						@Override
						public String call() throws Exception {
							return translateInternetType(internetStatus.getInternetStatus(), internetStatus.getInternetCharge());
						}
					}.init(roomsInternetStatus.get(i++)));
					bedResults.add(futureBeds);
					photoResults.add(futurePhoto);
					cancelResults.add(futureCancel);
					changeResults.add(futureChange);
					internetDescResults.add(futureInternet);
				}
				List<DayPriceExtend> totalDatePriceFinal = erpdb11gSearchRepository.getRoomDailyPriceExtend(hotelId, null, roomQty, checkin, checkout, "TW", srcAgentType);
				i = 0;
				List<DayPrice> datePriceFinal = new ArrayList<DayPrice>();
				List<String> hotelIds = new ArrayList<String>();
				hotelIds.add(hotelId);
				List<PromoInfo> totalRoomPromoInfo = promoService.getHotelPromoInfo(hotelIds, checkin, checkout, source.toLowerCase().equals("mobile") ? FilterType.APP : null);
				for (SearchMinsRoom room : allRooms) {
					try {
						room.setBedSize(bedResults.get(i).get());
						room.setPhotoURLs(photoResults.get(i).get());
						room.setCancelIconDescs(cancelResults.get(i).get());
						room.setCancelChangeDesc(changeResults.get(i).get().get(0));
						room.setInternetDescription(internetDescResults.get(i).get());

						// 計算出agentType((A0X ,B0X ,E0X ,E2X)) 其每日價錢
						// salePrice : 計算促銷價+供應商(agentType = B2C:(A01) or
						// B2E:(E0X) or APP+B2E:(E2X) or B2B:(B0X))
						datePriceFinal = roomCommonService.caculatRooomDailyPriceList(totalDatePriceFinal, totalRoomPromoInfo, srcAgentType, checkin, checkout, hotelId, room.getRoomId());
						room.setDayPrice(datePriceFinal);
						if (room.getDiscountYn() != null) { // 如果有促銷否和早晚鳥,同時存在,會優先看促銷否折扣,在看有無早晚鳥折扣
							if (room.getDiscountYn().trim().equals("N") || room.getDiscountYn().trim().length() == 0) {// 沒有勾促銷否
								// 取得早鳥晚鳥說明
								List<PromoInfo> roomPromoInfo = new ArrayList<PromoInfo>();
								for (PromoInfo promo : totalRoomPromoInfo) {
									if (promo.getRoomId().equals(room.getRoomId())) {
										roomPromoInfo.add(promo);
									}
								}
								if (roomPromoInfo != null && roomPromoInfo.size() > 0) {
									room.setPromoInfo(roomPromoInfo);
								} else {
									room.setPromoInfo(null);
								}
							} else {// 有勾促銷否
								room.setPromoInfo(null);
							}
						} else {
							// 取得早鳥晚鳥說明
							List<PromoInfo> roomPromoInfo = new ArrayList<PromoInfo>();
							for (PromoInfo promo : totalRoomPromoInfo) {
								if (promo.getRoomId().equals(room.getRoomId())) {
									roomPromoInfo.add(promo);
								}
							}
							if (roomPromoInfo != null && roomPromoInfo.size() > 0) {
								room.setPromoInfo(roomPromoInfo);
							} else {
								room.setPromoInfo(null);
							}
						}
						i++;
					} catch (InterruptedException e) {
						logger.error(e.toString());
					} catch (ExecutionException e) {
						logger.error(e.toString());
					}
				}
				for (SearchMinsRoom room : allRooms) {
					results.add(room.getRoom());
				}
				preCacheService.setCacheRoomlList(results, hotelId, roomId, roomQty, checkin, checkout, customerId, Source);
			} else {
				if (allRooms != null && allRooms.size() == 0) {
					logger.debug("### getHotelRooms END - empty {} ### ({}ms)", hotelId, System.currentTimeMillis() - tmpTime);
				}
			}
		}

		if (results != null && results.size() > 0) {
			// 入住日超過今日「兩天」則會讓候補房一起拋出去,若沒有責過濾掉
			if (HotelUtil.isQueryingWaiting(checkin)) {
				List<Room> tempresults = new ArrayList<Room>();
				for (int i = 0; i < results.size(); i++) {
					tempresults.add(results.get(i));
				}
				results = tempresults;
			}
		}
		if (version.equals("v2")) {
			/**
			 * 攜程台灣房 假設在今天15:00以前最快可訂明天入住 (3/29 可搜3/30) 15:00 後最快可訂明天+1入住 (3/29
			 * 可搜3/31)
			 *
			String todayDte = UtilDate.getToday("yyyyMMdd");
			if (Integer.parseInt(UtilDate.getTodayHHMM()) < 1500) {
				String aftDte = UtilDate.getAfterDate(todayDte, 1);
				if (UtilDate.getDiffDays(aftDte, checkin) >= 0) {
					List<Room> ctripRoomsTw = this.getCtripTwHotelRoomList(hotelId, roomId, checkin, checkout, roomQty, srcAgentType, customerId, source);// 取得ctrip台灣房,房型列表資訊
					if (ctripRoomsTw != null && ctripRoomsTw.size() > 0) {
						results = this.filterLowestPrice(results, ctripRoomsTw);
					}
				}
			} else {
				String aftDte = UtilDate.getAfterDate(todayDte, 2);
				if (UtilDate.getDiffDays(aftDte, checkin) >= 0) {
					List<Room> ctripRoomsTw = this.getCtripTwHotelRoomList(hotelId, roomId, checkin, checkout, roomQty, srcAgentType, customerId, source);// 取得ctrip台灣房,房型列表資訊
					if (ctripRoomsTw != null && ctripRoomsTw.size() > 0) {
						results = this.filterLowestPrice(results, ctripRoomsTw);
					}
				}
			}*/
		}
		activeRoomService.AsyncHotelList(hotelId, htlInfo.getCityCd(), null, null, checkin, checkout, customerId, Source, results, srcAgentType);

		logger.debug("### getHotelRooms END {} ### ({}ms)", hotelId, (System.currentTimeMillis() - startTime));
		return results;
	}

	/**
	 * 取得飯店資訊、景點、照片、公告(單一商品頁,上半部)
	 * @param hotelId 飯店代碼
	 * @return <T> HotelStandalone
	 */
	public <T> HotelStandalone getHotelInfo(final String hotelId) {
		long startTime = System.currentTimeMillis();
		logger.debug("### getHotelInfo START {} ###", hotelId);
		HotelStandalone hotel = new HotelStandalone();
		String cacheKey = cacheService.getHotelKey(Cache.ApiType.WEB, hotel.getClass().getSimpleName(), hotelId);
		CacheWrapper<?> cacheObj = cacheService.getCache(cacheKey, 86400);
		if (cacheObj != null) {
			hotel = (HotelStandalone) cacheObj.getCacheObject();
			int timeLeft = (int) (86400 - (System.currentTimeMillis() - cacheObj.getSettleTime()) / 1000);
			cacheService.setCache(cacheKey, cacheObj, timeLeft);
			logger.debug("### getHotelInfo END {} - cache get ### ({}ms)", hotelId, System.currentTimeMillis() - startTime);
		} else {
			hotel = getHotelStandalone(hotelId);
			if (hotel != null) {
				if (cacheFlag.toLowerCase().equals("true")) {
					String reCacheKey = cacheService.getHotelKey(Cache.ApiType.WEB, hotel.getClass().getSimpleName(), hotelId);
					CacheWrapper<?> reCacheObj = new CacheWrapper<HotelStandalone>(hotel, System.currentTimeMillis());
					cacheService.setCache(reCacheKey, reCacheObj, 86400);
				}
			}
			logger.debug("### getHotelInfo END {} ### (" + (System.currentTimeMillis() - startTime) + "ms)", hotelId);
		}
		// 瀏覽統計記錄
		new Thread() {
			private String hotelId;
			public Thread init(String hotelId) {
				this.hotelId = hotelId;
				return this;
			}
			public void run() {
				redisService.execBrowseRecord(hotelId);
			}
		}.init(hotelId).start();
		return hotel;
	}

	/**
	 * 取得飯店資訊、景點、照片、公告
	 * @param hotelId 飯店代碼
	 * @return <T> HotelStandalone
	 */
	private <T> HotelStandalone getHotelStandalone(final String hotelId) {
		logger.debug("### getHotelStandalone START ###");
		long startTime = System.currentTimeMillis();

		// 執行序物件宣告
		SearchThread photoJob = new SearchThread(new SearchPhoto(searchRepository), hotelId); // 飯店照片url
		SearchThread hotelDetail = new SearchThread(new SearchHotelDetail(searchRepository), hotelId); // 飯店資訊
		SearchThread facilityJob = new SearchThread(new SearchFacility(facilityService), hotelId); // 飯店設施、服務
		SearchThread viewsJob = new SearchThread(new SearchViews(searchRepository), hotelId); // 飯店景點

		// 執行序線程執行
		ExecutorService jobPool = ThreadPoolCenter.getIoPool();
		Map<String, Future<T>> results = new HashMap<>();
		results.put("photo", jobPool.submit(photoJob));
		results.put("detail", jobPool.submit(hotelDetail));
		results.put("facility", jobPool.submit(facilityJob));
		results.put("view", jobPool.submit(viewsJob));

		// 所需資訊取得集合
		HotelStandalone hotel = null; // 回傳物件
		List<String> photo = null;
		Map<String, List<String>> facilites = null;
		List<CodeDetail> view = null;
		try {
			hotel = (HotelStandalone) results.get("detail").get();
			photo = (List<String>) results.get("photo").get();
			facilites = (Map<String, List<String>>) results.get("facility").get();
			view = (List<CodeDetail>) results.get("view").get();
			this.usedEquipIds = UsedEquipsId.getUsedEquipsId(searchRepository);
		} catch (Exception e) {
			logger.error("getHotelStandalone Get Results Fail !!!");
			e.printStackTrace();
		}

		if (hotel != null) {
			// 飯店照片
			hotel.setPhotoURLs(photo);

			// 飯店設備代碼
			List<String> equips = new ArrayList<String>();
			equips = erpdb11gSearchRepository.getHtlsEquip(new ArrayList<String>(1) {
				{
					add(hotelId);
				}
			});
			if (equips != null && equips.size() > 0) {
				hotel.setHotelEquipmenList(equipementConverter(equips.get(0)));
			}

			// 飯店設施、服務
			if (facilites != null) {
				HotelFacility facility = new HotelFacility();
				facility.setGeneral(facilites.get("services"));
				facility.setServices(facilites.get("hardware"));
				facility.setActivities(facilites.get("activites"));
				facility.setRoomAmenities(facilites.get("roomAmenities"));
				hotel.setHotelFacility(facility);
			}

			// 飯店景點
			if (view != null && view.size() > 0) {
				hotel.setView(view);
			}

			// 飯店公告截止日判斷
			if (hotel.getHotelAd() == null || StringUtils.isBlank(hotel.getHotelAd())) {
				hotel.setHotelAd(null);
			} else if (hotel.getHotelAdCloseDt() != null) {
				if (StringUtils.isNotBlank(hotel.getHotelAdCloseDt())) {
					String nowDt = new SimpleDateFormat("yyyyMMdd").format(new Date());
					try {
						if (Integer.parseInt(nowDt) > Integer.parseInt(hotel.getHotelAdCloseDt()))
							hotel.setHotelAd(null);
					} catch (NumberFormatException ex) {
						logger.error("parseNumFormat Error, HotelAdCloseDt : " + hotel.getHotelAdCloseDt());
					} catch (Exception e) {
						logger.error("parseNumFormat Exception, HotelAdCloseDt : " + hotel.getHotelAdCloseDt());
						e.printStackTrace();
					}
				}
			}
		}
		logger.debug("### getHotelStandalone END ### (" + (System.currentTimeMillis() - startTime) + "ms)");
		return hotel;
	}

	/**
	 * 設備代碼啟用驗證 transfer binary string to list that contain hotel's all equipments
	 * @param equipCode
	 * @return List<String>
	 */
	private List<String> equipementConverter(String equipCode) {
		List<String> equps = new ArrayList<>(this.usedEquipIds.size());
		for (Integer equipId : this.usedEquipIds) {
			// 設備索引是否超出設定長度
			if (equipId > equipCode.length()) {
				break;
			}
			// 驗證該設備索引是否為啟用
			if (equipCode.charAt(equipId.intValue() - 1) == '1') {
				equps.add(equipId.toString());
			}
		}
		return equps;
	}

	/**
	 * 計算產品頁該飯店所有房型的平均售價
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkIn
	 * @param checkOut
	 * @param agentType
	 * @return
	 */
	private List<SearchMinsRoom> caculatehtlRoomsSalesPrice(String hotelId, String roomId, int roomQty, String checkIn, String checkOut, String agentType) {
		logger.debug("###caculatehtlRoomsSalesPrice Start {} ### ");
		long startTime = System.currentTimeMillis();
		List<SearchMinsRoom> allRooms = new ArrayList<SearchMinsRoom>();
		try {
			List<SearchMinsRoom> allRoomsExtends = erpdb11gSearchRepository.getHotelRoomsInfoExtend(hotelId, roomId, roomQty, checkIn, checkOut, "TW", agentType);
			List<String> hotelIds = new ArrayList<String>();
			hotelIds.add(hotelId);
			List<HtlMinPriceExtend> htlRoomsPriceAvgList = erpdb11gHotelSearchRepository.getHotelsMinPrice(hotelIds, roomQty, checkIn, checkOut, null, null, nationality, agentType, "PROD");
			List<List<HtlMinPriceExtend>> htlRoomsPriceAvgExtend = new ArrayList<>(hotelIds.size());
			htlRoomsPriceAvgExtend.add(htlRoomsPriceAvgList);
			List<List<String>> hotelIdsExtend = new ArrayList<List<String>>();
			hotelIdsExtend.add(hotelIds);

			List<List<HtlMinPrice>> htlRoomsPriceAvgFinal = new ArrayList<>(hotelIds.size());
			htlRoomsPriceAvgFinal = roomCommonService.caculatHotelLowPriceList(htlRoomsPriceAvgExtend, agentType, checkIn, checkOut, hotelIdsExtend, "all");
			for (List<HtlMinPrice> partOfHtlRoomsPrices : htlRoomsPriceAvgFinal) {
				for (final HtlMinPrice RoomsPrices : partOfHtlRoomsPrices) {
					for (SearchMinsRoom allRoomsExtend : allRoomsExtends) {
						if (allRoomsExtend.getRoomId().equals(RoomsPrices.getRoomId())) {
							allRoomsExtend.setPriceAvg(RoomsPrices.getAvgSitePrice());
							allRoomsExtend.setSalePriceAvg(RoomsPrices.getAvgSalePrice());
							if (agentType.substring(0, 1).equals("M") || agentType.substring(0, 2).equals("E2")) {
								allRoomsExtend.setMobileSalePriceAvg(RoomsPrices.getAvgSalePrice());
							}
							allRooms.add(allRoomsExtend);

						}
					}
				}
			}

			// 依照房型價錢排序
			Collections.sort(allRooms, new Comparator<SearchMinsRoom>() {
				public int compare(SearchMinsRoom o1, SearchMinsRoom o2) {
					Integer price1 = o1.getSalePriceAvg();
					Integer price2 = o2.getSalePriceAvg();
					return price1.compareTo(price2);
				}
			});
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.debug("###caculatehtlRoomsSalesPrice END {} ### (" + (System.currentTimeMillis() - startTime) + "ms)");
		return allRooms;
	}
	
	/**
	 * 取得基本房數量
	 * @param allRooms
	 * @param hotelId
	 * @param checkIn
	 * @param checkOut
	 * @return
	 */
	private List<SearchMinsRoom> getMasterMinRoomQty(List<SearchMinsRoom> allRooms, String hotelId, String checkIn, String checkOut) {
		Map<String, Integer> masterQty = new HashMap<String, Integer>();
		for (SearchMinsRoom room : allRooms) {
			if (StringUtils.isNotBlank(room.getMasterId()) && !masterQty.containsKey(room.getMasterId())) {// 有基本房,且還沒查詢過
				String minMasterQty = roomQtyEdbRepository.getMinMasterPq(hotelId, room.getMasterId(), checkIn, checkOut);// 有開房的基本房,其最低數量
				if (StringUtils.isNotBlank(minMasterQty))
					masterQty.put(room.getMasterId(), Integer.parseInt(minMasterQty));
			}
		}

		SearchMinsRoom room;
		for (int i = 0; i < allRooms.size(); i++) {
			room = allRooms.get(i);
			if (!"N".equals(room.getCommonQtyYN())) {// 可數量共用
				if (StringUtils.isNotBlank(room.getMasterId())) {
					if (masterQty.containsKey(room.getMasterId())) {// 該基本房ID包含在MAP裡面
						room.setMasterIdQty(masterQty.get(room.getMasterId()));
					} else {
						allRooms.remove(i); // 2017-08-10 有關聯基本房但卻取得不到基本房數量暫時停賣
						i--;
					}
				}
			} else {
				room.setMasterIdQty(0);
			}
		}
		return allRooms;
	}
	
	/**
	 * 2015/08/24 房型列表與網路描述代碼SQL排序方式不同修正
	 * @param mappingTarget
	 * @param sortObj
	 * @return List<InternetStatus>
	 */
	private List<InternetStatus> mappingSortByRoomList(List<SearchMinsRoom> mappingTarget, List<InternetStatus> sortObj) {
		List<InternetStatus> result = new ArrayList<InternetStatus>();
		for (SearchMinsRoom room : mappingTarget) {
			for (int i = 0; i < sortObj.size(); i++) {
				if (StringUtils.equals(room.getRoomId(), sortObj.get(i).getRoomtypeNo())) {
					result.add(sortObj.get(i));
					break;
				}
			}
		}
		return result;
	}
	
	/**
	 * 網路描述代碼轉譯入口
	 * @param internetStatus 上網支援房況
	 * @param internetFee 上網收費房況
	 * @return
	 */
	private String translateInternetType(String internetStatus, String internetFee) {
		if (internetStatus.contains("A")) { // 有線網路/WIFI
			return translateAllInternet(internetStatus, internetFee);
		} else if (internetStatus.contains("L") || internetStatus.contains("W")) { // 有線網路 or WIFI
			return translatePartInternet(internetStatus, internetFee);
		} else if (internetStatus.contains("N")) { // 無提供網路服務
			return "無提供網路服務";
		}
		// 無設定
		return null;
	}
	
	/**
	 * 網路描述轉譯 for A
	 * @param internetStatus 上網支援房況
	 * @param internetFee 上網收費房況
	 * @return
	 */
	private String translateAllInternet(String internetStatus, String internetFee) {
		StringBuilder internet = new StringBuilder("有線網路/WIFI：");
		switch (internetStatus) {
			case "A01":
				internet.append("部分房型無提供網路");
				break;
			case "A02":
				internet.append("所有房型皆提供WIFI上網，部分房型提供有線上網");
				break;
			case "A03":
				internet.append("所有房型皆提供有線上網，部分房型提供WIFI上網");
				break;
			case "A04":
				internet.append("所有房型皆提供網路");
				break;
		}
		if (internetFee.equals("A01") || internetFee.equals("A02") || internetFee.equals("A03")) {
			internet.append("；依飯店規定於飯店櫃檯收費");
		}
		return internet.toString();
	}
	
	/**
	 * 網路描述轉譯 for L or W
	 * @param internetStatus 上網支援房況
	 * @param internetFee 上網收費房況
	 * @return
	 */
	private String translatePartInternet(String internetStatus, String internetFee) {
		StringBuilder internet = new StringBuilder();
		if (internetStatus.contains("L")) {
			internet.append("有線網路：");
		} else if (internetStatus.contains("W")) {
			internet.append("WIFI：");
		}
		if (internetStatus.contains("01")) {
			internet.append("部分房型無提供網路");
		} else if (internetStatus.contains("02")) {
			internet.append("所有房型皆提供網路");
		}
		if (internetFee.contains("01")) {
			internet.append("；依飯店規定於飯店櫃檯收費");
		}
		return internet.toString();
	}
}
