package eztravel.core.service.hotel.extend;

import eztravel.core.service.hotel.FacilityService;

/**
 * Created by jimin on 6/9/15.
 */
public class SearchFacility implements SearchHotelInfo {

	private FacilityService facilityService;
	
	public SearchFacility(FacilityService facilityService) {
		super();
		this.facilityService = facilityService;
	}

	public <T> T getInfo(String hotelId) {
		return (T) facilityService.listHotelFacilites(hotelId);
	}
}
