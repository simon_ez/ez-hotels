package eztravel.core.service.hotel;

import java.util.List;

import eztravel.rest.pojo.hotel.Range;
import eztravel.rest.pojo.hotel.Room;

public interface SearchService {
	
	/**
	 * 取得該飯店所有可售房型列表資料及價錢(產品頁下半部).
	 * 1. WEB頁面上的價錢是看agentType(customerId所對應到的身份)來算出價錢
	 * 2  APP頁面上的web價錢,因為APP目前傳入的customerId都是A01,所以APP的web價錢都是A01 
	 * 3. APP的售價是看mobileAgentType :當日則為M02,預設為M01 
	 * 4.使用cache如果撈不到才撈db 
	 * 5.會更新該飯店所屬city列表的資料
	 * 
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param priceRanges
	 * @param customerId
	 * @param source
	 * @param version
	 * @return the hotel rooms
	 * @throws Exception
	 */
	List<Room> getHotelRooms(String hotelId, String roomId, int roomQty, String checkin, String checkout, List<Range> priceRanges, String customerId, String source, String version) throws Exception;
}
