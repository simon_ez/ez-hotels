/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: ActiveRoomServiceImpl.java
 * @author:   cano0530
 * @date:     2017/3/9, 下午 02:21:15
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import eztravel.core.enums.Cache;
import eztravel.core.enums.Cache.ApiType;
import eztravel.core.pojo.HtlMinPrice;
import eztravel.rest.pojo.hotel.CacheWrapper;
import eztravel.rest.pojo.hotel.Room;

/**
 * The Class ActiveRoomServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
@EnableAsync
public class ActiveRoomServiceImpl implements ActiveRoomService {

	/**
	 * The Constant logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(ActiveRoomService.class);

	/** The pre cache service. */
	@Autowired
	private PreCacheService preCacheService;

	/** The cache service. */
	@Autowired
	CacheService cacheService;

	/** cache hotelList expireTime. */
	@Value("${cache.hotelList.expireTime}")
	private int expireTime;

	/** async flag. */
	@Value("${async.flag}")
	private String asyncflag;

	/**
	 * 更新寫入cache.
	 * 
	 * @param hotelId
	 * @param cityCd
	 * @param travelType
	 * @param roomTypes
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 * @param roomList
	 * @param srcAgentType
	 * 
	 *  1. 只更新產品頁所過來的城市,入住日,退房日還有身份的cache資料
	 * 
	 *            String cacheKey =
	 *            cacheService.getHtlMinPriceKey(Cache.ApiType.WEB, "HtlMinPriceList", cityCd, travelType, checkin, checkout, agentType ,roomTypeStr);
	 */
	@Override
	public void AsyncHotelList(String hotelId, String cityCd, String travelType, List<Integer> roomTypes, String checkin, String checkout, String customerId, String source, List<Room> roomList, String srcAgentType) {
		logger.debug("updateHotelListCache  hotelId={} ,cityCd={} ,travelType={} ,roomTypes={},checkin={} ,checkout={}  , customerId={} , source={} 開始", hotelId, cityCd, travelType, roomTypes, checkin, checkout, customerId, source);
		long s = System.currentTimeMillis();
		boolean updateCacheFlags = false;// 是否需要更新cache
		boolean isdiffer = false;// 是否價錢數量有變動
		boolean hasHtlMinPriceListCache = false;// 是否有對應飯店清單價格 cache
		try {
			if (asyncflag.equals("TRUE")) {
				// 如果沒有城市,且撈不出城市則不做更新cache的動作
				if (cityCd != null) {
					ApiType apiType = Cache.ApiType.WEB;
					if ("mweb".equals(source)) {
						apiType = Cache.ApiType.MWEB;
					}
					String startStr = apiType + ":HtlMinPriceList:" + cityCd;
					// 將所有關鍵的cache key撈出來
					Set<String> cacheKeys = cacheService.getResultsCacheKeyByStartWith(startStr);
					if (cacheKeys.size() > 0) {
						for (String key : cacheKeys) {
							updateCacheFlags = false;
							isdiffer = false;
							String[] parmeter = key.split(":");
							// 組合cache key
							travelType = parmeter[3];
							String roomTypesStr = parmeter[4];
							// List<Integer> parmaRoomTypes = new
							// ArrayList<Integer>();//cache裡面roomtype key
							// 過濾除了0,1,2,3,4之外的資料,
							List<Integer> cacheRoomTypes = new ArrayList<Integer>();// cache裡面roomtype
																					// key的原始資料
							if (!roomTypesStr.equals("null")) {
								List<Integer> temp = new ArrayList<Integer>();
								List<Integer> cachetemp = new ArrayList<Integer>();
								String[] roomTypStr = roomTypesStr.split(",");
								for (String str : roomTypStr) {
									if (str.equals("0") || str.equals("1") || str.equals("2") || str.equals("3") || str.equals("4")) {
										temp.add(Integer.parseInt(str));
									}
									cachetemp.add(Integer.parseInt(str));
								}
								// parmaRoomTypes =temp;
								cacheRoomTypes = cachetemp;
							} else {
								// parmaRoomTypes=null;
								cacheRoomTypes = null;
							}
							String cacheCheckIn = parmeter[5];
							String cacheCheckout = parmeter[6];
							String agentType = parmeter[7];// 身份 ：A01 ,M01,M02
															// ,E01,..
							if (agentType.substring(0, 1).equals("M")) {// 取agentType的第一個位元
								source = "mobile";// 如果是M開頭,來源就是mobile
							} else if (agentType.substring(0, 2).equals("E2")) {
								source = "mobile";// 如果是E2開頭,來源就是mobile
							} else if ("mweb".equals(source)) {
								// 不做事，確保 source 不變
							} else {
								source = "";
							}
							// 過濾不必要更新的cache key
							if (cacheCheckIn.equals(checkin) && cacheCheckout.equals(checkout) && srcAgentType.equals(agentType)) {// 如果沒有對應的入住退房入,及身份
								if (roomTypes != null && roomTypes.size() > 0) {// 沒有對應的房型
									if (cacheRoomTypes.equals(roomTypes)) {
										updateCacheFlags = true;
									}
								} else {
									updateCacheFlags = true;
								}
							}
							if (updateCacheFlags) {

								if (roomList != null && roomList.size() > 0) {// 有房可賣時
									int srcSalePriceAvg = 0;
									// 如果是用app查,有更動,只更動MobileSalePriceAvg
									if (source.equals("mobile")) {
										srcSalePriceAvg = roomList.get(0).getMobileSalePriceAvg();
									} else {
										srcSalePriceAvg = roomList.get(0).getSalePriceAvg();
									}
									int srcPriceAvg = roomList.get(0).getPriceAvg();
									int srcRoomQty = roomList.get(0).getRoomQty();
									// 把cachekey的所有飯店資料從redis撈出來
									List<List<HtlMinPrice>> HtlMinPriceListCache = this.getCacheHtlminsPriceListData(key);
									if (HtlMinPriceListCache != null && HtlMinPriceListCache.size() > 0) {
										// 更新cache包中有異動的飯店
										for (List<HtlMinPrice> partOfHtlMinPrices : HtlMinPriceListCache) {
											for (HtlMinPrice htlMinPrice : partOfHtlMinPrices) {
												if (htlMinPrice.getHotelId().equals(hotelId)) {
													if (srcRoomQty > 0) {// 可以訂房
														// 飯店狀態可訂房
														if (htlMinPrice.getAvgSalePrice() != srcSalePriceAvg) {
															// BeanUtils.copyProperties(hotelsearch,
															// hotelStaticDB);
															htlMinPrice.setAvgSalePrice(srcSalePriceAvg);
															htlMinPrice.setAvgSitePrice(srcPriceAvg);
															htlMinPrice.setQty(srcRoomQty);
															isdiffer = true;
															hasHtlMinPriceListCache = true;
															// 飯店狀態滿房或候補->可訂房
														} else if (htlMinPrice.getQty() == 0) {
															htlMinPrice.setAvgSalePrice(srcSalePriceAvg);
															htlMinPrice.setAvgSitePrice(srcPriceAvg);
															htlMinPrice.setQty(srcRoomQty);
															isdiffer = true;
															hasHtlMinPriceListCache = true;
														}
													} else { // 可以訂房->候補
														htlMinPrice.setAvgSalePrice(srcSalePriceAvg);
														htlMinPrice.setAvgSitePrice(srcPriceAvg);
														htlMinPrice.setQty(srcRoomQty);
														isdiffer = true;
														hasHtlMinPriceListCache = true;
													}
													break;
												}
											}
										}
										// 沒有對應的飯店 HtlMinPrice 則產生新的
										// HtlMinPriceListCache
										if (!hasHtlMinPriceListCache) {
											List<HtlMinPrice> tempHtlMinPrices = new ArrayList<HtlMinPrice>();
											HtlMinPrice tempHtlMinPrice = new HtlMinPrice();
											tempHtlMinPrice.setHotelId(hotelId);
											tempHtlMinPrice.setRoomId(roomList.get(0).getRoomId());
											tempHtlMinPrice.setQty(srcRoomQty);
											tempHtlMinPrice.setAvgSalePrice(srcSalePriceAvg);
											tempHtlMinPrice.setAvgSitePrice(srcPriceAvg);
											tempHtlMinPrice.setCheckIn(roomList.get(0).getRoomCheckinTime());
											if (roomList.get(0).getPromoInfo() != null && !roomList.get(0).getPromoInfo().isEmpty()) {
												tempHtlMinPrice.setPromotionDay(roomList.get(0).getPromoInfo().get(0).getPromoDay());
												tempHtlMinPrice.setPromotionRate(roomList.get(0).getPromoInfo().get(0).getPromoRate());
												tempHtlMinPrice.setPromotionType(roomList.get(0).getPromoInfo().get(0).getPromoType());
											}
											tempHtlMinPrice.setRoomProjectName(roomList.get(0).getRoomProjectName());
											tempHtlMinPrice.setRoomSellPoints(roomList.get(0).getSellPoints());
											tempHtlMinPrices.add(tempHtlMinPrice);
											HtlMinPriceListCache.add(tempHtlMinPrices);
											isdiffer = true;
										}

										// 重新寫入到redis中
										if (isdiffer) {
											preCacheService.setCacheHtlMinPrice(HtlMinPriceListCache, cityCd, travelType, checkin, checkout, cacheRoomTypes, customerId, source, agentType);
										}
									}
								} else {// 沒有房可賣 可訂房->??????
									// 把cachekey的所有飯店資料從redis撈出來
									List<List<HtlMinPrice>> HtlMinPriceListCache = this.getCacheHtlminsPriceListData(key);
									if (HtlMinPriceListCache != null && HtlMinPriceListCache.size() > 0) {
										// 更新cache包中有異動的飯店
										for (List<HtlMinPrice> partOfHtlMinPrices : HtlMinPriceListCache) {
											for (HtlMinPrice htlMinPrice : partOfHtlMinPrices) {
												if (htlMinPrice.getHotelId().equals(hotelId)) {
													partOfHtlMinPrices.remove(htlMinPrice);
													isdiffer = true;
													break;
												}
											}
										}
										// 重新寫入到redis中
										if (isdiffer) {
											preCacheService.setCacheHtlMinPrice(HtlMinPriceListCache, cityCd, travelType, checkin, checkout, cacheRoomTypes, customerId, source, agentType);
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.debug("updateHotelListCache 結束 {} ms", System.currentTimeMillis() - s);
	}

	/**
	 * 取得cache裡面的HtlMinPrice list Data
	 * 
	 * @param cacheKey
	 * @return
	 */
	private List<List<HtlMinPrice>> getCacheHtlminsPriceListData(String cacheKey) {
		long startTime = System.currentTimeMillis();
		List<List<HtlMinPrice>> result = new ArrayList<List<HtlMinPrice>>();
		try {
			CacheWrapper<?> cacheObj = cacheService.getCache(cacheKey, expireTime);
			if (cacheObj != null) {
				String cacheObjResource = (String) cacheObj.getCacheObject();
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				List<List<HtlMinPrice>> resource = mapper.readValue(cacheObjResource, new TypeReference<List<List<HtlMinPrice>>>() {	});
				logger.debug(" - - ### getCacheHtlminsPriceListData END - cache get ### ({}ms)", System.currentTimeMillis() - startTime);
				resource.size();
				logger.debug("HtlminsPriceListData size =" + resource.size());
				if (resource.size() > 0) {
					return resource;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

}
