package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.persistence.repository.oracle.OrderRepository;
import eztravel.persistence.repository.oracle.SearchRepository;
import eztravel.util.Redis;

public class RedisServiceImpl implements RedisService {
	private static final Logger logger = LoggerFactory.getLogger(RedisServiceImpl.class);
	private static final Redis redis = new Redis(7);
	
	@Autowired
	private SearchRepository searchRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	/**
	 * 瀏覽商品數量統計
	 * @param city
	 * @param view
	 * @param hotelNo
	 */
	public void execBrowseRecord(String hotelNo){
		try{
			String key = this.getStatisticalRecordKey(null, hotelNo, null, null).get(0);
			//記錄本次瀏覽飯店記錄
			this.saveHistoryRecord(key, RecordType.BROWSE);
			//刷新瀏覽統計記錄
			this.updateStatisticalRecord(key.split("-")[0], RecordType.BROWSE);
		}catch(Exception e){
			logger.error("execBrowseRecord Error, HotelNo: " + hotelNo);
			logger.error(e.toString());
			e.printStackTrace();
		}
	}
	
	/**
	 * 反推統計記錄的鍵值 (反推出 City + View + HotelId 組鍵值)
	 * @param orderNo
	 * @param hotelId
	 * @param city
	 * @param view
	 * @param recordType
	 * @return List<String>
	 * @throws Exception
	 */
	private List<String> getStatisticalRecordKey(String orderNo, String hotelId, String city, String view) throws Exception{
		List<String> keys = null;
		try{
			if(StringUtils.isNotBlank(orderNo)){
				keys = new ArrayList<String>();
				List<Map<String, Object>> list = orderRepository.getHotelInfoByOrderNoOrProdNo(orderNo, null);
				keys.add(list.get(0).get("CITYCD") + "-" + list.get(0).get("VIEWCD") + "-" + list.get(0).get("PRODNO"));
			}else if(StringUtils.isNotBlank(hotelId)){
				String cacheKey = "StatisticalRecordKey:HotelId:" + hotelId;
				keys = (List<String>)redis.get(cacheKey);
				if(keys == null || keys.isEmpty()){
					keys = new ArrayList<String>();
					List<Map<String, Object>> list = orderRepository.getHotelInfoByOrderNoOrProdNo(null, hotelId);
					keys.add(list.get(0).get("CITYCD") + "-" + list.get(0).get("VIEWCD") + "-" + list.get(0).get("PRODNO"));
					redis.set(cacheKey, keys);
					redis.expire(cacheKey, 60*60*48);
				}
				
			}else if(StringUtils.isNotBlank(city) || StringUtils.isNotBlank(view)){
				String cacheKey = "StatisticalRecordKey:CityView:" + (StringUtils.isBlank(city)?"":city) + (StringUtils.isBlank(view)?"":view);
				keys = (List<String>)redis.get(cacheKey);
				if(keys == null || keys.isEmpty()){
					keys = new ArrayList<String>();
					for(Map<String, Object> hotel: searchRepository.getHotelIdByCityView(city, view))
						keys.add(hotel.get("CITYCD") + "-" + hotel.get("VIEWCD") + "-" + hotel.get("PRODNO"));
					redis.set(cacheKey, keys);
					redis.expire(cacheKey, 60*60*48);
				}
			}
		}catch(Exception e){
			StringBuilder message = new StringBuilder();
			message.append("反查飯店所在城市、景點異常");
			if(StringUtils.isNotBlank(orderNo))
				message.append("，OrderNo : " + orderNo);
			if(StringUtils.isNotBlank(hotelId))
				message.append("，HotelId : " + hotelId);
			if(StringUtils.isNotBlank(city))
				message.append("，City : " + city);
			if(StringUtils.isNotBlank(view))
				message.append("，View : " + view);
			logger.error(message.toString());
			logger.error(e.toString());
			throw new Exception(message.toString());
		}
		return keys;
	}
	
	/**
	 * 記錄指定Key、類別記錄
	 * @param key
	 * @param recordType
	 */
	private void saveHistoryRecord(String key, RecordType recordType) throws Exception {
		Map<String, List<Object>> record = (Map<String, List<Object>>)redis.get(recordType.getPath() + key);
		List<Object> objectList = null;
		if(record == null){
			record = new HashMap<String, List<Object>>();
			objectList = new ArrayList<Object>();
		}else{
			objectList = record.get(recordType.getContentKey());
			if(objectList == null)
				objectList = new ArrayList<Object>();
		}
		objectList.add(new Date().getTime());
		record.put(recordType.getContentKey(), objectList);
		redis.set(recordType.getPath() + key, record);
	}
	
	/**
	 * 更新統計記錄
	 * @param city
	 * @param view
	 * @param RecordType 類別設定
	 */
	public void updateStatisticalRecord(String city, RecordType recordType) throws Exception {
		Set<String> keys = redis.getKeys(recordType.getPath() + city + "*");
		//由城市取得所有景點鍵值
		List<String> viewKeys = new ArrayList<String>();
		for(String cityKey: keys){
			String viewKey = cityKey.replace(cityKey.split("-")[2], "");
			if(!viewKeys.contains(viewKey))
				viewKeys.add(viewKey);
		}
		
		//取得更新時效性
		Calendar overTime = Calendar.getInstance();
		overTime.add(Calendar.SECOND, -recordType.getOverTime());
		long timeLimit = overTime.getTimeInMillis();
		
		//取得景點下的飯店數量
		for(String viewKey: viewKeys){
			keys = redis.getKeys(viewKey + "*");
			
			List<StatisticalHtl> keepRecord = new ArrayList<StatisticalHtl>();
			for(String key: keys){
				try{
					Map<String, List<Object>> record = (Map<String, List<Object>>)redis.get(key);
					List<Object> objectList = record.get(recordType.getContentKey());
					if(objectList != null){
						
						//保留刷新前的數量記錄，提供萬一最後所有逾時資料被清空時可保留最低限度的資料
						StatisticalHtl htlRecord = new StatisticalHtl();
						htlRecord.setKey(key);
						htlRecord.setCount(objectList.size());
						keepRecord.add(htlRecord);
						
						//檢查時間記錄
						for(int s = 0; s < objectList.size(); s++){
							long recordTime = (long)objectList.get(s);
							//超過時效記錄移除
							if(recordTime < timeLimit){
								objectList.remove(s);
								s--;
							}
						}
						
						//若已經沒任何可參考時效資料時，從redis中移除
						if(objectList == null || objectList.size() == 0){
							redis.expire(key, 0);
						}else{
							record.put(recordType.getContentKey(), objectList);
							redis.set(key, record);
						}
					}
				}catch(Exception e){
					logger.error("updateStatisticalRecord Error: " + key);
					//redis.expire(key, 0);
				}
			}
		}
		//記錄最後更新統計時間
		redis.set(RecordType.TIMERECORD.getPath() + recordType.getContentKey() + ":" + city, new Date());
	}

}
