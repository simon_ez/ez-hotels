/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.hotel
 * @FileName: FacilityServiceImpl.java
 * @author:   Kent
 * @date:     2015/5/5, 下午 05:59:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.pojo.HotelFacility;
import eztravel.persistence.repository.oracle11g.Erpdb11gFacilityRepository;

/**
 * <pre>
 * FacilityServiceImpl, 飯店設施.
 * </pre>
 *
 * @author Kent
 */
public class FacilityServiceImpl implements FacilityService {

	/** The erpdb11g facility repository. */
	@Autowired
	private Erpdb11gFacilityRepository erpdb11gFacilityRepository;

	@Override
	public Map<String, List<String>> listHotelFacilites(String hotelId) {
		Map<String, List<String>> results = new HashMap<String, List<String>>();
		List<HotelFacility> facilites = erpdb11gFacilityRepository.listHotelFacilites(hotelId);
		if (facilites != null) {
			List<String> hardware = new ArrayList<String>(); // 硬體設施
			List<String> services = new ArrayList<String>(); // 服務項目
			List<String> activites = new ArrayList<String>(); // 活動設施
			List<String> roomAmenities = new ArrayList<String>(); // 客房設施和服務

			for (HotelFacility facility : facilites) {
				String itemCd = facility.getItemCd();
				String itemValue = facility.getItemValue();
				String groupId = itemCd.substring(0, 1);
				switch (groupId) {
					case "P":
						if (itemValue.equals("Y"))
							hardware.add(facility.getName());
						break;
						case "R":
						if (itemValue.equals("Y"))
							hardware.add(facility.getName());
						break;
						case "S":
						if (!itemValue.equals("C")) {
							String v = extractType2(facility);
							if (StringUtils.isNotBlank(v)) {
								services.add(v);
							}
						}
						break;
					case "F":
						if (itemValue.equals("Y")) {
							String v = extractType1(facility);
							if (StringUtils.isNotBlank(v)) {
								services.add(v);
							}
						}
						break;
					case "C":
						if (itemValue.equals("Y")) {
							String v = extractType1(facility);
							if (StringUtils.isNotBlank(v)) {
								services.add(v);
							}
						}
						break;
					case "E":
						if (itemValue.equals("Y")) {
							String v = extractType1(facility);
							if (StringUtils.isNotBlank(v)) {
								activites.add(v);
							}
						}
						break;
					case "A":
						if (itemValue.equals("Y"))
							roomAmenities.add(facility.getName());
						break;
					case "O":
						if (!itemValue.equals("C")) {
							String v = extractType2(facility);
							if (StringUtils.isNotBlank(v)) {
								roomAmenities.add(v);
							}
						}
						break;
					default:
						break;
				}
			}
			results.put("hardware", hardware);
			results.put("services", services);
			results.put("activites", activites);
			results.put("roomAmenities", roomAmenities);
		}
		return results;
	}

	private String extractType1(HotelFacility facility) {
		StringBuilder value = new StringBuilder(facility.getName());
		String groupId = facility.getItemCd().substring(0, 1);
		boolean creditCard = "C16".equals(facility.getItemCd()) ? true : false; // [可使用信用卡]不加上(需付費)
		if (!creditCard && (groupId.equals("C") || (groupId.equals("E") && facility.getData3().equals("Y")))) {
			value.append("(需付費)");
		}
		if (("F05F06F07F08C05C06C07C08").indexOf(facility.getItemCd()) != -1) {
			if (StringUtils.isNotBlank(facility.getData1()))
				value.append(":").append(facility.getData1());
		}
		return value.toString();
	}

	private String extractType2(HotelFacility facility) {
		String description = facility.getDescription();
		String[] descs = description.split(";");
		String itemValueDesc = descs[0];
		String[] itemValueDescs = itemValueDesc.split(" ");
		String itemValue = itemValueDescs[0];
		String itemName = itemValueDescs[1];
		itemName = itemName.substring(1, itemName.length() - 1);
		String itemCd = facility.getItemCd();
		if (itemCd.equals("O02")) {
			// A/B/C ((全部房間熨斗/熨衣板)/(熨斗/熨衣板(服務台取))/無)
			switch (facility.getItemValue()) {
				case "A":
					return "全部房間熨斗/熨衣板";
				case "B":
					return "熨斗/熨衣板(服務台取)";
				default:
					break;
			}
		} else if (itemCd.equals("O18")) {
			// A/B/C ((床具:鴨絨被)/(床具:毛毯或被子)/無)
			switch (facility.getItemValue()) {
				case "A":
					return "床具:鴨絨被";
	
				case "B":
					return "床具:毛毯或被子";
				default:
					break;
			}
		} else {
			String[] values = itemValue.split("/");
			String[] names = itemName.split("/");
			for (int i = 0; i < values.length; i++) {
				if (facility.getItemValue().equals(values[i])) {
					return names[i];
				}
			}
		}
		return null;
	}
}
