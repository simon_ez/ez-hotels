/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: CacheServiceImpl.java
 * @author:   002766
 * @date:     2017/11/7, 下午 05:26:50
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import eztravel.core.enums.Cache;
import eztravel.rest.pojo.hotel.CacheWrapper;
import eztravel.util.Redis;

/**
 * Created by jimin on 7/22/15.
 */
public class CacheServiceImpl implements CacheService {

	private static final Logger logger = LoggerFactory.getLogger(CacheServiceImpl.class);
	private static Redis redis = new Redis(7);
	
	/**
	 * Gets the hotel key.
	 */
	public String getHotelKey(Cache.ApiType apiType, String pojo, String hotelId) {
		return getKey(apiType, pojo, new StringBuilder().append("v1").append(":").append(hotelId));
	}

	/**
	 * Gets the key.
	 */
	private String getKey(Cache.ApiType apiType, String pojo, StringBuilder dataType) {
		return new StringBuilder().append(apiType.toString()).append(":").append(pojo).append(":").append(dataType).toString();
	}
	
	/**
	 * Gets the room list key.
	 */
	public String getRoomListKey(Cache.ApiType apiType, String pojo, String hotelId, String roomId, int roomQty, String checkin, String checkout, String agentType) {
		return getKey(apiType, pojo, new StringBuilder().append("v1").append(":").append(hotelId).append(":").append(roomId).append(":").append(roomQty).append(":").append(checkin).append(":").append(checkout).append(":").append(agentType));
	}

	/**
	 * 使用CacheWrapper來得到cache data.
	 * @param cacheKey
	 * @param expireTime
	 * @return the cache
	 */
	public CacheWrapper getCache(String cacheKey, int expireTime) {
		long start = System.currentTimeMillis();
		logger.debug("快取包查詢: " + cacheKey);

		CacheWrapper cacheObj = (CacheWrapper) redis.get(cacheKey);
		if (cacheObj == null) {
			logger.debug("查無此Cache,查詢結束，執行時間: {}ms", System.currentTimeMillis() - start);
			return null;
		} else if (cacheObj.validateCacheTime(System.currentTimeMillis(), expireTime)) {
			logger.debug("Cache過舊,查詢結束，執行時間: {}ms", System.currentTimeMillis() - start);
			return null;
		}
		logger.debug("成功取得Cache,查詢結束，執行時間: {}ms", System.currentTimeMillis() - start);
		return cacheObj;
	}

	/**
	 * 取得CacheWrapper的cache data.
	 * @param key
	 * @param cacheObj
	 * @param expireTime
	 */
	@Async
	public void setCache(String key, CacheWrapper cacheObj, int expireTime) {
		try {
			redis.set(key, cacheObj);
			redis.expire(key, expireTime);
			logger.debug("Cache設立完成,key值: {}", key);
		} catch (Exception e) {
			logger.error("Cache設立失敗, key值: {}, 錯誤訊息: {}", key, e.getMessage());
		}
	}
	
	/**
	 * 取得reids關鍵字key中所有的cache key.
	 * @param startStr
	 * @return the results cache key by start with
	 * @throws Exception
	 */
	@Override
	public Set<String> getResultsCacheKeyByStartWith(String startStr) throws Exception {
		Set<String> list = new HashSet<String>();
		list = redis.getResultsCacheKeyByStartWith(startStr);
		return list;
	}
	
	/**
	 * Gets the hotel mins price key. 取得redis中列表頁最低價及促銷
	 * @param apiType
	 * @param pojo
	 * @param city
	 * @param travelType  null:全部 ,001:飯店 , 002:民宿
	 * @param checkIn
	 * @param checkOut
	 * @param agentType
	 * @param roomType
	 * @return the hotel list key
	 */
	public String getHtlMinPriceKey(Cache.ApiType apiType, String pojo, String city, String travelType, String checkIn, String checkOut, String agentType, String roomType) {
		logger.debug("### getHtlMinPriceKey START ###");
		return getKey(apiType, pojo, new StringBuilder().append(city).append(":").append(travelType).append(":").append(roomType).append(":").append(checkIn).append(":").append(checkOut).append(":").append(agentType));
	}
}
