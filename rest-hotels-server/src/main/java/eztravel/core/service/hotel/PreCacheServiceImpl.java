/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.external
 * @FileName: PreCacheServiceImpl.java
 * @author:   002766
 * @date:     2018/4/30, 上午 11:51:46
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.hotel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import eztravel.core.enums.Cache;
import eztravel.core.enums.Cache.ApiType;
import eztravel.core.pojo.HtlMinPrice;
import eztravel.core.service.CommonService;
import eztravel.persistence.repository.oracle11g.Erpdb11gCustomerSearchRepository;
import eztravel.rest.pojo.hotel.CacheWrapper;
import eztravel.rest.pojo.hotel.Room;
import eztravel.util.UtilDate;

/**
 * The Class PreCacheServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
public class PreCacheServiceImpl extends CommonService implements PreCacheService {
	private static final Logger logger = LoggerFactory.getLogger(PreCacheService.class);
	
	/** cache hotelList expireTime. */
	@Value("${cache.hotelList.expireTime}")
	private int expireTime;
	
	@Autowired
	private CacheService cacheService;
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private Erpdb11gCustomerSearchRepository erpdb11gCustomerSearchRepository;
	
	/**
	 * 從redis取得產品頁下半部的房型列表清單.
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 * @return the cache room list
	 */
	public List<Room> getCacheRoomList(String hotelId, String roomId, int roomQty, String checkin, String checkout, String customerId, String source) {
		long startTime = System.currentTimeMillis();
		List<Room> result = new ArrayList<Room>();
		try {
			// 判斷身份
			String agentType = this.getAgent(customerId, source, checkin, hotelId);
			ApiType apiType = Cache.ApiType.WEB;
			if ("mweb".equals(source)) {
				apiType = Cache.ApiType.MWEB;
			}
			String cacheKey = cacheService.getRoomListKey(apiType, "RoomList", hotelId, roomId, roomQty, checkin, checkout, agentType);
			CacheWrapper<?> cacheObj = cacheService.getCache(cacheKey, 60);
			if (cacheObj != null) {
				String cacheObjResource = (String) cacheObj.getCacheObject();
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				List<Room> resource = mapper.readValue(cacheObjResource, new TypeReference<List<Room>>() { 	});
				logger.debug(" - - ### getCacheRoomList END - cache get ### ({}ms)", System.currentTimeMillis() - startTime);
				resource.size();
				logger.debug("CacheRoomList size =" + resource.size());
				if (resource.size() > 0) {
					return resource;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	/**
	 * 將產品頁下半部的房型列表清單存到redis. cache生命週期一分鐘
	 * @param rooms
	 * @param hotelId
	 * @param roomId
	 * @param roomQty
	 * @param checkin
	 * @param checkout
	 * @param customerId
	 * @param source
	 */
	public void setCacheRoomlList(List<Room> rooms, String hotelId, String roomId, int roomQty, String checkin, String checkout, String customerId, String source) {
		if (cacheFlag.toLowerCase().equals("true")) {
			logger.debug("setCacheRoomlList 開始 hotelId:{},roomId:{}, checkin:{} ,checkout:{} ,roomQty:{} ,customerId:{},source:{}", hotelId, roomId, checkin, checkout, roomQty, customerId, source);
			try {
				// 判斷身份
				String agentType = this.getAgent(customerId, source, checkin, hotelId);
				String str = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).writeValueAsString(rooms);
				if (rooms != null && rooms.size() > 0) {
					ApiType apiType = Cache.ApiType.WEB;
					if ("mweb".equals(source)) {
						apiType = Cache.ApiType.MWEB;
					}
					String cacheKey = cacheService.getRoomListKey(apiType, "RoomList", hotelId, roomId, roomQty, checkin, checkout, agentType);
					CacheWrapper<?> cacheObj = cacheService.getCache(cacheKey, 60);
					if (cacheObj != null) {
						cacheObj = new CacheWrapper<String>(str, System.currentTimeMillis());
						int timeLeft = (int) (60 - (System.currentTimeMillis() - cacheObj.getSettleTime()) / 1000);
						cacheService.setCache(cacheKey, cacheObj, timeLeft);
					} else {
						cacheService.setCache(cacheKey, cacheObj, 60);// cache生命週期一分鐘
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			logger.debug("setCacheRoomlList 結束");
		}
	}

	/**
	   * 判斷身份 若是app 且不是b2e(E2X) ,則需再判斷M01 or M02 若不是 則是 A01 ,B0X ,E0X ,E2X.
	   * @param customerId
	   * @param source
	   * @param checkin
	   * @param hotelId
	   * @return the agent
	   */
	  private String getAgent( String  customerId ,String source ,String checkin ,String hotelId) {
	    String agentType = agentService.getAgentType(customerId, source , checkin);
	    if(source!=null && source.toLowerCase().equals("mobile") && !agentType.substring(0,2).equals("E2"))//因為app無法使用b2b登入,所以暫不判斷
	      agentType = this.getMobileAgentType(checkin, hotelId);
	    return agentType;
	  }
	  
	  /**
		 * 取得app身份為M01 ,M02
		 * @param checkin
		 * @param hotelId 預設為null
		 * @return String
		 */
		private String getMobileAgentType(String checkin, String hotelId) {
			// 當日訂房app使用 M02 AGENT type -6%折扣
			String mobileAgentType = "M01";
			String todayDte = UtilDate.getToday("yyyyMMdd");
			if (checkin.equals(todayDte)) {
				if (hotelId == null) {
					hotelId = "";
				}
				String agentType = erpdb11gCustomerSearchRepository.vaildAgentRule(hotelId);
				if (agentType != null && agentType != "" && agentType != " ") {
					mobileAgentType = agentType;
				}
			}
			return mobileAgentType;
		}
		
	/**
	 * 將列表頁最低價促銷寫入到redis中. cache 有效期間為一小時
	 * 
	 * @param htlMinPriceResult
	 * @param cityCd
	 * @param travelType
	 * @param checkin
	 * @param checkout
	 * @param roomTypes
	 * @param customerId
	 * @param source
	 * @param agentType
	 */
	public void setCacheHtlMinPrice(List<List<HtlMinPrice>> htlMinPriceResult, String cityCd, String travelType, String checkin, String checkout, List<Integer> roomTypes, String customerId, String source, String agentType) {

		if (cacheFlag.toLowerCase().equals("true")) {
			logger.debug("setCacheHtlMinPrice 開始 cityCd:{},travelType:{}, checkin:{} ,checkout:{} ,customerId:{} ,source:{} ,roomTypes:{}", cityCd, travelType, checkin, checkout, customerId, source, roomTypes);
			try {
				// 判斷身份
				if (agentType == null) {
					agentType = this.getAgent(customerId, source, checkin, null);
				}
				String roomTypeStr = "";
				if (roomTypes != null && roomTypes.size() > 0) {
					for (Integer roomType : roomTypes) {
						roomTypeStr = roomTypeStr + roomType + ",";
					}
				} else {
					roomTypeStr = null;
				}
				String str = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).writeValueAsString(htlMinPriceResult);
				//List<HtlMinPrice> filterHtlMinPrice = new ArrayList<HtlMinPrice>();
				if (htlMinPriceResult != null && htlMinPriceResult.size() > 0) {

					ApiType apiType = Cache.ApiType.WEB;
					if ("mweb".equals(source)) {
						apiType = Cache.ApiType.MWEB;
					}
					String cacheKey = cacheService.getHtlMinPriceKey(apiType, "HtlMinPriceList", cityCd, travelType, checkin, checkout, agentType, roomTypeStr);

					CacheWrapper<?> cacheObj = cacheService.getCache(cacheKey, expireTime);
					if (cacheObj != null) {
						cacheObj = new CacheWrapper<String>(str, System.currentTimeMillis());
						int timeLeft = (int) (expireTime - (System.currentTimeMillis() - cacheObj.getSettleTime()) / 1000);
						cacheService.setCache(cacheKey, cacheObj, timeLeft);
					} else {
						// 為了cache有效時間為整點
						cacheObj = new CacheWrapper<String>(str, System.currentTimeMillis());
						String todayHm = UtilDate.getTodayHHMM();
						String mm = todayHm.substring(2, 4);
						logger.debug("TodayHHMM :" + mm);
						int cacheExpireTime = expireTime;
						if (60 - Integer.valueOf(mm) != 0) {

							cacheExpireTime = (60 - Integer.valueOf(mm)) * 60;
						}
						logger.debug("cacheExpireTime :" + cacheExpireTime);
						cacheService.setCache(cacheKey, cacheObj, cacheExpireTime);
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			logger.debug("setCacheHtlMinPrice 結束");
		}
	}
}
