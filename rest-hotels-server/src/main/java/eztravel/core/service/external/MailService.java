package eztravel.core.service.external;

public interface MailService {
  
	/**
	 * 忘記密碼郵件寄送
	 * @param targetMail
	 * @param targetPwd
	 */
	void sendForgetPasswordMail(String targetMail, String targetPwd);
	
	/**
	 * 郵件寄送
	 * @param from
	 * @param to
	 * @param subject
	 * @param content
	 */
	void sendMail(String from, String to, String subject, String content);
	void sendMail(String from, String to, String cc, String subject, String content);
	void sendMail(String from, String[] to, String subject, String content);
	
	/**
	 * 郵件寄送
	 * @param from
	 * @param to
	 * @param cc
	 * @param subject
	 * @param content
	 * @param filePath
	 * @param deleteFile
	 */
	void sendMail(String from, String to, String cc, String subject, String content, String filePath, boolean deleteFile);
	void sendMail(String from, String[] to, String[] cc, String subject, String content, String filePath, boolean deleteFile);
}
