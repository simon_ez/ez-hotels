package eztravel.core.service.external;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailServiceImpl implements MailService {
	private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);
	private final String Mail_Split = ";";
	
	@Value("${app.environment}") 
	private String environment;
	@Autowired
	private JavaMailSender javaMailSender;

	/**
	 * 忘記密碼郵件寄送
	 * @param targetMail
	 * @param targetPwd
	 */
	public void sendForgetPasswordMail(String targetMail, String targetPwd) {
		try {
			StringBuilder content = new StringBuilder();
			content.append("系統更新臨時密碼為：" + targetPwd + " , 請儘速登入系統並修改密碼，謝謝～");

			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper mail = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			mail.setFrom("Ez-Platform");
			mail.setSubject("Ez後台系統-忘記密碼郵件");
			mail.setTo(targetMail);
			mail.setText(content.toString(), true);
			javaMailSender.send(mimeMessage);
		} catch (Exception e) {
			log.error("sendForgetPassword Mail Error:" + e.toString());
		}
	}

	/**
	 * 郵件寄送
	 */
	public void sendMail(String from, String to, String subject, String content) {
		this.sendMail(from, to.split(Mail_Split), null, subject, content, null, false);
	}
	public void sendMail(String from, String to, String cc, String subject, String content) {
		this.sendMail(from, to.split(Mail_Split), cc.split(Mail_Split), subject, content, null, false);
	}
	public void sendMail(String from, String[] to, String subject, String content) {
		this.sendMail(from, to, null, subject, content, null, false);
	}
	
	/**
	 * 郵件寄送
	 * @param from
	 * @param to
	 * @param cc
	 * @param subject
	 * @param content
	 * @param filePath
	 * @param deleteFile
	 */
	public void sendMail(String from, String to, String cc, String subject, String content, String filePath, boolean deleteFile) {
		String[] t = to.split(Mail_Split), c = null;
		if(StringUtils.isNotBlank(cc))
			c = cc.split(Mail_Split);
		this.sendMail(from, t, c, subject, content, filePath, deleteFile);
	}
	public void sendMail(String from, String[] to, String[] cc, String subject, String content, String filePath, boolean deleteFile) {
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper mail = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			
			//郵寄基本資訊
			mail.setFrom(from);
			mail.setSubject(subject.concat(" (").concat(environment).concat(")"));
			
			//收件對象
			mail.setTo(to);
			if(cc != null && cc.length > 0)
				mail.setCc(cc);
			
			//文本內容
			if (StringUtils.isNotBlank(content))
				mail.setText(content, true);
			
			//夾帶檔案
			if (StringUtils.isNotBlank(filePath)) {
				FileSystemResource file = new FileSystemResource(filePath);
				mail.addAttachment(file.getFilename(), file);
			}
			
			//發送郵件
			javaMailSender.send(mimeMessage);
			
			//檔案刪除
			if(StringUtils.isNotBlank(filePath) && deleteFile){ 
				File xlsFile = new File(filePath);
				if (xlsFile.exists())
					xlsFile.delete();
			}
		} catch (Exception e) {
			log.error("sendMail Mail Error:" + e.toString());
		}
	}
}
