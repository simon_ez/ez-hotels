package eztravel.core.service.sample;

public interface SampleService {

	/**
	 * 定義接口方法名稱(作業名稱)、參數、返回內容
	 * @param sessionId
	 * @param path
	 * @param value
	 * @throws Exception
	 */
	//權限            返回     名稱        傳入參數
	//public void test(String sessionId, String path, String value) throws Exception;
	void test(String sessionId, String path, String value) throws Exception;
}