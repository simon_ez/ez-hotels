package eztravel.core.service.sample;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.service.CommonService;
import eztravel.persistence.repository.edb.SampleRepository;
import eztravel.persistence.repository.oracle.CodeRepository;
import eztravel.rest.controller.v1.SampleController;

public class SampleServiceImpl extends CommonService implements SampleService {
	private static final Logger log = LoggerFactory.getLogger(SampleController.class);

	@Autowired
	private SampleRepository sampleRepository;
	@Autowired
	private CodeRepository codeRepository;
	
	/**
	 * 定義接口方法名稱(作業名稱)、參數、返回內容
	 * @param sessionId
	 * @param path
	 * @param value
	 * @throws Exception
	 */
	public void test(String sessionId, String path, String value) throws Exception {
		try{
			// edb test
			List<Map<String, String>> queryObj = sampleRepository.query(null);
			for(Map<String, String> result: queryObj){
				log.info("User ID：" + result.get("userid"));
				log.info("FUNC：" + result.get("trifunc"));
				log.info("MSG：" + result.get("txmsg"));
			}
		}catch(Exception e){
			log.error("這是錯誤紀錄至LOG");
			e.printStackTrace();
			throw e;
		}
	}
}
