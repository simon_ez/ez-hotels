/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.cancel
 * @FileName: CancelDatePrice.java
 * @author:   cano0530
 * @date:     2016/2/26, 上午 11:55:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;

import java.util.List;
import eztravel.rest.pojo.hotel.DayPrice;


/**
 * The Class CancelDatePrice.
 * 
 * <pre>
 * 
 * </pre>
 */
public class CancelDatePrice {
  


  /** The total sale price. */
  private int totalSalePrice;
  
  /** The cancel Type. */
  private List<DayPrice> cancelDayPrice;

  /**
   * Gets the total sale price.
   * 
   * @return the total sale price
   */
  public int getTotalSalePrice() {
    return totalSalePrice;
  }

  /**
   * Sets the total sale price.
   * 
   * @param totalSalePrice
   *          the new total sale price
   */
  public void setTotalSalePrice(int totalSalePrice) {
    this.totalSalePrice = totalSalePrice;
  }

  /**
   * Gets the cancel day price.
   * 
   * @return the cancel day price
   */
  public List<DayPrice> getCancelDayPrice() {
    return cancelDayPrice;
  }

  /**
   * Sets the cancel day price.
   * 
   * @param cancelDayPrice
   *          the new cancel day price
   */
  public void setCancelDayPrice(List<DayPrice> cancelDayPrice) {
    this.cancelDayPrice = cancelDayPrice;
  }
  
  

 
}
