/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.cancel
 * @FileName: CancelOrderDetailDB.java
 * @author:   cano0530
 * @date:     2016/9/12, 下午 05:32:27
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;

import java.util.List;

import eztravel.rest.pojo.hotel.order.response.OrderDetail;

/**
 * The Class CancelOrderDetailDB.
 * 
 * <pre>
 * 
 * </pre>
 */
public class CancelOrderDetailDB extends OrderDetail {

  /** db取出的最終價格字串 ex.2000, 3000, 4000/2000, 3000, 4000. */
  private String finalPriceListDb;
  
  /** 最終價格字串對應的partSeq ex.1, 2, 5. */
  private List<String> partSeqListDb;
  
  /** 此 orderDetail 以 partSeq 所對應的最終價格 ex.2000.**/
  private int finalPrice;
  
  /**ex.000**/
  private List<String> saleType;

  /**
   * Gets the final price.
   * 
   * @return the finalPrice
   */
  public int getFinalPrice() {
    return finalPrice;
  }

  /**
   * Sets the final price.
   * 
   * @param finalPrice the finalPrice to set
   */
  public void setFinalPrice(int finalPrice) {
    this.finalPrice = finalPrice;
  }

  

  /**
   * Gets the final price list db.
   * 
   * @return the final price list db
   */
  public String getFinalPriceListDb() {
    return finalPriceListDb;
  }

  /**
   * Sets the final price list db.
   * 
   * @param finalPriceListDb
   *          the new final price list db
   */
  public void setFinalPriceListDb(String finalPriceListDb) {
    this.finalPriceListDb = finalPriceListDb;
  }

  /**
   * Gets the part seq list db.
   * 
   * @return the partSeqListDb
   */
  public List<String> getPartSeqListDb() {
    return partSeqListDb;
  }

  /**
   * Sets the part seq list db.
   * 
   * @param partSeqListDb
   *          the partSeqListDb to set
   */
  public void setPartSeqListDb(List<String> partSeqListDb) {
    this.partSeqListDb = partSeqListDb;
  }

  /**
   * Gets the sale type.
   * 
   * @return the sale type
   */
  public List<String> getSaleType() {
    return saleType;
  }

  /**
   * Sets the sale type.
   * 
   * @param saleType
   *          the new sale type
   */
  public void setSaleType(List<String> saleType) {
    this.saleType = saleType;
  }

  
}
