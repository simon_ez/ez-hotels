/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.cancel
 * @FileName: CancelDetail.java
 * @author:   cano0530
 * @date:     2016/3/23, 下午 03:14:59
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;

import java.util.List;
/**
 * The Class CancelDetail.
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @author trudie
 */
public class CancelDetail {
  
  /** The rule before. */
  private Integer ruleBefore;
  
  /** The rule value. */
  private Double ruleValue;
  
  /** The rule descs. */
  private String ruleDescs;
  
  /** The room id. */
  private String roomId;
  
  /** The refund type. */
  private String refundType;
  
  /** The rule type. */
  private String ruleType;
  
  /** The charge list. */
  private List<String> chargeList;
  
  /** The from date. */
  private String fromDate;
  
  /** The to date. */
  private String toDate;
  
  
  /**
   * Gets the from date.
   * 
   * @return the fromDate
   */
  public String getFromDate() {
    return fromDate;
  }

  /**
   * Sets the from date.
   * 
   * @param fromDate
   *          the fromDate to set
   */
  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * Gets the to date.
   * 
   * @return the toDate
   */
  public String getToDate() {
    return toDate;
  }

  /**
   * Sets the to date.
   * 
   * @param toDate
   *          the toDate to set
   */
  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  
  /**
   * Gets the rule before.
   * 
   * @return the rule before
   */
  public Integer getRuleBefore() {
    return ruleBefore;
  }

  /**
   * Sets the rule before.
   * 
   * @param ruleBefore
   *          the new rule before
   */
  public void setRuleBefore(Integer ruleBefore) {
    this.ruleBefore = ruleBefore;
  }

  /**
   * Gets the rule value.
   * 
   * @return the rule value
   */
  public Double getRuleValue() {
    return ruleValue;
  }
  
  /**
   * Sets the rule value.
   * 
   * @param ruleValue
   *          the new rule value
   */
  public void setRuleValue(Double ruleValue) {
    this.ruleValue = ruleValue;
  }
  
  /**
   * Gets the rule descs.
   * 
   * @return the rule descs
   */
  public String getRuleDescs() {
    return ruleDescs;
  }
  
  /**
   * Sets the rule descs.
   * 
   * @param ruleDescs
   *          the new rule descs
   */
  public void setRuleDescs(String ruleDescs) {
    this.ruleDescs = ruleDescs;
  }
  
  /**
   * Gets the room id.
   * 
   * @return the room id
   */
  public String getRoomId() {
    return roomId;
  }
  
  /**
   * Sets the room id.
   * 
   * @param roomId
   *          the new room id
   */
  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }
  
  /**
   * Gets the refund type.
   * 
   * @return the refund type
   */
  public String getRefundType() {
    return refundType;
  }
  
  /**
   * Sets the refund type.
   * 
   * @param refundType
   *          the new refund type
   */
  public void setRefundType(String refundType) {
    this.refundType = refundType;
  }
  
  /**
   * Gets the rule type.
   * 
   * @return the rule type
   */
  public String getRuleType() {
    return ruleType;
  }

  /**
   * Sets the rule type.
   * 
   * @param ruleType
   *          the new rule type
   */
  public void setRuleType(String ruleType) {
    this.ruleType = ruleType;
  }

  /**
   * Gets the charge list.
   * 
   * @return the charge list
   */
  public List<String> getChargeList() {
    return chargeList;
  }

  /**
   * Sets the charge list.
   * 
   * @param chargeList
   *          the new charge list
   */
  public void setChargeList(List<String> chargeList) {
    this.chargeList = chargeList;
  }
  
  
}
