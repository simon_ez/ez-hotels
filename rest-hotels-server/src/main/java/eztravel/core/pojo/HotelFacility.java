/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.hotel
 * @FileName: HotelFacility.java
 * @author:   Kent
 * @date:     2015/5/5, 下午 02:18:11
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;

/**
 * <pre> HotelFacility, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class HotelFacility {

  /** The hotel id. 飯店編號 */
  private String hotelId;
  
  /** The item cd. 飯店設施或服務項目代號 */
  private String itemCd;
  
  /** The item value. 項目值 */
  private String itemValue;
  
  /** The data1. 輔助說明1 */
  private String data1;
  
  /** The data2. 輔助說明2 */
  private String data2;
  
  /** The data3. 輔助說明3 */
  private String data3;
  
  /** The data4. 輔助說明4 */
  private String data4;
  
  /** The name. 中文描述 */
  private String name;
  
  /** The desc. 補充說明() */
  private String description;

  /**
   * Gets the hotel id.
   * 
   * @return the hotel id
   */
  public String getHotelId() {
    return hotelId;
  }

  /**
   * Sets the hotel id.
   * 
   * @param hotelId
   *          the new hotel id
   */
  public void setHotelId(String hotelId) {
    this.hotelId = hotelId;
  }

  /**
   * Gets the item cd.
   * 
   * @return the item cd
   */
  public String getItemCd() {
    return itemCd;
  }

  /**
   * Sets the item cd.
   * 
   * @param itemCd
   *          the new item cd
   */
  public void setItemCd(String itemCd) {
    this.itemCd = itemCd;
  }

  /**
   * Gets the item value.
   * 
   * @return the item value
   */
  public String getItemValue() {
    return itemValue;
  }

  /**
   * Sets the item value.
   * 
   * @param itemValue
   *          the new item value
   */
  public void setItemValue(String itemValue) {
    this.itemValue = itemValue;
  }

  /**
   * Gets the data1.
   * 
   * @return the data1
   */
  public String getData1() {
    return data1;
  }

  /**
   * Sets the data1.
   * 
   * @param data1
   *          the new data1
   */
  public void setData1(String data1) {
    this.data1 = data1;
  }

  /**
   * Gets the data2.
   * 
   * @return the data2
   */
  public String getData2() {
    return data2;
  }

  /**
   * Sets the data2.
   * 
   * @param data2
   *          the new data2
   */
  public void setData2(String data2) {
    this.data2 = data2;
  }

  /**
   * Gets the data3.
   * 
   * @return the data3
   */
  public String getData3() {
    return data3;
  }

  /**
   * Sets the data3.
   * 
   * @param data3
   *          the new data3
   */
  public void setData3(String data3) {
    this.data3 = data3;
  }

  /**
   * Gets the data4.
   * 
   * @return the data4
   */
  public String getData4() {
    return data4;
  }

  /**
   * Sets the data4.
   * 
   * @param data4
   *          the new data4
   */
  public void setData4(String data4) {
    this.data4 = data4;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   * 
   * @param name
   *          the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets the description.
   * 
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description.
   * 
   * @param description
   *          the new description
   */
  public void setDescription(String description) {
    this.description = description;
  }


  
}
