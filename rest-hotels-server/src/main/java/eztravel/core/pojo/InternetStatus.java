package eztravel.core.pojo;

/**
 * Created by jimin on 5/27/15.
 */
public class InternetStatus {
    private String partNo;
    private String roomtypeNo;
    private String internetStatus; //房型網路描述代碼, A1-部分房型無提供網路, A2-所有房型皆提供WIFI上網，部分房型提供有線上網, A3-所有房型皆提供有線上網，部分房型提供WIFI上網, A4-所有房型皆提供網路
    private String internetCharge; //房型網路收費代碼, A1 & A2 & A3-依飯店規定於飯店櫃檯收費

    public String getInternetStatus() {
        return internetStatus;
    }

    public void setInternetStatus(String internetStatus) {
        this.internetStatus = internetStatus;
    }

    public String getInternetCharge() {
        return internetCharge;
    }

    public void setInternetCharge(String internetCharge) {
        this.internetCharge = internetCharge;
    }

    public String getPartNo() {
      return partNo;
    }

    public void setPartNo(String partNo) {
      this.partNo = partNo;
    }

    public String getRoomtypeNo() {
      return roomtypeNo;
    }

    public void setRoomtypeNo(String roomtypeNo) {
      this.roomtypeNo = roomtypeNo;
    }
}
