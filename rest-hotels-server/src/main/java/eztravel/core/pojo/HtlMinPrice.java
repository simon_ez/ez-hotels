package eztravel.core.pojo;

import java.io.Serializable;

import eztravel.rest.pojo.hotel.PriceMark;

/**
 * Created by jiminhsieh on 5/6/15.
 */
public class HtlMinPrice implements Serializable{
  private static final long serialVersionUID = 1048704428372227359L;
  private String hotelId;
  private String roomId;
  private int avgSalePrice;
  private int avgSitePrice;
  private int avgCost;
  private String roomProjectName; //專案名稱
  private String roomSellPoints; //房型賣點
  private String promotionType; //earlier or later
  private String promotionDay;  //早鳥天數 or last mins天數
  private Double promotionRate; //早鳥折扣 or last mins折扣
  private String discountYN;
  private String checkIn;
  private int qty;
  private PriceMark priceMark;//The price mark. 價格註記

  public String getHotelId() {
    return hotelId;
  }

  public void setHotelId(String hotelId) {
    this.hotelId = hotelId;
  }

  public int getAvgSalePrice() {
    return avgSalePrice;
  }

  public void setAvgSalePrice(int avgSalePrice) {
    this.avgSalePrice = avgSalePrice;
  }

  public int getAvgSitePrice() {
    return avgSitePrice;
  }

  public void setAvgSitePrice(int avgSitePrice) {
    this.avgSitePrice = avgSitePrice;
  }
  
  public String getRoomProjectName() {
    return roomProjectName;
  }
  
  public void setRoomProjectName(String roomProjectName) {
    this.roomProjectName = roomProjectName;
  }

  public String getRoomSellPoints() {
    return roomSellPoints;
  }

  public void setRoomSellPoints(String roomSellPoints) {
    this.roomSellPoints = roomSellPoints;
  }

  public String getPromotionType() {
    return promotionType;
  }

  public void setPromotionType(String promotionType) {
    this.promotionType = promotionType;
  }

  public String getPromotionDay() {
    return promotionDay;
  }

  public void setPromotionDay(String promotionDay) {
    this.promotionDay = promotionDay;
  }

  public Double getPromotionRate() {
    return promotionRate;
  }

  public void setPromotionRate(Double promotionRate) {
    this.promotionRate = promotionRate;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  public String getDiscountYN() {
    return discountYN;
  }

  public void setDiscountYN(String discountYN) {
    this.discountYN = discountYN;
  }

  /**
   * @return the checkIn
   */
  public String getCheckIn() {
    return checkIn;
  }

  /**
   * @param checkIn the checkIn to set
   */
  public void setCheckIn(String checkIn) {
    this.checkIn = checkIn;
  }
  
	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

  public int getAvgCost() {
    return avgCost;
  }

  public void setAvgCost(int avgCost) {
    this.avgCost = avgCost;
  }

  public PriceMark getPriceMark() {
    return priceMark;
  }

  public void setPriceMark(PriceMark priceMark) {
    this.priceMark = priceMark;
  }
	
	
}
