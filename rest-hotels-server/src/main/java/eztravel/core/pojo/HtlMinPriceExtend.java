package eztravel.core.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jiminhsieh on 5/6/15.
 */
public class HtlMinPriceExtend extends HtlMinPrice implements Serializable  {

  /**
   * 
   */
  private static final long serialVersionUID = 6320842894812881596L;
  private String begDt;
  private int cost;
  private int promoprice;
  private int sitePrice;
  private String levelType;
  private String level1_r2;
  private String level2_r2;
  private String level3_r2; 
  private String level4_r2;
  private List<HotelPromoInfo> hotelPromo;
  private String travelType;
  

  public String getBegDt() {
    return begDt;
  }

  public void setBegDt(String begDt) {
    this.begDt = begDt;
  }

  public int getCost() {
    return cost;
  }

  public void setCost(int cost) {
    this.cost = cost;
  }

  public int getPromoprice() {
    return promoprice;
  }

  public void setPromoprice(int promoprice) {
    this.promoprice = promoprice;
  }

  public void setSitePrice(int sitePrice) {
    this.sitePrice = sitePrice;
  }
  

  public String getLevelType() {
    return levelType;
  }

  public void setLevelType(String levelType) {
    this.levelType = levelType;
  }

  public String getLevel1_r2() {
    return level1_r2;
  }

  public void setLevel1_r2(String level1_r2) {
    this.level1_r2 = level1_r2;
  }

  public String getLevel2_r2() {
    return level2_r2;
  }

  public void setLevel2_r2(String level2_r2) {
    this.level2_r2 = level2_r2;
  }

  public String getLevel3_r2() {
    return level3_r2;
  }

  public void setLevel3_r2(String level3_r2) {
    this.level3_r2 = level3_r2;
  }

  public String getLevel4_r2() {
    return level4_r2;
  }

  public void setLevel4_r2(String level4_r2) {
    this.level4_r2 = level4_r2;
  }



  public List<HotelPromoInfo> getHotelPromo() {
    return hotelPromo;
  }

  public void setHotelPromo(List<HotelPromoInfo> hotelPromo) {
    this.hotelPromo = hotelPromo;
  }

  public int getSitePrice() {
    return sitePrice;
  }

  public String getTravelType() {
	return travelType;
  }

  public void setTravelType(String travelType) {
	this.travelType = travelType;
  }
	
	
}
