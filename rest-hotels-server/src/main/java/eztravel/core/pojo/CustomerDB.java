package eztravel.core.pojo;

/**
 * Created by jimin on 5/28/15.
 */
public class CustomerDB {
  private String nationality;
  private String agentType;

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getAgentType() {
    return agentType;
  }

  public void setAgentType(String agentType) {
    this.agentType = agentType;
  }
}
