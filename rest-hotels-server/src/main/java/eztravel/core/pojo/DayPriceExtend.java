/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.db
 * @FileName: DayPriceExtend.java
 * @author:   cano0530
 * @date:     2017/5/17, 下午 01:51:12
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;

import java.util.List;

/**
 * Created by jiminhsieh on 5/6/15.
 */
public class DayPriceExtend  {

  /** The room Id. */
  private String roomId;
  
  /** The beg dt. */
  private String begDt;
  
  /** The cost. */
  private int cost;
  
  /** The qty. */
  private int qty;
  
  /** The promoprice. */
  private int promoprice;
  
  /** The site price. */
  private int sitePrice;
  
  /** The level type. */
  private String levelType;
  
  /** The level1_r2. */
  private String level1_r2;
  
  /** The level2_r2. */
  private String level2_r2;
  
  /** The level3_r2. */
  private String level3_r2; 
  
  /** The level4_r2. */
  private String level4_r2;
  
  /** The discount yn. */
  private String discountYN;
  
  private List<HotelPromoInfo> hotelPromo;
  
  /** The computed promo price. 每日促銷折扣金額*/
  private int computedPromoPrice;
  
  /** The computed agent price.每日agent折扣金額 */
  private int computedAgentPrice;

  
  /**
   * Gets the beg dt.
   * 
   * @return the beg dt
   */
  public String getBegDt() {
    return begDt;
  }
  
  /**
   * Sets the beg dt.
   * 
   * @param begDt
   *          the new beg dt
   */
  public void setBegDt(String begDt) {
    this.begDt = begDt;
  }
  
  /**
   * Gets the cost.
   * 
   * @return the cost
   */
  public int getCost() {
    return cost;
  }
  
  /**
   * Sets the cost.
   * 
   * @param cost
   *          the new cost
   */
  public void setCost(int cost) {
    this.cost = cost;
  }
  
  /**
   * Gets the qty.
   * 
   * @return the qty
   */
  public int getQty() {
    return qty;
  }
  
  /**
   * Sets the qty.
   * 
   * @param qty
   *          the new qty
   */
  public void setQty(int qty) {
    this.qty = qty;
  }
  
  /**
   * Gets the promoprice.
   * 
   * @return the promoprice
   */
  public int getPromoprice() {
    return promoprice;
  }
  
  /**
   * Sets the promoprice.
   * 
   * @param promoprice
   *          the new promoprice
   */
  public void setPromoprice(int promoprice) {
    this.promoprice = promoprice;
  }
  
  /**
   * Gets the site price.
   * 
   * @return the site price
   */
  public int getSitePrice() {
    return sitePrice;
  }
  
  /**
   * Sets the site price.
   * 
   * @param sitePrice
   *          the new site price
   */
  public void setSitePrice(int sitePrice) {
    this.sitePrice = sitePrice;
  }
  
  /**
   * Gets the level type.
   * 
   * @return the level type
   */
  public String getLevelType() {
    return levelType;
  }
  
  /**
   * Sets the level type.
   * 
   * @param levelType
   *          the new level type
   */
  public void setLevelType(String levelType) {
    this.levelType = levelType;
  }
  
  /**
   * Gets the level1_r2.
   * 
   * @return the level1_r2
   */
  public String getLevel1_r2() {
    return level1_r2;
  }
  
  /**
   * Sets the level1_r2.
   * 
   * @param level1_r2
   *          the new level1_r2
   */
  public void setLevel1_r2(String level1_r2) {
    this.level1_r2 = level1_r2;
  }
  
  /**
   * Gets the level2_r2.
   * 
   * @return the level2_r2
   */
  public String getLevel2_r2() {
    return level2_r2;
  }
  
  /**
   * Sets the level2_r2.
   * 
   * @param level2_r2
   *          the new level2_r2
   */
  public void setLevel2_r2(String level2_r2) {
    this.level2_r2 = level2_r2;
  }
  
  /**
   * Gets the level3_r2.
   * 
   * @return the level3_r2
   */
  public String getLevel3_r2() {
    return level3_r2;
  }
  
  /**
   * Sets the level3_r2.
   * 
   * @param level3_r2
   *          the new level3_r2
   */
  public void setLevel3_r2(String level3_r2) {
    this.level3_r2 = level3_r2;
  }
  
  /**
   * Gets the level4_r2.
   * 
   * @return the level4_r2
   */
  public String getLevel4_r2() {
    return level4_r2;
  }
  
  /**
   * Sets the level4_r2.
   * 
   * @param level4_r2
   *          the new level4_r2
   */
  public void setLevel4_r2(String level4_r2) {
    this.level4_r2 = level4_r2;
  }
  
  /**
   * Gets the discount yn.
   * 
   * @return the discount yn
   */
  public String getDiscountYN() {
    return discountYN;
  }
  
  /**
   * Sets the discount yn.
   * 
   * @param discountYN
   *          the new discount yn
   */
  public void setDiscountYN(String discountYN) {
    this.discountYN = discountYN;
  }

  public List<HotelPromoInfo> getHotelPromo() {
    return hotelPromo;
  }

  public void setHotelPromo(List<HotelPromoInfo> hotelPromo) {
    this.hotelPromo = hotelPromo;
  }

  public int getComputedPromoPrice() {
    return computedPromoPrice;
  }

  public void setComputedPromoPrice(int computedPromoPrice) {
    this.computedPromoPrice = computedPromoPrice;
  }

  public int getComputedAgentPrice() {
    return computedAgentPrice;
  }

  public void setComputedAgentPrice(int computedAgentPrice) {
    this.computedAgentPrice = computedAgentPrice;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  
	
}
