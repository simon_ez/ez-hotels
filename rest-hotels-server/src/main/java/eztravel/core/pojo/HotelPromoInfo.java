/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.db
 * @FileName: HotelPromoInfo.java
 * @author:   002766
 * @date:     2018/3/7, 下午 03:24:56
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo;


/**
 * The Class HotelPromoInfo.
 * 
 * <pre>
 * 
 * </pre>
 */
public class HotelPromoInfo {
  
  /** The promo type. */
  private String promoType; //earlier or later
  
  /** The beg date. */
  private String begDate; //入住日期begDate
  
  /** The promo day. */
  private String promoDay;  //早鳥天數 or last mins天數
  
  /** The promo rate. */
  private Double promoRate; //早鳥折扣 or last mins折扣
  
  /** The app range. */
  private String appRange; //適用範圍,1全部,2web,3app
  
  /** The cost type. */
  private String costType;//成本模式,1飯店提案,2PM自損
   
  /** The discount type. */
  private String discountType;//折扣方式,1百分比,2定額
  
  
  /**
   * Gets the promo type.
   * 
   * @return the promo type
   */
  public String getPromoType() {
    return promoType;
  }
  
  /**
   * Sets the promo type.
   * 
   * @param promoType
   *          the new promo type
   */
  public void setPromoType(String promoType) {
    this.promoType = promoType;
  }
  
  /**
   * Gets the promo day.
   * 
   * @return the promo day
   */
  public String getPromoDay() {
    return promoDay;
  }
  
  /**
   * Sets the promo day.
   * 
   * @param promoDay
   *          the new promo day
   */
  public void setPromoDay(String promoDay) {
    this.promoDay = promoDay;
  }
  
  /**
   * Gets the promo rate.
   * 
   * @return the promo rate
   */
  public Double getPromoRate() {
    return promoRate;
  }
  
  /**
   * Sets the promo rate.
   * 
   * @param promoRate
   *          the new promo rate
   */
  public void setPromoRate(Double promoRate) {
    this.promoRate = promoRate;
  }

  /**
   * Gets the app range.
   * 
   * @return the app range
   */
  public String getAppRange() {
    return appRange;
  }

  /**
   * Sets the app range.
   * 
   * @param appRange
   *          the new app range
   */
  public void setAppRange(String appRange) {
    this.appRange = appRange;
  }

  /**
   * Gets the cost type.
   * 
   * @return the cost type
   */
  public String getCostType() {
    return costType;
  }

  /**
   * Sets the cost type.
   * 
   * @param costType
   *          the new cost type
   */
  public void setCostType(String costType) {
    this.costType = costType;
  }

  /**
   * Gets the discount type.
   * 
   * @return the discount type
   */
  public String getDiscountType() {
    return discountType;
  }

  /**
   * Sets the discount type.
   * 
   * @param discountType
   *          the new discount type
   */
  public void setDiscountType(String discountType) {
    this.discountType = discountType;
  }

  /**
   * Gets the beg date.
   * 
   * @return the beg date
   */
  public String getBegDate() {
    return begDate;
  }

  /**
   * Sets the beg date.
   * 
   * @param begDate
   *          the new beg date
   */
  public void setBegDate(String begDate) {
    this.begDate = begDate;
  }

   
  
}
