/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.config.platform
 * @FileName: RootConfig.java
 * @author:   cano0530
 * @date:     2016/7/18, 上午 10:35:44
 *
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.config.platform;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import eztravel.config.AppProperties;
import eztravel.config.server.CommonJettyConfig;
import eztravel.core.service.external.MailService;
import eztravel.core.service.external.MailServiceImpl;
import eztravel.core.service.hotel.ActiveRoomService;
import eztravel.core.service.hotel.ActiveRoomServiceImpl;
import eztravel.core.service.hotel.AgentService;
import eztravel.core.service.hotel.AgentServiceImpl;
import eztravel.core.service.hotel.CacheService;
import eztravel.core.service.hotel.CacheServiceImpl;
import eztravel.core.service.hotel.CancelService;
import eztravel.core.service.hotel.CancelServiceImpl;
import eztravel.core.service.hotel.FacilityService;
import eztravel.core.service.hotel.FacilityServiceImpl;
import eztravel.core.service.hotel.PreCacheService;
import eztravel.core.service.hotel.PreCacheServiceImpl;
import eztravel.core.service.hotel.PromoService;
import eztravel.core.service.hotel.PromoServiceImpl;
import eztravel.core.service.hotel.RedisService;
import eztravel.core.service.hotel.RedisServiceImpl;
import eztravel.core.service.hotel.RoomCommonService;
import eztravel.core.service.hotel.RoomCommonServiceImpl;
import eztravel.core.service.hotel.SearchService;
import eztravel.core.service.hotel.SearchServiceImpl;
import eztravel.core.service.sample.SampleService;
import eztravel.core.service.sample.SampleServiceImpl;
import eztravel.persistence.repository.oracle.CommonRepository;

/**
 * <pre> RootConfig, TODO: add Class Javadoc here. </pre>
 * @author Kent
 */

@Configuration
@Import({ AppProperties.class, EdbRootConfig.class, DataSourceOra11g.class})
@EnableTransactionManagement
@MapperScan(basePackages = { "eztravel.persistence.repository.oracle" }, sqlSessionFactoryRef = "sqlSessionFactory")
public class RootConfig extends CommonJettyConfig {

	/**
	 * Transaction Manager.
	 * @param oracleDataSource
	 * @return the data source transaction manager
	 * @throws Exception
	 */
	@Bean
	public DataSourceTransactionManager oracleTx(DataSource oracleDataSource)
			throws Exception {
		DataSourceTransactionManager oracleTxManager = new DataSourceTransactionManager();
		oracleTxManager.setDataSource(oracleDataSource);
		return oracleTxManager;
	}
	
	/**
	 * Mail service.
	 * @return the Mail service
	 */
	@Bean
	public MailService mailService() {
		return new MailServiceImpl();
	}

	/**
	 * MailSend service.
	 * @return the MailSend service
	 */
	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp3.eztravel.com.tw");
		return mailSender;
	}
	
	@Bean
	public SampleService sampleService() {
		return new SampleServiceImpl();
	}
	
	@Bean
	public ActiveRoomService activeRoomService() {
		return new ActiveRoomServiceImpl();
	}
	
	@Bean
	public AgentService agentService(CommonRepository commonRepository) {
		return new AgentServiceImpl(commonRepository);
	}
	
	@Bean
	public CacheService cacheService() {
		return new CacheServiceImpl();
	}
	
	@Bean
	public CancelService cancelService() {
		return new CancelServiceImpl();
	}
	
	@Bean
	public FacilityService facilityService() {
		return new FacilityServiceImpl();
	}
	
	@Bean
	public PreCacheService preCacheService() {
		return new PreCacheServiceImpl();
	}
	
	@Bean
	public PromoService promoService() {
		return new PromoServiceImpl();
	}
	
	@Bean
	public RedisService redisService() {
		return new RedisServiceImpl();
	}
	
	@Bean
	public RoomCommonService roomCommonService() {
		return new RoomCommonServiceImpl();
	}
	
	@Bean
	public SearchService searchService() {
		return new SearchServiceImpl();
	}
}
