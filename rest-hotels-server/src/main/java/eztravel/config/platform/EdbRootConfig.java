package eztravel.config.platform;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = { "eztravel.persistence.repository.edb" }, sqlSessionFactoryRef = "edbSessionFactory")
public class EdbRootConfig {
	/**
	 * Edb data source.
	 * @return the data source
	 * @throws Exception
	 */
	@Bean
	protected DataSource edbDataSource(@Value("${edb.url}") String url,
										@Value("${edb.port}") String port,
										@Value("${edb.database}") String database,
										@Value("${edb.user}") String user,
										@Value("${edb.password}") String password,
										@Value("${edb.maxPoolSize:1}") int maxPoolSize,
										@Value("${edb.minIdle:0}") int minIdle,
										@Value("${database.prop.machine}") String machine,
										@Value("${database.prop.program}") String program) throws Exception {
		HikariDataSource ds = jdbcSrc(url, user, password, maxPoolSize, minIdle);
		//HikariDataSource ds = jndiSrc(url, port, database, user, password, maxPoolSize, minIdle);
		Properties props = new Properties();
		props.setProperty("v$session.machine", machine);
		props.setProperty("v$session.program", program);
		ds.addDataSourceProperty("connectionProperties", props);
		return ds;
	}

	private HikariDataSource jdbcSrc(String url, String user, String pwd, int maxPool, int minIdle){
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(url);
		config.setUsername(user);
		config.setPassword(pwd);
		config.setMaximumPoolSize(maxPool);
		config.setMinimumIdle(minIdle);
		return new HikariDataSource(config);
	}
	
	/*
	private HikariDataSource jndiSrc(String url, String port, String database, String user, String pwd, int maxPool, int minIdle){
		HikariDataSource ds = new HikariDataSource();
		ds.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
		//ds.setDataSourceClassName("com.edb.ds.PGSimpleDataSource");
		ds.addDataSourceProperty("serverName", url);
	    ds.addDataSourceProperty("portNumber", port);
	    ds.addDataSourceProperty("databaseName", database);
	    ds.addDataSourceProperty("user", user);
	    ds.addDataSourceProperty("password", pwd);
	    ds.setMaximumPoolSize(maxPool);
	    ds.setMinimumIdle(minIdle);
		return ds;
	}*/

	/**
	 * Edb session factory.
	 * @return the sql session factory
	 * @throws Exception
	 */
	@Bean
	public SqlSessionFactory edbSessionFactory(DataSource edbDataSource) throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(edbDataSource);
		return factoryBean.getObject();
	}

	/**
	 * Transaction Manager
	 * @throws Exception
	 */
	@Bean
	public DataSourceTransactionManager edbTx(DataSource edbDataSource)throws Exception {
		DataSourceTransactionManager txManager = new DataSourceTransactionManager();
		txManager.setDataSource(edbDataSource);
		return txManager;
	}
}
