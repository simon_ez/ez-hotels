package eztravel.config.platform;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = { "eztravel.persistence.repository.oracle11g" }, sqlSessionFactoryRef = "ora11gSessionFactory")
public class DataSourceOra11g {
	/**
	 * Oracle 11g data source.
	 * @return the data source
	 * @throws Exception
	 */
	@Bean
	protected DataSource ora11gDataSource(@Value("${ora11g.url}") String url,
										@Value("${ora11g.user}") String user,
										@Value("${ora11g.password}") String password,
										@Value("${ora11g.maxPoolSize:1}") int maxPoolSize,
										@Value("${ora11g.minIdle:0}") int minIdle,
										@Value("${database.prop.machine}") String machine,
										@Value("${database.prop.program}") String program) throws Exception {
		HikariDataSource ds = new HikariDataSource();
		ds.setDataSourceClassName("oracle.jdbc.pool.OracleDataSource");
		ds.addDataSourceProperty("url", url);
		ds.addDataSourceProperty("user", user);
		ds.addDataSourceProperty("password", password);
		Properties props = new Properties();
		props.setProperty("v$session.machine", machine);
		props.setProperty("v$session.program", program);
		ds.addDataSourceProperty("connectionProperties", props);
		ds.setMaximumPoolSize(maxPoolSize);
		ds.setMinimumIdle(minIdle);
		return ds;
	}

	/**
	 * Oracle 11g session factory.
	 * 
	 * @return the sql session factory
	 * @throws Exception
	 */
	@Bean
	public SqlSessionFactory ora11gSessionFactory(DataSource ora11gDataSource) throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(ora11gDataSource);
		return factoryBean.getObject();
	}

	/**
	 * Transaction Manager
	 * 
	 * @throws Exception
	 */
	@Bean
	public DataSourceTransactionManager ora11gTx(DataSource ora11gDataSource)throws Exception {
		DataSourceTransactionManager txManager = new DataSourceTransactionManager();
		txManager.setDataSource(ora11gDataSource);
		return txManager;
	}
}
