/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.config.platform
 * @FileName: ServletConfig.java
 * @author:   Kent
 * @date:     2016/5/4, 下午 03:59:26
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.config.platform;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import eztravel.config.server.CommonServletConfig;
import eztravel.rest.controller.RestHandlerInterceptor;

/**
 * <pre> ServletConfig, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
@Configuration
@ComponentScan(basePackages = {"eztravel.rest.controller"})
@Import({ SwaggerConfig.class })
public class ServletConfig extends CommonServletConfig {
	/*
   * (non-Javadoc)
   * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry)
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new RestHandlerInterceptor());
    super.addInterceptors(registry);
  }
}
