/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.config
 * @FileName: AppProperties.java
 * @author:   ezkent
 * @date:     2016年5月4日, 上午11:16:45
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import eztravel.config.server.EncryptableProperties;

/**
 * <pre> AppProperties, TODO: add Class Javadoc here. </pre>
 * @author Kent
 */
public class AppProperties extends EncryptableProperties {  // 繼承EncryptableProperties以取得EncryptablePropertyPlaceholderConfigurer
	
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(AppProperties.class);

	/**
	 * Property placeholder configurer.
	 * @return the property placeholder configurer
	 */
	@Bean
	public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		// 為提供正式台設定檔密碼加密功能，用EncryptablePropertyPlaceholderConfigurer取代PropertyPlaceholderConfigurer
	    // PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
	    PropertyPlaceholderConfigurer configurer = this.encryptablePropertyPlaceholderConfigurer();
	    
		String location = System.getenv("HOTEL_CONF");
		Resource resource = null;
		if (location == null) {
			location = "config.properties";
			resource = new ClassPathResource("config.properties");
		} else {
			location = location.concat("/jetty/config.properties");
			resource = new FileSystemResource(location);
		}
		logger.info("Use config file: " + location);
		configurer.setLocation(resource);
		return configurer;
	}
	
}
