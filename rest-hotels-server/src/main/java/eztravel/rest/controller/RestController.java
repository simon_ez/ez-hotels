/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.controller
 * @FileName: RestController.java
 * @author:   Kent
 * @date:     2016/5/4, 下午 04:01:46
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eztravel.rest.pojo.common.RestError;
import eztravel.rest.pojo.common.RestResource;
import eztravel.rest.pojo.common.RestResourceFactory;
import eztravel.util.RestCode;
import eztravel.util.exception.BsLogicException;
import eztravel.util.exception.LoginException;
import eztravel.util.exception.ParamException;

/**
 * <pre> RestController, TODO: add Class Javadoc here. </pre>
 *
 * @author ezkent
 */
public abstract class RestController {

  /**
   * Success.
   *
   * @param <T>
   *          the generic type
   * @param items
   *          the items
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> success(List<T> items) {
    return fill(HttpStatus.OK.value(), items, null, null, null, null, null, null);
  }
  
  /**
   * Fail.
   *
   * @param <T>
   *          the generic type
   * @param error
   *          the error
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fail(RestError... error) {
    return fill(HttpStatus.INTERNAL_SERVER_ERROR.value(), null, null, null, null, null, error);
  }
  
  /**
   * Fail.
   *
   * @param <T>
   *          the generic type
   * @param errCode
   *          the RestErrCode mapping Code
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fail(RestCode errCode, String description) {
    RestError error = new RestError();
    error.setCode(errCode.getCode());
    error.setMessage(errCode.getMsg());
    error.setDescription(description);
    return fill(HttpStatus.INTERNAL_SERVER_ERROR.value(), null, null, null, null, null, error);
  }

  /**
   * Fail.
   *
   * @param <T>
   *          the generic type
   * @param errCode
   *          the err code
   * @param errMessage
   *          the err message
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fail(Integer errCode, String errMessage, String description) {
    RestError error = new RestError();
    error.setCode(errCode);
    error.setMessage(errMessage);
    error.setDescription(description);
    return fill(HttpStatus.INTERNAL_SERVER_ERROR.value(), null, null, null, null, null, error);
  }
  
  /**
   * Fail.
   * @param Exception
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fail(Exception e) {
  	RestError error;
  	int httpStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
  	if (e instanceof ParamException){
  		error = this.buildRestError(RestCode.PARAM_MISSING, e);
      httpStatus = HttpStatus.BAD_REQUEST.value();
  	}else if(e instanceof BsLogicException){
  		error = this.buildRestError(RestCode.BNSINESS_LOGIC_ERR, e);
      httpStatus = HttpStatus.ACCEPTED.value();
  	}else if(e instanceof LoginException){
  		error = this.buildRestError(RestCode.LOGIN_ERROR, e);
      httpStatus = HttpStatus.UNAUTHORIZED.value();
  	}else if(e instanceof DuplicateKeyException){
  		error = this.buildRestError(RestCode.BNSINESS_LOGIC_ERR, e);
  		error.setDescription(e.getCause().toString());
      httpStatus = HttpStatus.ACCEPTED.value();
  	}else if(e instanceof DataIntegrityViolationException){
  		error = this.buildRestError(RestCode.PARAM_MISSING, e);
  		error.setDescription(e.getCause().toString());
      httpStatus = HttpStatus.BAD_REQUEST.value();
  	}else if(e instanceof ValidationException){
  		error = this.buildRestError(RestCode.BNSINESS_LOGIC_ERR, e);
  		error.setDescription(e.getMessage());
  		httpStatus = HttpStatus.BAD_REQUEST.value();
  	}else{
  		error = this.buildRestError(RestCode.SYSTEM_UNKNOWN_ERR, e);
  		httpStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
  	}
    return fill(httpStatus, null, null, null, null, null, error);
  }
  
  /**
   * RestError Build
   * @param restCode
   * @param e
   * @return RestError
   */
  private RestError buildRestError(RestCode restCode, Exception e){
  	RestError error = new RestError();
  	error.setCode(restCode.getCode());
  	error.setMessage(restCode.getMsg());
  	error.setDescription(e.getMessage());
  	return error;
  }

  /**
   * Fail.
   * @param httpCode
   * @param errCode
   * @param description
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fail(HttpStatus httpCode, Integer errCode, String description) {
    RestError error = new RestError();
    error.setCode(errCode);
    error.setMessage(RestCode.fromCode(errCode).getMsg());
    error.setDescription(description);
    return fill(httpCode.value(), null, null, null, null, null, error);
  }

  /**
   * Fill.
   *
   * @param <T>
   *          the generic type
   * @param status
   *          the status
   * @param items
   *          the items
   * @param totalItems
   *          the total items
   * @param startIndex
   *          the start index
   * @param itemsPerPage
   *          the items per page
   * @param currentItem
   *          the current item
   * @param errors
   *          the errors
   * @return the response entity
   */
  protected <T> ResponseEntity<RestResource<T>> fill(Integer status, List<T> items,
      Integer totalItems, Integer startIndex, Integer itemsPerPage,
      Integer currentItem, RestError... errors) {
    RestResource<T> body = RestResourceFactory.newInstance();
    body.setStatus(status);
    body.setItems(items);
    body.setTotalItems(totalItems);
    body.setStartIndex(startIndex);
    body.setItemsPerPage(itemsPerPage);
    body.setCurrentItemCount(currentItem);
    if(RestErrorIsEmpty(errors))
    	body.setErrors(Arrays.asList(errors));
    ResponseEntity<RestResource<T>> entity = new ResponseEntity<RestResource<T>>(body, HttpStatus.OK);
    return entity;
  }
  
  
  /**
   * check RestError isEmpty
   * @param errors
   * @return boolean
   */
  private boolean RestErrorIsEmpty(RestError... errors){
  	for(RestError err: errors){
  		if(err != null)
  			return true;
  	}
  	return false;
  }
}
