/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.controller
 * @FileName: RestHandlerInterceptor.java
 * @author:   Kent
 * @date:     2016/5/4, 下午 04:01:52
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * <pre> RestHandlerInterceptor, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class RestHandlerInterceptor extends HandlerInterceptorAdapter {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(RestHandlerInterceptor.class);

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
   */
  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    Long startTime = (Long)request.getAttribute("startTime");
    Long executeTime = System.currentTimeMillis() - startTime;
    
    //Trigger URL
    StringBuilder log = new StringBuilder();
    log.append("Request URL::").append(request.getRequestURL().toString());
    
    /*
     * 目前POST跟Header暫時先不記錄，可能有安全性問題
     */
    
    //Query param
    Map<String, String[]> param = request.getParameterMap();
    if(param != null && !param.isEmpty()){
    	log.append("?");
    	for(String paramKey: param.keySet()){
      	log.append(paramKey).append("=");
      	for(String paramValue: param.get(paramKey))
      		log.append(paramValue).append(",");
      	log.replace(log.length()-1, log.length(), "&");
      }
    	log.delete(log.length()-1, log.length());
    }
    log.append(" :: Verb=").append(request.getMethod());
    log.append(" :: Execute Time=").append(executeTime);
    logger.info(log.toString());
    super.afterCompletion(request, response, handler, ex);
  }

  @Override
  public void afterConcurrentHandlingStarted(HttpServletRequest request,
      HttpServletResponse response, Object handler) throws Exception {
    super.afterConcurrentHandlingStarted(request, response, handler);
  }

  @Override
  public void postHandle(HttpServletRequest request,
      HttpServletResponse response, Object handler, ModelAndView modelAndView)
          throws Exception {
    super.postHandle(request, response, handler, modelAndView);
  }

  @Override
  public boolean preHandle(HttpServletRequest request,
      HttpServletResponse response, Object handler) throws Exception {
    
    Long startTime = System.currentTimeMillis();
    request.setAttribute("startTime", startTime);
    /*
    logger.info("Request URL::" + request.getRequestURL().toString()
        + ":: Start Time=" + startTime);
    */
    
    return super.preHandle(request, response, handler);
  }
  
  
}
