package eztravel.rest.controller.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import tw.com.eztravel.aes_string_convert.AES;
import eztravel.core.service.sample.SampleService;
import eztravel.jasypt.encryption.EzStringEncryptor;
import eztravel.rest.controller.RestController;
import eztravel.rest.pojo.common.RestResource;

@Api(value="/Sample Service", description="Sample API")
@Controller
@RequestMapping(value="/rest/v1")
public class SampleController extends RestController {
	
	@Autowired
	private SampleService sampleService;
  
	@ApiOperation(value = "測試代碼01", notes = "純粹就是測試用", response = String.class)
	@RequestMapping(value = "/sample/test1/{path}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<RestResource<String>> test1(@ApiParam(value = "session", required = true) @RequestHeader String sessionId
													, @ApiParam(value = "URL PATH參數", required = true) @PathVariable String path
													, @ApiParam(value = "指定參數", required = false) @RequestParam(required = false) String value){
		
		List<String> result = new ArrayList<String>();
		try {
			sampleService.test(sessionId, path, value);
		} catch (Exception e) {
			return fail(e);
		}
		return success(result);
		
	}
	
	@ApiOperation(value = "加密文字", notes = "DB連線使用加密法", response = String.class, hidden = true)
	@RequestMapping(value = "/sample/encode", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<RestResource<String>> encodeString(@ApiParam(value = "欲加密文字", required = true) @RequestParam(required = true) String str) {
		List<String> result = new ArrayList<String>();
		try {
			EzStringEncryptor encryptor = new EzStringEncryptor();
			if (StringUtils.isBlank(System.getenv("JETTY_KEY_PATH")))
				throw new RuntimeException("取得 JETTY_KEY_PATH 系統變數失敗");
			if (StringUtils.isBlank(System.getenv("JETTY_KEY_VERSION")))
				throw new RuntimeException("取得 JETTY_KEY_VERSION 系統變數失敗");
			encryptor.setAes(new AES(System.getenv("JETTY_KEY_PATH")));
			encryptor.setKeyVersion(System.getenv("JETTY_KEY_VERSION"));
			result.add(encryptor.encrypt(str));
		} catch (Exception e) {
			return fail(e);
		}
		return success(result);
	}

}