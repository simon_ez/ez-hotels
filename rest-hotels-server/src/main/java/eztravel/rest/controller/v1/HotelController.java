package eztravel.rest.controller.v1;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import eztravel.core.service.hotel.SearchService;
import eztravel.rest.controller.RestController;
import eztravel.rest.controller.common.RestException;
import eztravel.rest.pojo.common.RestResource;
import eztravel.rest.pojo.common.RestResourceFactory;
import eztravel.rest.pojo.hotel.Room;
import eztravel.rest.util.common.StringUtils;

@Api(value = "/Hotel Query Service", description = "Hotel API")
@Controller
@RequestMapping(value = "/rest/v1")
public class HotelController extends RestController {
	private static final Logger logger = LoggerFactory.getLogger(HotelController.class);
	
	@Autowired
	private SearchService searchService;

	@ApiOperation(value = "商品頁下半部 房型列表", notes = "商品頁下半部 房型列表", response = Room.class)
	@RequestMapping(value = "/rooms/{hotelCode}", method = GET)
	public ResponseEntity<RestResource<Room>> getRooms(@PathVariable String hotelCode
																												, @RequestParam(required = false) String roomCode
																												, @RequestParam String roomQty
																												, @RequestParam String checkInDate
																												, @RequestParam String checkOutDate
																												, @RequestParam(required = false) String customerId
																												, @RequestParam(required = false) String source) {
		long startTime = System.currentTimeMillis();
		logger.debug("### 取得房型列表 開始 {} ###", hotelCode);

		RestResource<Room> body = RestResourceFactory.newInstance();

		try {
			body.setItems(searchService.getHotelRooms(hotelCode, roomCode, Integer.parseInt(roomQty), checkInDate, checkOutDate, null, customerId, StringUtils.fp_isNull(source), "v1"));
			body.setStatus(200);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new RestException(e.getMessage(), body);
		}

		logger.info("### 取得房型列表 結束 {} ### ({}ms)", hotelCode, System.currentTimeMillis() - startTime);
		return new ResponseEntity<RestResource<Room>>(body, HttpStatus.OK);
	}

}