package eztravel.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class UtilDate {
  
  /**
   * 取得日期相差天數
   * @param String startday
   * @param String endday
   * @return 天數
   * 取得日期相差天數
   */
  public static int getDiffDays(String startday, String endday) {
    Calendar calendarStart = new GregorianCalendar(Integer.parseInt(startday.substring(0,4)),
        Integer.parseInt(startday.substring(4,6))-1,
        Integer.parseInt(startday.substring(6,8)));

    Calendar calendarEnd = new GregorianCalendar(Integer.parseInt(endday.substring(0,4)),
        Integer.parseInt(endday.substring(4,6))-1,
        Integer.parseInt(endday.substring(6,8)));
    return Integer.parseInt(""+(calendarEnd.getTime().getTime()-calendarStart.getTime().getTime()) / 86400000);
  }
  
	/**
	 * 抓取今天日期
	 * @param dFormat  輸出格式，預設格式yyyy/MM/dd HH:mm:ss
	 * @return 抓取今天日期
	 */
	public static String getToday(String dFormat) {
		if (dFormat.length() == 0 || dFormat == null)
			dFormat = "yyyy/MM/dd HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
		return formatter.format(new java.util.Date()).toString();
	}
  
	/**
	 * 取得系統當下時間時分
	 * @return
	 */
	public static String getTodayHHMM() {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("HHmm");
		return format.format(date);
	}
	
	/**
	 * NNNNNNN 日一二三四五六
	 * @param dateType
	 * @return
	 */
	public static List<String> convertDateType(String dateType) {
		List<String> result = new ArrayList<String>();
		if (dateType != null && dateType.trim().length() > 0) {
			for (int i = 0; i < dateType.length(); i++) {
				String str = dateType.substring(i, i + 1);
				switch (i) {
					case 0:
						if (str.equals("Y")) {
							String week = "日";
							result.add(week);
						}
						break;
					case 1:
						if (str.equals("Y")) {
							String week = "一";
							result.add(week);
						}
						break;
					case 2:
						if (str.equals("Y")) {
							String week = "二";
							result.add(week);
						}
						break;
					case 3:
						if (str.equals("Y")) {
							String week = "三";
							result.add(week);
						}
						break;
					case 4:
						if (str.equals("Y")) {
							String week = "四";
							result.add(week);
						}
						break;
					case 5:
						if (str.equals("Y")) {
							String week = "五";
							result.add(week);
						}
						break;
					case 6:
						if (str.equals("Y")) {
							String week = "六";
							result.add(week);
						}
						break;
					default:
						break;
				}
			}
		}
		return result;
	}
	
	/**
	 * 函式名稱：getWeek(String date) 
	 * 功　　能：取得星期幾資訊 
	 * 傳　　入：YYYYMMDD
	 * 日期字串 傳　　回：星期N
	 */
	public static String getWeek(String date) {
		int str_week = 0;
		final String week[] = { "日", "一", "二", "三", "四", "五", "六" };

		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(4, 6)) - 1;
		int day = Integer.parseInt(date.substring(6, 8));

		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		str_week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		return week[str_week];
	}
	
	/**
	 * 取得之後天數的日期
	 * @param String
	 * @param int diiday
	 * @return afterdate
	 */
	public static String getAfterDate(String startday, int diiday) {
		String result = "";
		try {
			String dateString = startday;
			// 設定日期格式
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			// 進行轉換
			Date date = sdf.parse(dateString);
			String afterdate = sdf.format(new Date(date.getTime() + diiday * 24 * 60 * 60 * (long) 1000));
			result = afterdate;
		} catch (Exception e) {
			return null;
		}
		return result;
	}
	
	/**
	 * 將20160301時間轉換成03/01
	 * @param String(YYYYMMDD)
	 * @return String (MM/DD)
	 */
	public static String toSimpleDate(String date) {
		String dateString = "";
		// String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		dateString = month + "/" + day;
		return dateString;
	}
	
	/**
	 * 取得之前天數的日期
	 * @param String  startday (YYYYMMDD)
	 * @param int diiday
	 * @return beforedate
	 * 
	 */
	public static String getBeforeDate(String startday, int diiday) {
		String result = "";
		try {
			// 設定日期格式
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			// 進行轉換
			Date date = sdf.parse(startday);
			String beforedate = sdf.format(new Date(date.getTime() - diiday * 24 * 60 * 60 * (long) 1000));
			result = beforedate;
		} catch (Exception e) {
			return null;
		}
		return result;
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateTransform(String date) { // 20060704新增
		if (date != null && !date.equals("")) {
			int year = Integer.parseInt(date.substring(0, 4)) - 1900;
			int month = Integer.parseInt(date.substring(4, 6)) - 1;
			int day = Integer.parseInt(date.substring(6, 8));
			Date d = new Date(year, month, day);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/M/d");
			return formatter.format(d);
		} else {
			return "";
		}
	}
}
