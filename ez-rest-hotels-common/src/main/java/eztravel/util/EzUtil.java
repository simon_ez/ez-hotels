package eztravel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.validation.ValidationException;

import eztravel.rest.pojo.common.CodeInfo;

public class EzUtil {
  
  /**
   * 只返回有合約的適用時間，不拋異常
   * @param startDt 預計使用日期起(yyyyMMdd)
   * @param endDt 預計使用日期迄(yyyyMMdd) p.s 非退房日
   * @param agrees 合約內容 from API: /rest/v1/code/items/agree
   * @param Area 指定主要區域
   */
  public static List<Agree> getAgreeRangeArea(String startDt, String endDt, List<CodeInfo> agrees, Area focusArea) {
    try {
      return checkAgreeRangeAreaUsingDefaultWay(startDt, endDt, agrees, focusArea, false);
    } catch (Exception e) {
     return new ArrayList<Agree>();
    }
  }
  
  /**
   * 返回有合約的適用時間，若無適用時間則拋異常
   * @param startDt 預計使用日期起(yyyyMMdd)
   * @param endDt 預計使用日期迄(yyyyMMdd) p.s 非退房日
   * @param agrees 合約內容 from API: /rest/v1/code/items/agree
   * @param Area 指定主要區域
   */
  public static List<Agree> checkAgreeRangeAreaUsing(String startDt, String endDt, List<CodeInfo> agrees, Area focusArea) throws Exception {
    return checkAgreeRangeAreaUsingDefaultWay(startDt, endDt, agrees, focusArea, true);
  }
  
  
  /**
   * 檢查合約適用時間及區域，並可抉擇若無適用區間是否拋出異常
   * 有副約吃副約，沒副約吃主約
   * @param startDt 預計使用日期起(yyyyMMdd)
   * @param endDt 預計使用日期迄(yyyyMMdd) p.s 非退房日
   * @param agrees 合約內容 from API: /rest/v1/code/items/agree
   * @param Area 指定主要區域
   * @param isThrowError 是否要拋出異常
   * @return
   */
  private final static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
  public static List<Agree> checkAgreeRangeAreaUsingDefaultWay(String startDt, String endDt, List<CodeInfo> agrees, Area focusArea, boolean isThrowError) throws Exception {
    List<Agree> results = new ArrayList<Agree>(), allAgree = converCodeInfoToAgree(agrees), mAgrees = new ArrayList<Agree>();
    Agree dAgree = null;
    for(Agree agree: allAgree){
      if("D".equals(agree.getType()) && focusArea.code.equals(agree.getCountryCode())){ //副約
        dAgree = returnAgreeUseDateRange(startDt, endDt, agree);
        if(dAgree != null) {
          results.add(dAgree);
        }
      } 
       if("M".equals(agree.getType())) {
           mAgrees.add(agree);
       }
    }
    
    //以下過濾副約日期，餘下日期吃主約
    List<String[]> splitNonAgreeDay = null;
    if(results.isEmpty()){
      if(mAgrees.isEmpty()){
        if (isThrowError) {
          throw new ValidationException("區間含有無合約日期");
        } 
      }else{
        List<Agree> focusMagrees = getFocusMasterAgree(Integer.valueOf(startDt), Integer.valueOf(endDt), mAgrees, focusArea);
        List<String[]> cleanDaysAgree = returnCleanSubAgreeDaysList(startDt, endDt, focusMagrees);//確認主約是否含無何約區間
        results.addAll(focusMagrees); //完全無副約純主約
        if (focusMagrees.isEmpty()) {
          if (!cleanDaysAgree.isEmpty()) {
            throw new ValidationException("區間含有無合約日期");
          }
        } 
      }
    }else{
      splitNonAgreeDay = returnCleanSubAgreeDaysList(startDt, endDt, results);
      if(!splitNonAgreeDay.isEmpty()){
        if(mAgrees.isEmpty()){
          if (isThrowError) {
            throw new ValidationException("區間含有無合約日期");
          } 
        }else{
          for(String[] dayRange: splitNonAgreeDay){
            List<Agree> focusMagrees = getFocusMasterAgree(Integer.valueOf(dayRange[0]), Integer.valueOf(dayRange[1]), mAgrees, focusArea);
            results.addAll(focusMagrees);
            } 
          List<String[]> cleanDaysAgree = returnCleanSubAgreeDaysList(startDt, endDt, results);
          if (!cleanDaysAgree.isEmpty()) {
            if (isThrowError) {
              throw new ValidationException("區間含有無合約日期");
            } 
          }
        }
      }
    }
    
    Collections.sort( results, new Comparator<Agree>(){
      @Override
      public int compare(Agree a1, Agree a2) {
        return a1.getStartDt().compareTo(a2.getStartDt());
      }
    });
    return results;
  }
  
  /**
   * 過濾已有副約區間的日期
   * @param startDt
   * @param endDt
   * @param subAgree
   * @return List<Integer[]> [0]=無合約日期起，[1]無合約日期迄
   * @throws Exception
   */
  private static List<String[]> returnCleanSubAgreeDaysList(String startDt, String endDt, List<Agree> subAgree) throws Exception  {
    List<String[]> result = new ArrayList<>();
    List<String[]> spiltList =new ArrayList<>();
    for (Agree dAgree : subAgree) {
      if (result.isEmpty()) {
        result.addAll(splitSubAgreeDays(startDt, endDt, dAgree));
      } else {
        for (String[] dayRange: result) {
          spiltList.addAll(splitSubAgreeDays(dayRange[0], dayRange[1], dAgree));
        }
        result.clear();
        result.addAll(spiltList);
        spiltList.clear();
      }
    }
    return result;
  }
  
  /**
   * 用已適用合約過濾無合約日期
   * @param startDt
   * @param endDt
   * @param dAgree
   * @return
   * @throws ParseException
   */
  private static List<String[]> splitSubAgreeDays(String startDt, String endDt, Agree dAgree) throws ParseException {
    List<String[]> splitResults = new ArrayList<>();
    String[] dayRange = null;
    Calendar cal = Calendar.getInstance();
    if (Integer.parseInt(startDt) >= Integer.parseInt(dAgree.getStartDt()) &&
        Integer.parseInt(endDt) <= Integer.parseInt(dAgree.getEndDt())) {
      return splitResults;
    }
    if (Integer.parseInt(startDt) < Integer.parseInt(dAgree.getStartDt())
        && Integer.parseInt(endDt) > Integer.parseInt(dAgree.getStartDt())) {
      dayRange = new String[2];
      cal.setTime(yyyyMMdd.parse(dAgree.getStartDt()));
      cal.add(Calendar.DAY_OF_MONTH, -1);
      dayRange[0] = startDt;
      dayRange[1] = yyyyMMdd.format(cal.getTime());
      splitResults.add(dayRange);
    }
    if (Integer.parseInt(endDt) > Integer.parseInt(dAgree.getEndDt())
        && Integer.parseInt(endDt) > Integer.parseInt(dAgree.getStartDt())) {
      dayRange = new String[2];
      cal.setTime(yyyyMMdd.parse(dAgree.getEndDt()));
      cal.add(Calendar.DAY_OF_MONTH, 1);
      cal.setTime(yyyyMMdd.parse(dAgree.getEndDt()));
      cal.add(Calendar.DAY_OF_MONTH, 1);
      dayRange[0] = yyyyMMdd.format(cal.getTime());
      dayRange[1] = endDt;
      splitResults.add(dayRange);
    }
    if (dayRange == null) {
      dayRange = new String[2];
      dayRange[0] = startDt;
      dayRange[1] = endDt;
      splitResults.add(dayRange);
    }
    return splitResults;
  }
  
  /**
   * 返回驗證日期適用的合約區間
   * @param begDt
   * @param endDt
   * @param agree
   * @return
   */
  private static Agree returnAgreeUseDateRange(String begDt, String endDt, Agree agree){
    if(UtilDate.getDiffDays(begDt, agree.getStartDt()) <= 0){  //入住日 是否在 合約開始後
      if(UtilDate.getDiffDays(begDt, agree.getEndDt()) >= 0){ //入住日 是否在 合約結束前
        if(UtilDate.getDiffDays(endDt, agree.getEndDt()) >= 0) //退房日 是否再 合約結束前
          return new Agree(begDt, endDt, agree.getType(), agree.getCode(), agree.getCodeDesc(), agree.getCountryCode());
        else
          return new Agree(begDt, agree.getEndDt(), agree.getType(), agree.getCode(), agree.getCodeDesc(), agree.getCountryCode());
      }
    }else{
      if(UtilDate.getDiffDays(endDt, agree.getStartDt()) <= 0){ //退房日 是否在 合約開始後
        if(UtilDate.getDiffDays(endDt, agree.getEndDt()) >= 0) //退房日 是否在 合約結束前
          return new Agree(agree.getStartDt(), endDt, agree.getType(), agree.getCode(), agree.getCodeDesc(), agree.getCountryCode());
        else
          return new Agree(agree.getStartDt(), agree.getEndDt(), agree.getType(), agree.getCode(), agree.getCodeDesc(), agree.getCountryCode());
      }
    }
    return null;
  }
  
  /**
   * 返回適用的主約
   * @param startDt
   * @param endDt
   * @param mAgrees
   * @param focusArea
   * @return
   */
  private static List<Agree> getFocusMasterAgree(final Integer startDt, final Integer endDt, final List<Agree> mAgrees, Area focusArea) {
    List<Agree> list = new ArrayList<>();
    for (Agree agree : mAgrees) {
      Agree mAgree = returnAgreeUseDateRange(String.valueOf(startDt), String.valueOf(endDt), agree);
      if (mAgree != null) {
        list.add(mAgree);
      }
      }
    return list;
  }
  
  /**
   * 轉換合約API內容 CodeInfo格式 
   *    [ { "codeId": "001", "codeName": "台灣", "comment": "D,20170710~20170731" }, 
   *    { "codeId": "000", "codeName": "全球", "comment": "M,20170401~20501231" } ]
   * @param agrees
   * @return
   * @throws IllegalAccessException 
   * @throws Exception 
   */
  private static List<Agree> converCodeInfoToAgree(List<CodeInfo> agrees) throws Exception {
    List<Agree> result = new ArrayList<Agree>();
    for(CodeInfo info: agrees) {
      Integer count = info.getComment().split(",").length - 1; 
      for (int i = 1; i <= count; i++) {
        result.add(
            new Agree(info.getComment().split(",")[i].split("~")[0], 
            info.getComment().split(",")[i].split("~")[1], 
            info.getComment().split(",")[0], 
            info.getCodeId(), 
            info.getCodeName(),
            info.getCodeName().split(",")[1])
            );
      }
    }
    return result;
  }
  
  public static class Agree {
    private String startDt;
    private String endDt;
    private String type;
    private String code;
    private String codeDesc;
    private String countryCode;

    public Agree(String startDt, String endDt, String type, String code, String codeDesc, String countryCode){
      this.startDt = startDt;
      this.endDt = endDt;
      this.type = type;
      this.code = code;
      this.codeDesc = codeDesc;
      this.countryCode = countryCode;
    }
    public String getStartDt() {
      return startDt;
    }
    public void setStartDt(String startDt) {
      this.startDt = startDt;
    }
    public String getEndDt() {
      return endDt;
    }
    public void setEndDt(String endDt) {
      this.endDt = endDt;
    }
    public String getType() {
      return type;
    }
    public void setType(String type) {
      this.type = type;
    }
    public String getCode() {
      return code;
    }
    public void setCode(String code) {
      this.code = code;
    }
    public String getCodeDesc() {
      return codeDesc;
    }
    public void setCodeDesc(String codeDesc) {
      this.codeDesc = codeDesc;
    }
    public String getCountryCode() {
      return countryCode;
    }
    public void setCountryCode(String countryCode) {
      this.countryCode = countryCode;
    }
    
  }
  
  /**
   * 參考 Tblcode_main.item_id = 'R61'定義
   */
  public enum Area{
    TW("TW","台灣"),
    HK("HK","港澳地區"),
    CN("CN","大陸"),
    AU("AU","AU"),//不確定哪個國家,name先以代碼表示
    DE("DE","DE"),
    FR("FR","FR"),
    ID("ID","ID"),
    JP("JP","日本"),
    KR("KR","KR"),
    MO("MO","MO"),
    MY("MY","MY"),
    PH("PH","PH"),
    SG("SG","SG"),
    TH("TH","TH"),
    UK("UK","UK"),
    US("US","US"),
    VI("VI","VI");
    String code, name;
    Area(String code, String name){
      this.code = code;
      this.name = name;
    }
    public String getCode() {
      return code;
    }
    public String getName() {
      return name;
    }
    public void setCode(String code) {
      this.code = code;
    }
    public void setName(String name) {
      this.name = name;
    }

    /**
     * From code.
     * @param area code
     * @return area
     */
    public static Area fromCode(String code) {
      for (Area area : values()) {
        if (area.code.equals(code)) {
          return area;
        }
      }
      throw new IllegalArgumentException("No matching constant for [" + code + "]");
    }
  }
  
}
