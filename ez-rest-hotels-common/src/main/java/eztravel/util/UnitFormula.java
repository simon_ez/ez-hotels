package eztravel.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 單位公式轉換
 * @author Simon
 */
public class UnitFormula {
	
	/**
	 * 面積單位轉換 (預設小數點兩位)
	 * @param value
	 * @param input
	 * @param output
	 * @return
	 */
	public static BigDecimal converterAreaUnit(BigDecimal value, UnitArea input, UnitArea output){
		return converterAreaUnit(value, input, output, 2);
	}
	
	/**
	 * 面積單位轉換
	 * @param value
	 * @param input
	 * @param output
	 * @return
	 */
  public static BigDecimal converterAreaUnit(BigDecimal value, UnitArea input, UnitArea output, int pointScope){
		switch(input){
			case METER:
				switch(output){
					case PING:
						value = value.multiply(new BigDecimal(0.3025)).setScale(pointScope, BigDecimal.ROUND_HALF_UP);
					default:
						break;
				}
			case PING:
				switch(output){
					case METER:
						value = value.multiply(new BigDecimal(3.30579)).setScale(pointScope, BigDecimal.ROUND_HALF_UP);
					default:
						break;
			}
		}
		return value;
	}
	
	/**
	 * 面積單位
	 * @author Simon
	 */
	public static enum UnitArea{
		METER("Square Meters", "平方公尺"),
		PING("Taiwanese Ping", "坪");
		
		String unit, desc;
		UnitArea(String unit, String desc){
			this.unit = unit;
			this.desc = desc;
		}
	}
	
	/**
	 * 價格(整數Int)分位
	 * 每三位切入一個逗號
	 * 例：1000 = 1,000
	 * @param price
	 * @return
	 */
	public static String spiltPriceBit(int price){
    return new DecimalFormat("#,###").format(price);
	}
}
