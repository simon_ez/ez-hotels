package eztravel.util;

import org.apache.commons.lang.StringUtils;

public class ValidateEzUtil {
	/**
	 * 驗證使用者密碼規則
   * 1. 密碼不得同於帳號
   * 2. 密碼最短為8碼、最長20碼
   * 3. 英文數字混合
   * 4. 英文大小有區分
   * 5. 只可使用英文及數字，不可使用特殊符號
	 * @param userId
	 * @param pwd
	 * @return boolean true = 合法，false = 不合法
	 */
  public static boolean checkPasswordRule(String userId, String pwd){
  	boolean math = false;
  	boolean eng = false;
  	String regex1 = "[0-9]";
  	String regex2 = "[a-zA-Z]";
  	if(userId != null && pwd != null && !StringUtils.equals(userId, pwd)){
  		if(pwd.length() >= 8 && pwd.length() <= 20){ // 最短為8碼、最長20碼
  			for(int i = 0; i < pwd.length(); i++){
  				if(pwd.substring(i, i+1).matches(regex1)) //是數字
  					math = true;
  				else if(pwd.substring(i, i+1).matches(regex2)) //是英文
  					eng = true;
  				else //有非法字元
  					return false;
  			}
  		}
  	}
  	if(math && eng)
  		return true; //符合英數混合
  	else
  		return false; //不符合英數混合
  }
}
