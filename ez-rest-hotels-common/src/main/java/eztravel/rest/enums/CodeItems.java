package eztravel.rest.enums;

public enum CodeItems {
	check("check", "檢查帳號"), 
	authenticationInfo("authentication", "帳號密碼相關資訊")
	;

	private CodeItems(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/** The code. */
	final String code;
	/** The name. */
	final String name;

	/**
	 * Gets the name.
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
}
