package eztravel.rest.pojo.common;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author yuwei
 * <pre>查詢CodeDetail</pre>
 */
@ApiModel(description="代碼資訊")
public class CodeInfo implements Serializable{

  @ApiModelProperty("代碼ID")
  private String codeId;
  
  @ApiModelProperty("代碼名稱")
  private String codeName;
  
  @ApiModelProperty("代碼描述")
  private String codeDesc;
  
  @ApiModelProperty("Display Seq")
  private Integer displaySeq;
  
  @ApiModelProperty("其他註釋用")
  private String comment;

  public String getCodeId() {
    return codeId;
  }
  public void setCodeId(String codeId) {
    this.codeId = codeId;
  }
  public String getCodeName() {
    return codeName;
  }
  public void setCodeName(String codeName) {
    this.codeName = codeName;
  }
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCodeDesc() {
		return codeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}
	public Integer getDisplaySeq() {
		return displaySeq;
	}
	public void setDisplaySeq(Integer displaySeq) {
		this.displaySeq = displaySeq;
	}
	
}
