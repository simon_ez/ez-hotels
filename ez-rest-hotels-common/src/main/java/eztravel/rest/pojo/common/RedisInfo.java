package eztravel.rest.pojo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

@ApiModel(description = "Redis儲存格式")
public class RedisInfo {
	private String key;
	private Object obj;
	private Integer expire;
	
	@ApiModelProperty(value = "鍵值", example="PATH:KEY")
  @JsonProperty("key")
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	@ApiModelProperty(value = "內容", example="ABCD1234")
  @JsonProperty("obj")
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	@ApiModelProperty(value = "儲存時間", example="30000")
  @JsonProperty("expire")
	public Integer getExpire() {
		return expire;
	}
	public void setExpire(Integer expire) {
		this.expire = expire;
	}
}
